//
//  kata_SearchViewController.m
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_SearchViewController.h"
#import "KATAUtils.h"
#import "kata_segView.h"
#import "kata_Tools.h"
#import "kata_AddressViewController.h"
#import "KATAUtils.h"
#import "kata_DeliveryTimeCell.h"


#import "kata_SearchMEGInsideView.h"
#import "kata_SearchMEGOutView.h"
#import "kata_SearchPTAInsideView.h"
#import "kata_SearchPTAOutView.h"

#import "kata_QueryResultViewController.h"

#import "KATASearchDao.h"

#import "KATAUITabBarController.h"
#import "KATAConstants.h"
@interface kata_SelcetConditionCell : UITableViewCell

@end

@implementation kata_SelcetConditionCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        
        
        UILabel *nameLab  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        nameLab.tag = 11;
        
        nameLab.backgroundColor = [UIColor clearColor];
        
//        nameLab.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:nameLab];
        
        [nameLab release];
        
        UITextField *textFiled = [[UITextField alloc]initWithFrame:CGRectMake(90, 7, 220, 30)];
        
        textFiled.userInteractionEnabled = NO;
        textFiled.tag = 12;
        
        textFiled.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
//        textFiled.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:textFiled];
        
        [textFiled release];
 
    }
    
    return self;
    
}

@end



@interface kata_SearchViewController ()
{
    UITableView *_tableView;
    
    
    UIDatePicker *_picker;

    BOOL isPicker;
    
    
    int _plateType;
    
    KATASearchDao *_searchDao;
    
//    UITextField *_textField;
    
    kata_SearchMEGInsideView *_searchMEGInsideView;
    
    kata_SearchMEGOutView *_searchMEGOutView;
    
    kata_SearchPTAInsideView *_searchPTAInsideView;
    
    kata_SearchPTAOutView *_searchPTAOutView;

}
@property(nonatomic,retain)NSMutableArray *dataArray;

@property(nonatomic,retain)NSString *pricefrom;
@property(nonatomic,retain)NSString *priceto;
@property(nonatomic,retain)NSString *dirStr;
@property(nonatomic,retain)NSString *priceStr;
@property(nonatomic,retain)NSString *goodsStr;
@property(nonatomic,retain)NSString *shipStr;
@property(nonatomic,retain)NSString *arrivalStr;
@property(nonatomic,retain)NSString *stateStr;
@property(nonatomic,retain)NSString *addressStr;
@property(nonatomic,retain)NSString *dateStr1;
@property(nonatomic,retain)NSString *dateStr2;
@property(nonatomic,retain)NSString *dateStr3;
@property(nonatomic,retain)NSString *dateStr4;
@property(nonatomic,retain)NSString *dispatching;
@property(nonatomic,retain)NSString *toString;
@property(nonatomic,retain)NSString *delivelyTimeStr;

@property(nonatomic,retain)NSMutableArray *insideArrayMEG;
@property(nonatomic,retain)NSMutableArray *outerArrayMEG;
@property(nonatomic,retain)NSMutableArray *insideArrayPTA;
@property(nonatomic,retain)NSMutableArray *outerArrayPTA;


@end

@implementation kata_SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}
-(void)dealloc
{
    [_pricefrom  release];
    [_priceto release];
    [_dirStr release];
    
    [_priceStr release];
    
    [_goodsStr release];
    [_shipStr release];
    [_addressStr release];
    [_stateStr release];
    [_addressStr release];
    [_dateStr1 release];
    
    [_dateStr2 release];
    [_dateStr3 release];
    [_dateStr4 release];
    [_dispatching release];
    [_toString release];
    
    [_delivelyTimeStr release];
   
   
    
    [_searchMEGInsideView release];

    [_searchMEGOutView release];
    
    [_searchPTAInsideView release];
    
    [_searchPTAOutView  release];
    
    
    [_searchDao release];
    
    [_insideArrayPTA release];
    [_outerArrayPTA release];
    [_insideArrayMEG release];
    [_outerArrayMEG release];
    
    [_picker release];

    [_dataArray release];

    [super dealloc];
}
#pragma mark - beginningView

-(void)beginningView
{
    self.title = @"搜索";
    
    self.navigationItem.hidesBackButton = YES;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    
    self.dataArray = [NSMutableArray arrayWithObjects:@"方向",@"价格",@"货种",@"交割时间",@"状态",@"交割地", nil];

    
    kata_segView *segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40) typeIdentifier:101];

    DebugLog(@"%@",NSStringFromCGRect(segView.frame));
    
    segView.delegate = self;
    
    [self.view addSubview:segView];
    
    [segView release];

    
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    backBtn.frame = CGRectMake(0, 0, 44, 44);
//    
//    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
//    
//    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
//    
//    
//    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
//    
//    
//    self.navigationItem.leftBarButtonItem  = leftItem;
//    
//    [leftItem release];
//
    
}

//- (void)backClick
//{
//    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
//    
//    [tab showTabBar];
//    
//    [self.navigationController popViewControllerAnimated:YES];
//    
//}
- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    _insideArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"货种",@"交割时间",@"状态",@"交割地", nil];
    _outerArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"类型",@"装船时间",@"到港时间",@"状态",@"交割地", nil];
    
    _insideArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"配送",@"货种",@"交割时间",@"状态",@"交割地", nil];
    
    _outerArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"价格",@"类型",@"装船时间",@"到港时间",@"状态",@"交割地", nil];
    
    
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchTableViewData:) name:SEARCH_TABLEVIEWDATA object:nil];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self beginningView];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    _tableView.backgroundView = nil;
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];
    

    _searchDao  = [[KATASearchDao alloc]init];
    
    _searchDao.pricefrom  = nil;
    
    _searchDao.priceto = nil;
    
    _searchDao.temptime  = @"-1";
    _searchDao.temptime1  = @"-1";
    _searchDao.temptime2  = @"-1";
    _searchDao.temptime3  = @"-1";
    
    int defaultValue = -1;
    
    _searchDao.cargobonded  = defaultValue;

    _searchDao.dispatching  = defaultValue;

    
    
    //分散
//   _searchMEGInsideView = [[kata_SearchMEGInsideView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
//    
//    _searchMEGInsideView.delegate = self;
//    
//    [self.view addSubview:_searchMEGInsideView];
//
//
//    _searchMEGOutView = [[kata_SearchMEGOutView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
//
//    _searchMEGOutView.delegate = self;
//    
//    
//    
//     _searchPTAInsideView = [[kata_SearchPTAInsideView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
//
//    _searchPTAInsideView.delegate = self;
//    
//    
//    _searchPTAOutView = [[kata_SearchPTAOutView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight)];
//
//    _searchPTAOutView.delegate = self;
//



    
    
    


}


#pragma mark -  NSNotification -- SEARCH_TABLEVIEWDATA

- (void)searchTableViewData:(NSNotification *)notification
{
    
    int type = ([[notification object] intValue] - 1);
    
    switch (type) {
        case 0:
        {
            [self changeSearchArray:_insideArrayMEG];
        }
            break;
        case 1:
        {
            [self changeSearchArray:_outerArrayMEG];
        }
            break;
        case 2:
        {
            [self changeSearchArray:_insideArrayPTA];
            
            
        }
            break;
        case 3:
        {
            [self changeSearchArray:_outerArrayPTA];
            
        }
            break;
            
        default:
            break;
    }
    
}


- (void)changeSearchArray:(NSMutableArray *)aArray
{

    [_dataArray removeAllObjects];
    
    [_dataArray addObjectsFromArray:aArray];
    
    [_tableView reloadData];
    
}


#pragma mark - kata_segDelegate

- (void)changeIndex:(int)index
{
    _plateType = index - 1;

    _searchDao.brandid = index - 1;
    
    
    self.dirStr = @"";
    self.pricefrom = @"";
    self.priceto = @"";
    self.addressStr = @"";
    self.stateStr = @"";
    self.shipStr = @"";
    self.dateStr1 = @"";
    self.dateStr2 = @"";
    self.dateStr3 = @"";
    self.dateStr4 = @"";

    self.goodsStr = @"";
    self.priceStr = @"";
    self.arrivalStr = @"";

    self.toString  = @"";
    

}


#pragma mark - custom
-(void)queryBtnClick
{
    DebugLog(@"查询");
    
            
    kata_QueryResultViewController *queryResultVC =   [[kata_QueryResultViewController alloc]initWithKATASearchDao:_searchDao];
    
    [self.navigationController pushViewController:queryResultVC animated:YES];
    
    [queryResultVC release];

    
    
}
- (void)changeTime:(id)sender
{
    
    UIDatePicker *picker = (UIDatePicker *)sender;
    
    isPicker =  NO;
    
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:1];
    
    
    _picker.frame = CGRectMake(0, ScreenHeight + 216, ScreenWidth, 216);
    
    [UIView commitAnimations];
    
    
//    UIDatePicker* control = (UIDatePicker*)sender;
    
    NSDate* _date = _picker.date;
    
    DebugLog(@"_date == %@",_date);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    [formatter setDateFormat:@"yyyy-MM-dd "];

    NSString *dateStr = [formatter stringFromDate:_date];
    
    if (picker.tag == 11) {
        
        self.dateStr1 = dateStr;
        
        _searchDao.temptime = dateStr;
        
    }else if (picker.tag == 12){
        
       self.dateStr2 = dateStr;

        _searchDao.temptime1 = dateStr;

    }else if (picker.tag == 13){
        
        self.dateStr3 = dateStr;
        
        _searchDao.temptime2 = dateStr;
        
    }

    else{
        
        self.dateStr4 = dateStr;
        
        _searchDao.temptime3 = dateStr;
    }

    
    [formatter release];

    [_tableView reloadData];
}

#pragma mark - tableViewDataSoure
-(float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 85.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *view = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 85.0)] autorelease];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(85, 20, 150, 45);
    
    [button addTarget:self action:@selector(queryBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [button setImage:[UIImage imageNamed:@"but.png"] forState:UIControlStateNormal];
    
    [view addSubview:button];
    
    
    return view;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_dataArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static  NSString *cellIdtifierMEGInside = @"CellMEGInside ";
    
    static  NSString *cellIdtifierMEGInsideTime = @"CellMEGInsideTime";
    
    static  NSString *cellIdtifierMEGOut = @"CellMEGOut";

    static  NSString *cellIdtifierMEGOutTime  = @"CellMEGOutTime";
    static  NSString *cellIdtifierMEGOutTime1 = @"CellMEGOutTime1";

    static  NSString *cellIdtifierPTAInside = @"CellPTAInside";
    
    static  NSString *cellIdtifierPTAInsideTime  = @"CellPTAInsideTime ";

    static  NSString *cellIdtifierPTAOut = @"CelPTAOut";
    
    static  NSString *cellIdtifierPTAOutTime = @"CelPTAOutTime";
    static  NSString *cellIdtifierPTAOutTime1 = @"CelPTAOutTime1";

    int row = indexPath.row;

    if (_plateType == 0) {   //MEG内盘
        
        if (row == 3) {
            
            kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInsideTime];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInsideTime] autorelease];
                
                
                cell.delegate = self;
                
            }
            
            UILabel *nameLab = (UILabel *)[cell viewWithTag:11];
            
            nameLab.text = @"交割时间";
            
            
            UIButton *btn1 = (UIButton *)[cell viewWithTag:1];
            
            btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn1 setTitle:self.dateStr3 forState:UIControlStateNormal];

            
            UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
            
            
            btn2.titleLabel.adjustsFontSizeToFitWidth = YES;

            [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn2 setTitle:self.dateStr4 forState:UIControlStateNormal];
            

            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }else{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInside];

            
            if (!cell) {
                
                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInside] autorelease];
                
                
                
            }
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];

            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];

            if (indexPath.row == ([self.dataArray count] - 1) )
            {

                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;

            }

            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];

            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];


            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = @"";

                textFiled.text = self.dirStr;
            }
            
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = @"";
                
                NSString *temp1  = [self.pricefrom stringByAppendingString:@"  "];
                NSString *temp2  = [temp1 stringByAppendingString:self.toString];
                NSString *temp3  = [temp2 stringByAppendingString:@"  "];

                NSString *price = [temp3 stringByAppendingString:self.priceto];
                
                textFiled.text = price;
            }
            
            
            if ([textFiled.text isEqualToString:@"货种"])
            {
                
                textFiled.text = @"";
                textFiled.text = self.goodsStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = @"";
                
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = @"";

                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
             
                textFiled.text = @"";

                textFiled.text = self.addressStr;
            }

            
                     
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;

            
            
        }
        
        
        
        
    }else if(_plateType == 1){ //MEG外盘
        
        
       
            
            if (row == 3 ) {
                
                kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGOutTime];
                
                if (!cell) {
                    
                    cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGOutTime ] autorelease];
                    
                    
                    cell.tag = row;
                    
                    cell.delegate = self;
                    
                }
                
                UILabel *nameLab  = (UILabel *)[cell viewWithTag:11];
                
                nameLab.text = [self.dataArray objectAtIndex:row];
                
                UIButton *btn1 = (UIButton *)[cell viewWithTag:1] ;
                    
                    btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
                    
                    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    
                    [btn1 setTitle:self.dateStr1 forState:UIControlStateNormal];
                    
                    
                    UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
                    
                    
                    btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
                    
                    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    [btn2 setTitle:self.dateStr2 forState:UIControlStateNormal];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;

            }
        
                if (row ==  4) {
                    
                    kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGOutTime1];
                    
                    if (!cell) {
                        
                        cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGOutTime1 ] autorelease];
                        
                        
                        cell.tag = row;
                        
                        cell.delegate = self;

                    
                }
                    UILabel *nameLab  = (UILabel *)[cell viewWithTag:11];
                    
                    nameLab.text = [self.dataArray objectAtIndex:row];
                    
                    UIButton *btn1 = (UIButton *)[cell viewWithTag:1] ;
                    
                    btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
                    
                    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    
                    [btn1 setTitle:self.dateStr3 forState:UIControlStateNormal];
                    
                    
                    UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
                    
                    
                    btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
                    
                    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    
                    [btn2 setTitle:self.dateStr4 forState:UIControlStateNormal];

                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;

            }else{
                
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGOut];
                
                
                if (!cell) {
                    
                    
                    cell = [[[kata_SelcetConditionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGOut] autorelease];
                    
                }
                
                
                
                UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
                
                nameLab.text = [self.dataArray objectAtIndex:row];
                
                if (indexPath.row == ([self.dataArray count] - 1) )
                {
                    
                    cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                    
                }
                
                UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
                
                textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
                
                
                if ([textFiled.text isEqualToString:@"装船时间"])
                {
                    textFiled.text = self.dateStr1;
                }
                if ([textFiled.text isEqualToString:@"到港时间"])
                {
                    textFiled.text = self.dateStr2;
                }
                
                
                if ([textFiled.text isEqualToString:@"方向"])
                {
                    textFiled.text = self.dirStr;
                }
                if ([textFiled.text isEqualToString:@"价格"])
                {
                    textFiled.text = @"";
                    
                    NSString *temp1  = [self.pricefrom stringByAppendingString:@"  "];
                    NSString *temp2  = [temp1 stringByAppendingString:self.toString];
                    NSString *temp3  = [temp2 stringByAppendingString:@"  "];
                    
                    NSString *price = [temp3 stringByAppendingString:self.priceto];
                    
                    textFiled.text = price;
                }
                
                if ([textFiled.text isEqualToString:@"类型"])
                {
                    
                    textFiled.text = @"";
                    textFiled.text = self.shipStr;
                }
                if ([textFiled.text isEqualToString:@"交割时间"])
                {
                    textFiled.text = nil;
                }
                if ([textFiled.text isEqualToString:@"状态"])
                {
                    textFiled.text = self.stateStr;
                }
                if ([textFiled.text isEqualToString:@"交割地"])
                {
                    
                    textFiled.text = self.addressStr;
                }
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;
                
            }
        
    }else if(_plateType == 2){  //PTA内盘
        
        
        
        if (row == 4 ) {
            
            kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAInsideTime];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAInsideTime] autorelease];
                
                
                cell.delegate = self;

                
            }
            
            UILabel *nameLab = (UILabel *)[cell viewWithTag:11];
            
            nameLab.text = @"交割时间";
            
            
            UIButton *btn1 = (UIButton *)[cell viewWithTag:1];
            
            btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn1 setTitle:self.dateStr3 forState:UIControlStateNormal];
            
            
            UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
            
            
            btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn2 setTitle:self.dateStr4 forState:UIControlStateNormal];
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            return cell;
            
        }else{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAInside];
            
            
            if (!cell) {
                
                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAInside] autorelease];
                
                
                
            }
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == ([self.dataArray count] - 1) )
            {
                
                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            
            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
            
            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
            
            
            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
          
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = @"";
                
                NSString *temp1  = [self.pricefrom stringByAppendingString:@"  "];
                NSString *temp2  = [temp1 stringByAppendingString:self.toString];
                NSString *temp3  = [temp2 stringByAppendingString:@"  "];
                
                NSString *price = [temp3 stringByAppendingString:self.priceto];
                
                textFiled.text = price;
            }
            if ([textFiled.text isEqualToString:@"配送"])
            {
                //                textFiled.text = @"";
                
                textFiled.text = self.dispatching;
            }

            if ([textFiled.text isEqualToString:@"类型"])
            {
                textFiled.text = self.shipStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = @"";
                
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }
            
            if ([textFiled.text isEqualToString:@"货种"])
            {
                
                textFiled.text = @"";
                textFiled.text = self.goodsStr;
            }

            if ([textFiled.text isEqualToString:@"sgu"])
            {
                
                textFiled.text = @"";
                textFiled.text = self.goodsStr;
            }

            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }

        
        
    }else{ //PTA外盘

     
        
        if (row == 3 ) {
            
            kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAOutTime];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAOutTime ] autorelease];
                
                
                cell.tag = row;
                
                cell.delegate = self;
                
            }
            
            UILabel *nameLab  = (UILabel *)[cell viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:row];
            
            UIButton *btn1 = (UIButton *)[cell viewWithTag:1] ;
            
            btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            [btn1 setTitle:self.dateStr1 forState:UIControlStateNormal];
            
            
            UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
            
            
            btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn2 setTitle:self.dateStr2 forState:UIControlStateNormal];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }
        
        if (row ==  4) {
            
            kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAOutTime1];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAOutTime1 ] autorelease];
                
                
                cell.tag = row;
                
                cell.delegate = self;
                
                
            }
            UILabel *nameLab  = (UILabel *)[cell viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:row];
            
            UIButton *btn1 = (UIButton *)[cell viewWithTag:1] ;
            
            btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            [btn1 setTitle:self.dateStr3 forState:UIControlStateNormal];
            
            
            UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
            
            
            btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
            
            [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [btn2 setTitle:self.dateStr4 forState:UIControlStateNormal];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }else{
        
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierPTAOut];
            
            
            if (!cell) {
                
                cell = [[[kata_SelcetConditionCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierPTAOut] autorelease];
                
                
                
            }
            
            
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
            
            nameLab.text = [self.dataArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == ([self.dataArray count] - 1) )
            {
                
                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            
            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
            
            textFiled.text = [self.dataArray objectAtIndex:indexPath.row];
            
        
        if ([textFiled.text isEqualToString:@"装船时间"])
        {
            textFiled.text = self.dateStr1;
        }
        if ([textFiled.text isEqualToString:@"到港时间"])
        {
            textFiled.text = self.dateStr2;
        }

            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = @"";
                
                NSString *temp1  = [self.pricefrom stringByAppendingString:@"  "];
                NSString *temp2  = [temp1 stringByAppendingString:self.toString];
                NSString *temp3  = [temp2 stringByAppendingString:@"  "];
                
                NSString *price = [temp3 stringByAppendingString:self.priceto];
                
                textFiled.text = price;
            }
            if ([textFiled.text isEqualToString:@"类型"])
            {
                textFiled.text = self.shipStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
        }
    }

}

#pragma tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *str = [self.dataArray objectAtIndex:indexPath.row];
    
    
    
    
    if ([str isEqualToString:@"交割时间"])
    {
        int delivelyTimeTag = 11;
        [self pickerShow:delivelyTimeTag];
        
    }

    
    if ([str isEqualToString:@"装船时间"])
    {
        int time1Tag = 1;
        [self pickerShow:time1Tag];
        
    }
    if ([str isEqualToString:@"到港时间"])
    {
        int time2Tag = 2;

        [self pickerShow:time2Tag];
        
    }

    
    
    if ([str isEqualToString:@"方向"])
    {
        
        DebugLog(@"alert");
        
       UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择方向" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"卖盘",@"买盘", nil];
        
        dirSheet.tag = 21;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;

        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
    }
    if ([str isEqualToString:@"货种"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择货种" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"期货",@"现货", nil];
        
        dirSheet.tag = 22;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }
   
    if ([str isEqualToString:@"状态"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择状态" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"有效/未成交",@"过期/未成交",@"成交", nil];
        
        dirSheet.tag = 23;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }
    if ([str isEqualToString:@"类型"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择类型" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"船税",@"保税", nil];
        
        dirSheet.tag = 24;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }
    if ([str isEqualToString:@"配送"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"配送方式" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"送到",@"自提", nil];
        
        dirSheet.tag = 25;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self.view];
        
        [dirSheet release];
        
        
    }

  
    
    if ([str isEqualToString:@"交割地"])
    {
        
        kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:_plateType addressType:1000];
        
        addressVC.addressDelegate   = self;
        
        [self.navigationController pushViewController:addressVC animated:YES];
        
        [addressVC release];
        
        
    }

    if ([str isEqualToString:@"价格"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"选择价格" message:@"\n" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        
        
        UITextField *priceText = [[UITextField alloc]initWithFrame:CGRectMake(10, 45, 132 - 15, 30)];
        
//        priceText.backgroundColor = [UIColor redColor];
        
        priceText.borderStyle = UITextBorderStyleRoundedRect;
        
//        priceText.text = self.priceAtAalertStr;
        
        priceText.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText.delegate = self;
        
        priceText.tag = 1;
        
        [alert addSubview:priceText];
        
        [priceText release];
        
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10 + 132 -15, 45, 30, 30)];
        
        self.toString =  @"至";
        
        label.text  = self.toString;
        
        
        label.backgroundColor  = [UIColor clearColor];
        
        
        label.textAlignment = NSTextAlignmentCenter;
        
        [alert addSubview:label];
        
        [label release];
        
        
        
        
        UITextField *priceText2 = [[UITextField alloc]initWithFrame:CGRectMake(10 + 132 + 15, 45, 132 - 15, 30)];
        
        //        priceText.backgroundColor = [UIColor redColor];
        
        priceText2.borderStyle = UITextBorderStyleRoundedRect;
        
        //        priceText.text = self.priceAtAalertStr;
        
        priceText2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText2.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText2.delegate = self;
        
        priceText2.tag = 2;
        
        [alert addSubview:priceText2];
        
        [priceText2 release];

        
        
        
        alert.delegate = self;

        [alert show];
        
        [alert release];
        
    }

    
}

//-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
//{
//    
//    
//}

#pragma mark - actionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex == %d",buttonIndex);
   
    switch (actionSheet.tag) {
        case 21:  //方向
        {
            if (buttonIndex == 0)
            {
                self.dirStr =  @"卖盘";
                
                _searchDao.direction  = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
               self.dirStr =  @"买盘";
                
                _searchDao.direction  = 1;

                
                [_tableView reloadData];

            }
        }
            break;
        case 22:  // 货种
        {
            
            if (buttonIndex == 0)
            {
                self.goodsStr =  @"期货";
                
                _searchDao.spotfutures = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
                self.goodsStr =  @"现货";
                
                _searchDao.spotfutures = 1;


                [_tableView reloadData];
                
            }

        }
            break;
        case 23:  //状态
        {
            if (buttonIndex == 0)
            {
                self.stateStr =  @"有效/未成交";
                
                _searchDao.dealstatus = 5;
                
                [_tableView reloadData];
                
            }
            else if (buttonIndex == 1)
            {
                self.stateStr =  @"过期/未成交";
                
                _searchDao.dealstatus = 6;

                [_tableView reloadData];
                
            }else
            {
                self.stateStr =  @"成交";
                
                
                _searchDao.dealstatus = 7;
                
                
                [_tableView reloadData];
                
            }


        }
            break;

        case 24:  //类型
        {
            if (buttonIndex == 0)
            {
                self.shipStr =  @"船税";
                
                _searchDao.cargobonded = 0;

                [_tableView reloadData];
                
            }
            else
            {
                self.shipStr =  @"保税";
                
                _searchDao.cargobonded = 1;

                [_tableView reloadData];
                
            }
            
        }
            break;
        case 25:  //类型
        {
            if (buttonIndex == 0)
            {
                self.dispatching =  @"送到";
                
                _searchDao.dispatching = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
                self.dispatching =  @"自提";
                
                _searchDao.dispatching = 1;
                
                [_tableView reloadData];
                
            }
            
        }
            break;

        default:
            break;
    }
    
}
#pragma mark - pickerDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:1];
    
    
    _picker.frame = CGRectMake(0, ScreenHeight + 120, ScreenWidth, 120);
    
    
    [UIView commitAnimations];
    

    
}

#pragma mark - alertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    
    [_tableView reloadData];

    
    
    DebugLog(@"alertView.frame == %@ ",NSStringFromCGRect(alertView.frame));
}

#pragma mark -  textFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    DebugLog(@"textField = %@",textField);

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    DebugLog(@"textField  == %@",textField.text);
    
    if (textField.tag == 1) {
        
        self.pricefrom  = textField.text;
        
        _searchDao.pricefrom = textField.text;

    }else{
        
        self.priceto   = textField.text;

        _searchDao.priceto = textField.text;

    }
    
        return YES;

       
    
}

#pragma mark - kata_AddressViewControllerDelegate

- (void)kata_AddressViewControllerWithOfferType:(int)aOfferType addressType:(int)aAddressType AddressName:(NSString *)aAddressName addressId:(NSString *)aAddressId
{
    
    self.addressStr = aAddressName;
    
    _searchDao.addressId = [aAddressId intValue];
    
    [_tableView reloadData];

}



#pragma mark - pickerShow
- (void)pickerShow:(int)tag
{
    if (isPicker == NO)
    {
        isPicker = YES;
        
        _picker  = [[UIDatePicker alloc]init];
        
        _picker.frame = CGRectMake(0,ScreenHeight, ScreenWidth, 216);
        
        _picker.datePickerMode = UIDatePickerModeDate;
        
        _picker.tag = tag;
        
        [_picker addTarget:self action:@selector(changeTime:) forControlEvents:UIControlEventValueChanged];
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:1];
        
        //        [UIView setAnimationDelegate:self];
        //
        //        [UIView setAnimationDidStopSelector:@selector(animationDidStop:
        //                                                      finished:
        //                                                      context:)];
        
        _picker.frame = CGRectMake(0, ScreenHeight - 216 - 49, ScreenWidth, 216);
        
        
        
        [UIView commitAnimations];
        
        [self.view addSubview:_picker];
        
    }

}





#pragma mark - 

//- (void)push
//{
//    
//    kata_QueryResultViewController *queryResultVC =   [[kata_QueryResultViewController alloc]initWithKATASearchDao:_searchDao];
//    
//    [self.navigationController pushViewController:queryResultVC animated:YES];
//    
//    [queryResultVC release];
//    
//
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
