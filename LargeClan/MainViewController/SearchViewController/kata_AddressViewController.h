//
//  kata_AddressViewController.h
//  LargeClan
//
//  Created by kata on 13-11-27.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASIHTTPRequest.h"

@protocol kata_AddressViewControllerDelegate <NSObject>



- (void)kata_AddressViewControllerWithOfferType:(int)aOfferType addressType:(int)aAddressType AddressName:(NSString *)aAddressName addressId:(NSString *) aAddressId;

@end

@interface kata_AddressViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,
    ASIHTTPRequestDelegate>
{
    id <kata_AddressViewControllerDelegate> addressDelegate;
    
}
- (id)initWithOfferType:(int)aOfferType addressType:(int)aAddressType;

@property (nonatomic,assign)    id <kata_AddressViewControllerDelegate> addressDelegate;


@end
