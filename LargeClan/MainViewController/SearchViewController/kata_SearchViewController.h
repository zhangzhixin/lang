//
//  kata_SearchViewController.h
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "kata_segView.h"
#import "kata_AddressViewController.h"
#import "kata_DeliveryTimeCell.h"
#import "kata_SearchMEGInsideView.h"
#import "kata_SearchMEGOutView.h"
#import "kata_SearchPTAInsideView.h"
#import "kata_SearchPTAOutView.h"


@protocol kata_SearchViewControllerDelegate <NSObject>

//- (void):kata_SearchViewControllerMyOffer:(int)aIndex;

@end

@interface kata_SearchViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,kata_segViewDelegate,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UITextFieldDelegate,
    kata_AddressViewControllerDelegate,
    kata_DeliveryTimeCellDelegate,
    kata_SearchMEGInsideViewDelegate,
    kata_SearchMEGOutViewDelegate,
    kata_SearchPTAInsideViewDelegate,
    kata_SearchPTAOutViewDelegate>

{
    
    id <kata_SearchViewControllerDelegate> delegate;
}

@property(nonatomic,assign)    id <kata_SearchViewControllerDelegate> delegate;


@end
