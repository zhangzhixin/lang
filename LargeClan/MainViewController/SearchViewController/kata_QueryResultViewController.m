//
//  kata_QueryResultViewController.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_QueryResultViewController.h"

#import "kata_TwoTableView.h"
#import "KATAUtils.h"

#import "ASIFormDataRequest.h"

#import "KATAConstants.h"

#import "NSString+MD5.h"

#import "KATASearchDao.h"

#import "KATAUITabBarController.h"

#import "MBProgressHUD.h"
@interface kata_QueryResultViewController ()

{
    int _type;
    
    KATASearchDao *_searchDao;
    
}
@property(nonatomic,retain)NSMutableDictionary *postDic;

@end


@implementation kata_QueryResultViewController
- (id)initWithKATASearchDao:(id)aKATASearchDao

{
    if (self = [super init]) {
        
        
        
       _searchDao = (KATASearchDao *)aKATASearchDao;
        
        
        
        
        DebugLog(@"%d" ,_searchDao.brandid);

        
        DebugLog(@"%d %d %@ %@  time %@ %@ %@ %@ " ,_searchDao.direction,_searchDao.addressId,_searchDao.pricefrom,_searchDao.priceto,_searchDao.temptime,_searchDao.temptime1,_searchDao.temptime2,_searchDao.temptime3);
        
        
    }
    
    return self;
    
}



- (void)viewDidLoad
{
    
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"搜索结果";
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

    
    
    
}


- (void)backClick
{
    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    
    [tab showTabBar];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    
    [tab hideTabBar];

    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:SERVER_URI]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds = 20;

    request.delegate = self;
    
    [request startAsynchronous];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
}


#pragma mark - ASIFormDataRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    DebugLog(@"request = %@",[request responseString]);
    
    
    kata_TwoTableView *twoTableView = [[kata_TwoTableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    
    [self.view addSubview:twoTableView];
    
    [twoTableView release];
    

    
    NSDictionary *dic = [NSJSONSerialization    JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_RESULT_HEARDView object:[NSNumber numberWithInteger:_searchDao.brandid]];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_RESULT object:self userInfo:dic];
    
    
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"searchButtonChangeData" object:[NSNumber numberWithInt:_searchDao.brandid]];

    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    
}


#pragma mark - sign

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offer_get";
}
-(NSString *)changeTimeToTimeSp:(NSString *)timeStr{
    
    if ([timeStr isEqualToString:@"-1"]) {
        
        return timeStr;
    }else{
        
        long time;
        NSDateFormatter *format=[[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd"];
        NSDate *fromdate=[format dateFromString:timeStr];
        time= (long)[fromdate timeIntervalSince1970];
        
        NSString *timeString = [NSString stringWithFormat:@"%ld",time];
        
        [format release];
        return timeString;

    }
}

- (NSDictionary *)params
{
    
    DebugLog(@"%d" ,_searchDao.brandid);

    
    DebugLog(@"%d %d %@ %@  time %@ %@ %@ %@ ",_searchDao.direction,_searchDao.addressId,_searchDao.pricefrom,_searchDao.priceto,_searchDao.temptime,_searchDao.temptime1,_searchDao.temptime2,_searchDao.temptime3);

   
    if (_searchDao.brandid == 0) {
        _postDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    
//                    [NSNumber numberWithInt:_searchDao.brandid],@"brandid",
                    [NSNumber numberWithInt:_searchDao.brandid],@"nameofpart",
                    [NSNumber numberWithInt:_searchDao.direction],@"direction",
                    [NSNumber numberWithInt:_searchDao.dealstatus],@"dealstatus",
                    [NSNumber numberWithInt:_searchDao.spotfutures],@"spotfutures",
                    [NSNumber numberWithInt:_searchDao.addressId],@"regionid",
                    
//                    [self changeTimeToTimeSp:_searchDao.temptime],@"temptime",
//                    [self changeTimeToTimeSp:_searchDao.temptime1],@"temptime1",
                    [self changeTimeToTimeSp:_searchDao.temptime2],@"temptime2",
                    [self changeTimeToTimeSp:_searchDao.temptime3],@"temptime3",
                    
                    _searchDao.pricefrom,@"pricefrom",
                    _searchDao.priceto,@"priceto",
                    
                    nil];
        
        
        
        return _postDic;

    }else if(_searchDao.brandid == 1){
        _postDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    
//                    [NSNumber numberWithInt:_searchDao.brandid],@"brandid",
                    [NSNumber numberWithInt:_searchDao.brandid],@"nameofpart",
                    
                    [NSNumber numberWithInt:_searchDao.dealstatus],@"dealstatus",
               
                    [NSNumber numberWithInt:_searchDao.cargobonded],@"cargobonded",
                    
//                    [NSNumber numberWithInt:_searchDao.spotfutures],@"spotfutures",
                    [NSNumber numberWithInt:_searchDao.addressId],@"regionid",
                    //                [NSNumber numberWithInt:_searchDao.dispatching],@"dispatching",
                    
                    [self changeTimeToTimeSp:_searchDao.temptime],@"temptime",
                    [self changeTimeToTimeSp:_searchDao.temptime1],@"temptime1",
                    [self changeTimeToTimeSp:_searchDao.temptime2],@"temptime2",
                    [self changeTimeToTimeSp:_searchDao.temptime3],@"temptime3",
                    
                    [NSNumber numberWithInt:_searchDao.direction],@"direction",
                    _searchDao.pricefrom,@"pricefrom",
                    _searchDao.priceto,@"priceto",
                    //                -1,@"dealstatus",
                    
                    nil];
        
        
        
        return _postDic;

    }else if(_searchDao.brandid == 2){
        
        _postDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    
//                    [NSNumber numberWithInt:_searchDao.brandid],@"brandid",
                    [NSNumber numberWithInt:_searchDao.brandid],@"nameofpart",
                    [NSNumber numberWithInt:_searchDao.direction],@"direction",
                    [NSNumber numberWithInt:_searchDao.dealstatus],@"dealstatus",
                    [NSNumber numberWithInt:_searchDao.spotfutures],@"spotfutures",
                    [NSNumber numberWithInt:_searchDao.addressId],@"regionid",
                    [NSNumber numberWithInt:_searchDao.dispatching],@"dispatching",
//                    [self changeTimeToTimeSp:_searchDao.temptime],@"temptime",
//                    [self changeTimeToTimeSp:_searchDao.temptime1],@"temptime1",
                    [self changeTimeToTimeSp:_searchDao.temptime2],@"temptime2",
                    [self changeTimeToTimeSp:_searchDao.temptime3],@"temptime3",
                    
                    _searchDao.pricefrom,@"pricefrom",
                    _searchDao.priceto,@"priceto",
                    
                    nil];

        
        
        return _postDic;

    }else{
        
        _postDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    
//                    [NSNumber numberWithInt:_searchDao.brandid],@"brandid",
                    [NSNumber numberWithInt:_searchDao.brandid],@"nameofpart",
                    
                    [NSNumber numberWithInt:_searchDao.dealstatus],@"dealstatus",

                    [NSNumber numberWithInt:_searchDao.cargobonded],@"cargobonded",
//                    [NSNumber numberWithInt:_searchDao.spotfutures],@"spotfutures",
                    [NSNumber numberWithInt:_searchDao.addressId],@"regionid",
                    //                [NSNumber numberWithInt:_searchDao.dispatching],@"dispatching",
                    
                    [self changeTimeToTimeSp:_searchDao.temptime],@"temptime",
                    [self changeTimeToTimeSp:_searchDao.temptime1],@"temptime1",
                    [self changeTimeToTimeSp:_searchDao.temptime2],@"temptime2",
                    [self changeTimeToTimeSp:_searchDao.temptime3],@"temptime3",
                    
                    [NSNumber numberWithInt:_searchDao.direction],@"direction",
                    _searchDao.pricefrom,@"pricefrom",
                    _searchDao.priceto,@"priceto",
                    nil];
        
        
        
        return _postDic;

    }

    
}


- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}











@end
