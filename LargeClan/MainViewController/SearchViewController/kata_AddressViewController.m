//
//  kata_AddressViewController.m
//  LargeClan
//
//  Created by kata on 13-11-27.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_AddressViewController.h"
#import "KATAUtils.h"
#import "ASIFormDataRequest.h"
#import "KATAConstants.h"

#import "NSString+MD5.h"
#import "KATAAddress.h"
@interface kata_AddressViewController ()

{
    UITableView *_tableView;
    
    int _offerType;
    
    int _addressType;
    
}
@property(nonatomic,retain)NSDictionary *getDic;

@property(nonatomic,retain)NSMutableArray *getArr;

@end

@implementation kata_AddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        
    }
    return self;
}


- (id)initWithOfferType:(int)aOfferType addressType:(int)aAddressType
{
    if (self = [super init]) {
        
        
        _offerType = aOfferType;
        
        _addressType = aAddressType; 
        
    }
    
    return self;
}

- (void)dealloc {
    
    [_getArr release];
    
    [_tableView release];
    
    [super dealloc ];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self.navigationItem setHidesBackButton:YES];
    
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@",SERVER_URI];

    ASIFormDataRequest *request = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];

    
    request.timeOutSeconds = 20;
    
    request.delegate  = self;
    
    [request startAsynchronous];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self beginingView];
   
    
    _getArr  = [[NSMutableArray alloc]init];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, ScreenWidth, ScreenHeight - 49 - 44) style:UITableViewStyleGrouped];
    
    _tableView.backgroundView = nil;
    
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];

    

}

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_placemessage_get";
}

- (NSDictionary *)params
{
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",_offerType];
    
   _getDic = [[[NSDictionary alloc]initWithObjectsAndKeys:
              nameofpart,@"nameofpart",
              nil] autorelease];
    
    
    return _getDic;
    
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark - custom
-(void)beginingView
{
    self.title = @"交割地";
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
}



-(void)backClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma tableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.getArr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static  NSString *cellIdtifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifier];
    
    if (!cell)
    {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifier] autorelease];
        
        
        
        
    }
    
    if ([self.getArr count] != 0) {
        
        KATAAddress  *address= [self.getArr objectAtIndex:indexPath.row];
        
        cell.textLabel.text = address.addressName;
    }
    
  

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    KATAAddress  *address= [self.getArr objectAtIndex:indexPath.row];


    
    if ([self.addressDelegate respondsToSelector:@selector(kata_AddressViewControllerWithOfferType:addressType:AddressName:addressId:)]) {
        
        [self.addressDelegate kata_AddressViewControllerWithOfferType:_offerType addressType:_addressType AddressName:address.addressName addressId:address.addressId];
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



#pragma mark - ASIHttpRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    DebugLog(@"request ==  %@",[request responseString]);
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    if ([[dic objectForKey:@"code"] isEqualToString:@"0"])  {
        
        
        NSDictionary *dataDic = [dic objectForKey:@"data"];
        
        
        NSString *code = [dataDic objectForKey:@"code"];

        
        if (_addressType ==  1000 && _offerType == 0) {   //
            
            NSArray *regions= [dataDic objectForKey:@"regions"];
            
            [self regions:regions];
            
        }
        
        if (_addressType ==  1000 && _offerType == 1) {   //
            
            NSArray *regions= [dataDic objectForKey:@"regions"];
            
            [self regions:regions];
            
        }

        if (_addressType ==  1001 && _offerType == 1) {   //
            
            NSArray  *goodsSources = [dataDic objectForKey:@"goodsSources"];
            
            [self goodsSources:goodsSources];
            
        }
        
        
        if (_addressType ==  1000 && _offerType == 2) {   //
            
            NSArray *regions= [dataDic objectForKey:@"regions"];
            
            [self regions:regions];
            
        }
        
        if (_addressType ==  1002 && _offerType == 2) {   //
            
            NSArray  *brands = [dataDic objectForKey:@"brands"];

            [self brands:brands];
        
        }
        


        
        
        if (_addressType ==  1000 && _offerType == 3) {   //
            
            NSArray *regions= [dataDic objectForKey:@"regions"];
            
            [self regions:regions];
            
        }
        
        if (_addressType ==  1001 && _offerType == 3) {   //
            
            NSArray  *goodsSources = [dataDic objectForKey:@"goodsSources"];
            
            [self goodsSources:goodsSources];
            
        }
        if (_addressType ==  1002 && _offerType == 3) {   //
            
            NSArray  *brands = [dataDic objectForKey:@"brands"];

                [self brands:brands];
        }


       
        
//        NSArray  *brands = [dataDic objectForKey:@"brands"];
//        
//        [self brands:brands];
//        
        
//        NSArray  *goodsSources = [dataDic objectForKey:@"goodsSources"];
//
//        [self goodsSources:goodsSources];
//       
//        
//        NSArray *regions= [dataDic objectForKey:@"regions"];
//        
//        [self regions:regions];
//        
        
        [_tableView reloadData];

        
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    
}


#pragma mark - 

- (void)brands:(NSArray *)aBrands
{
    
    
    for (int i = 0; i < [aBrands count]; i++) {
        
        
        
        KATAAddress *address  = [[KATAAddress alloc]init];
        
        
        NSDictionary *dic = [aBrands objectAtIndex:i];
        
        NSString *brandsName = [dic   objectForKey:@"brandname"];
        
        address.addressName = brandsName;
        
        NSString *brandsid= [dic objectForKey:@"id"];
        
        address.addressId = brandsid;
        
        [self.getArr addObject:address];
        
        [address release];
        
        
    }

}


- (void)goodsSources:(NSArray *)aGoodsSources
{
    
    for (int i = 0; i < [aGoodsSources count]; i++) {
        
        
        
        KATAAddress *address  = [[KATAAddress alloc]init];
        
        
        NSDictionary *dic = [aGoodsSources objectAtIndex:i];
        
        NSString *goodsSourcename = [dic   objectForKey:@"goodsSourcename"];
        
        address.addressName = goodsSourcename;
        
        NSString *goodsSourceid= [dic objectForKey:@"id"];
        
        address.addressId = goodsSourceid;
        
        [self.getArr addObject:address];
        
        [address release];
        
        
    }

}

-(void)regions:(NSArray *)aRegions
{
    for (int i = 0; i < [aRegions count]; i++) {
        
        
        
        KATAAddress *address  = [[KATAAddress alloc]init];
        
        
        NSDictionary *dic = [aRegions objectAtIndex:i];
        
        NSString *regions = [dic   objectForKey:@"regionname"];
        
        address.addressName = regions;
        
        NSString *addressId = [dic objectForKey:@"id"];
        
        address.addressId = addressId;
        
        [self.getArr addObject:address];
        
        [address release];
        
        
    }
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
