//
//  kata_TwoTableView.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_TwoTableView.h"
#import "KATAUtils.h"

#import "kata_LeftTableDelegate.h"

#import "kata_RightTableDelegate.h"
#import "KATAConstants.h"
#import "KATAConstants.h"
@interface kata_TwoTableView ()
{
    
    UIScrollView *_scrollView;
}
@property(nonatomic,retain)NSMutableArray *rightHeaderArr;

@property(nonatomic,retain)NSMutableArray *insideIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *insideIndexArrayPTA;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayPTA;


@end

@implementation kata_TwoTableView

- (void)dealloc
{
    

    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    
    [_scrollView release];
    
    [_rightHeaderArr release];
    
    
    [_leftDelegate release];
    
    [_rightDelegate release];
    
    
    [_insideIndexArrayPTA release];
    [_outerIndexArrayPTA release];
    [_insideIndexArrayMEG release];
    [_outerIndexArrayMEG release];
    
    
    [super dealloc];
}


- (id)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame ]) {
        
        
        _leftDelegate  = [[kata_LeftTableDelegate alloc]init];
        
        _rightDelegate = [[kata_RightTableDelegate alloc]init];
        
        
        
        _rightHeaderArr  =  [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];

    
        _insideIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];
        _outerIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"美金（CFR）",@"类型",@"货源地",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];
        
        _insideIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"人民币",@"配送",@"品牌",@"货种",@"交割时间",@"交割地",@"数量（吨）",@"报盘人",@"保证金",@"发票",@"状态",@"备注", nil];
        
        _outerIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"美金（CFR）",@"类型",@"货源地",@"品牌",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];
        
        
      _leftDelegate.leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 80, ScreenHeight) style:UITableViewStylePlain];
        
        
//        _leftDelegate.leftTableView.backgroundColor = [UIColor redColor];
        
        _leftDelegate.leftTableView.delegate = _leftDelegate;
        
        _leftDelegate.leftTableView.dataSource = _leftDelegate;
        
        
        [self addSubview:_leftDelegate.leftTableView];
        
        [_leftDelegate.leftTableView release];
        

        
        
       _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(80, 0, 220, ScreenHeight)];
        
//        scrollView.contentSize = CGSizeMake([_firstRowArrayInTableView2 count] * LABELWIDTH, 0);
        
        _scrollView.delegate = self;
        
         _scrollView.contentSize = CGSizeMake([_rightHeaderArr count] *LBLWIDTH + 20, 0);

        
        [self addSubview:_scrollView];
        
        
        
//       UITableView *rightTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [_firstRowArrayInTableView2 count] * LABELWIDTH, ScreenHeight) style:UITableViewStylePlain];
        
        
        _rightDelegate.rightTableView  = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [_rightHeaderArr count] *LBLWIDTH, ScreenHeight) style:UITableViewStylePlain];
        
        
         _rightDelegate.rightTableView.delegate = _rightDelegate;
        
         _rightDelegate.rightTableView.dataSource = _rightDelegate;
        
//         _rightDelegate.rightTableView.backgroundColor = [UIColor greenColor];
        
    
        [_scrollView addSubview: _rightDelegate.rightTableView];
        
        [ _rightDelegate.rightTableView release];
        

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(indexHeardView:) name:INDEX_HEARDVIEW object:nil];

        
        
        
        //改变表头上的数据           //改变表头上的数据
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(test:) name:SEARCH_RESULT_HEARDView object:nil];
        

        
    }
    return self;
    
}

#pragma mark - NSNotification -- INDEX_HEARDVIEW

- (void)test:(NSNotification *)notification
{
    int type = [[notification object] intValue] ;
    
    [self changeType:type];
    
}


#pragma mark - NSNotification -- INDEX_HEARDVIEW

- (void)indexHeardView:(NSNotification *)notification
{
    if ([notification object] != nil) {
        
        int type = [[notification object] intValue] - 1;
        
        [self changeType:type];
        
      
    }
    
    
}


- (void)changeType:(int)aType

{
 
    switch (aType) {
        case 0:
        {
            
            [self changeFrameAndContentSize:_insideIndexArrayMEG];
            
        }
            break;
        case 1:
        {
            [self changeFrameAndContentSize:_outerIndexArrayMEG];
            
        }
            break;
        case 2:
        {
            [self changeFrameAndContentSize:_insideIndexArrayPTA];
            
            
        }
            break;
        case 3:
        {
            [self changeFrameAndContentSize:_outerIndexArrayPTA];
            
        }
            break;
            
        default:
            break;
    }

}


- (void)changeFrameAndContentSize:(NSMutableArray *)aArray
{
    
    [_rightHeaderArr removeAllObjects];
    
    [_rightHeaderArr addObjectsFromArray:aArray];
    
    _scrollView.contentSize = CGSizeMake([_rightHeaderArr count] *LBLWIDTH + 20, 0);
    
    _rightDelegate.rightTableView.frame =  CGRectMake(0, 0, [_rightHeaderArr count] *LBLWIDTH, ScreenHeight);
    
}


#pragma mark -uiscrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
//    _leftDelegate.leftTableView.contentOffset =  _scrollView.contentOffset;

    
//    _rightDelegate.rightTableView.contentOffset =    _leftDelegate.leftTableView.contentOffset ;
//    
    
}




@end
