//
//  kata_MyOfferPTAOutView.h
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface kata_MyOfferPTAOutView : UIView
//
//@end

#import "kata_Tools.h"

#import "kata_DatePickerDao.h"

#import "kata_DeliveryTimeCell.h"

#import "ASIHTTPRequest.h"

#import "kata_ShipTime2Cell.h"
#import "kata_ShipTime1Cell.h"

@protocol kata_MyOfferPTAOutViewDelegate <NSObject>


- (void)kata_MyOfferPTAOutViewSelectTime;

- (void)kata_MyOfferPTAOutViewPushGoodsSource:(int)aGoodsSource;

- (void)kata_MyOfferPTAOutViewPushRegion:(int)aRegion;

- (void)kata_MyOfferPTAOutViewPushBrand:(int)aBrand;


- (void)kata_MyOfferPTAOutViewSelectPassNumber;


@end


@interface kata_MyOfferPTAOutView : UIView <UITableViewDataSource,UITableViewDelegate,
    kata_ToolsDelegate,
    kata_DatePickerDaoDelegate,
    kata_DeliveryTimeCellDelegate,
    ASIHTTPRequestDelegate,
    kata_ShipTime2CellDelegate,
    kata_ShipTime1CellDelegate,
    UITextFieldDelegate>

{
    id <kata_MyOfferPTAOutViewDelegate> delegate;
    
}

@property(nonatomic,assign)    id <kata_MyOfferPTAOutViewDelegate> delegate;

@property(nonatomic,retain) UITableView *tableView;

@property(nonatomic,copy) NSString *selectGoodsSource;

@property(nonatomic,copy) NSString *selectRegion;

@property(nonatomic,copy) NSString *selectGoodsSourceId;

@property(nonatomic,copy) NSString *selectRegionId;

@property(nonatomic,copy) NSString *selectBrandName;

@property(nonatomic,copy) NSString *selectBrandId;




@property(nonatomic,assign) int selectTimeId;

@property(nonatomic,copy) NSString *selectTime;


@property(nonatomic,copy) NSString *selectHandNumName;

@property(nonatomic,assign) int selectHandNum;


@end
