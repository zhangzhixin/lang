//
//  kata_TwoTableView.h
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "kata_RightTableDelegate.h"
#import "kata_LeftTableDelegate.h"
@interface kata_TwoTableView : UIView  <UIScrollViewDelegate>

{
    
    kata_LeftTableDelegate *_leftDelegate;
    
    kata_RightTableDelegate *_rightDelegate;
}
@end
