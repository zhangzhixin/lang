//
//  kata_SearchMEGInsideView.m
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_SearchMEGInsideView.h"
#import "KATAUtils.h"
#import "KATASearchDao.h"
#import "kata_DeliveryTimeCell.h"

#import "kata_SearchCell.h"

#import "kata_QueryResultViewController.h"
#import "kata_AddressViewController.h"
@interface kata_SearchMEGInsideView ()

{
    UIDatePicker *_picker;
    
    BOOL isPicker;
    
    
    int _plateType;

}
@property(nonatomic,retain) KATASearchDao *searchDao;

@property(nonatomic,retain) UITableView *tableView;


@property(nonatomic,retain) NSMutableArray *MEGInsideArray;


@property(nonatomic,retain)NSMutableArray *dataArray;

@property(nonatomic,retain)NSString *priceAtAalertStr;

@property(nonatomic,retain)NSString *dirStr;
@property(nonatomic,retain)NSString *priceStr;
@property(nonatomic,retain)NSString *goodsStr;
@property(nonatomic,retain)NSString *shipStr;
@property(nonatomic,retain)NSString *arrivalStr;
@property(nonatomic,retain)NSString *stateStr;
@property(nonatomic,retain)NSString *addressStr;


@property(nonatomic,retain)NSString *dateStr1;
@property(nonatomic,retain)NSString *dateStr2;

@property(nonatomic,retain)NSString *delivelyTimeStr;




@end

@implementation kata_SearchMEGInsideView

- (id)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame]) {
        
        
        
        
        _MEGInsideArray = [[NSMutableArray alloc]initWithObjects:
                           @"方向",@"价格",@"货种",@"交割时间",@"状态",@"交割地", nil];
        
        
        
        
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 00, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
        
        _tableView.backgroundView = nil;
        
        _tableView.delegate = self;
        
        _tableView.dataSource = self;
        
        [self addSubview:_tableView];
        
        
        _searchDao  = [[KATASearchDao alloc]init];

        
        
    
        
        
    }
    
    return self;
    
}
-(void)dealloc
{
    [_MEGInsideArray  release];
    
    [_searchDao release];
    
    [super dealloc];
    
}

#pragma mark - kata_AddressViewControllerDelegate
- (void)kata_AddressViewControllerAddressName:(NSString *)aAddressName addressId:(NSString *)aAddressId
{
    
    DebugLog(@"aAddressName == %@",aAddressName);
    
}

#pragma mark - tableViewDataSoure
-(float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 85.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *view = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 85.0)] autorelease];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(85, 20, 150, 45);
    
    [button addTarget:self action:@selector(queryBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [button setImage:[UIImage imageNamed:@"but.png"] forState:UIControlStateNormal];
    
    [view addSubview:button];
    
    
    return view;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_MEGInsideArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static  NSString *cellIdtifierMEGInside = @"CellMEGInside ";
    
    static  NSString *cellIdtifierMEGInsideTime = @"CellMEGInsideTime";
    
    
    int row = indexPath.row;
    
   
    if (row == 3) {
            
            kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInsideTime];
            
            if (!cell) {
                
                cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInsideTime] autorelease];
                
                
//                cell.delegate = self;
                
            }
            
        UILabel *nameLab  = (UILabel *)[cell viewWithTag:11];
        
        nameLab.text = [_MEGInsideArray objectAtIndex:indexPath.row];
//            UIButton *btn1 = (UIButton *)[cell viewWithTag:1];
//            
//            btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
//            
//            [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//            
//            [btn1 setTitle:self.dateStr1 forState:UIControlStateNormal];
//            
//            
//            UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
//            
//            
//            btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
//            
//            [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//            
//            [btn2 setTitle:self.dateStr2 forState:UIControlStateNormal];
        
        
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        }else{
            
            kata_SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInside];
            
            
            if (!cell) {
                
                cell = [[[kata_SearchCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInside] autorelease];
                
                
                
            }
            
            UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
            
            nameLab.text = [_MEGInsideArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == ([_MEGInsideArray count] - 1) )
            {
                
                cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            
            UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
            
            textFiled.text = [_MEGInsideArray objectAtIndex:indexPath.row];
            
            
            if ([textFiled.text isEqualToString:@"方向"])
            {
                textFiled.text = self.dirStr;
            }
            if ([textFiled.text isEqualToString:@"价格"])
            {
                textFiled.text = self.priceAtAalertStr;
            }
            if ([textFiled.text isEqualToString:@"货种"])
            {
                
                textFiled.text = @"";
                textFiled.text = self.goodsStr;
            }
            if ([textFiled.text isEqualToString:@"交割时间"])
            {
                textFiled.text = nil;
            }
            if ([textFiled.text isEqualToString:@"状态"])
            {
                textFiled.text = self.stateStr;
            }
            if ([textFiled.text isEqualToString:@"交割地"])
            {
                
                textFiled.text = self.addressStr;
            }
            
            
            
            
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
            
        }
        
        
}

#pragma tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *str = [_MEGInsideArray   objectAtIndex:indexPath.row];
    
    
    if ([str isEqualToString:@"交割时间"])
    {
        int delivelyTimeTag = 11;
        [self pickerShow:delivelyTimeTag];
        
        
    }

    
    if ([str isEqualToString:@"装船时间"])
    {
        int time1Tag = 1;
        [self pickerShow:time1Tag];
        
    }
    if ([str isEqualToString:@"到港时间"])
    {
        int time2Tag = 2;
        
        [self pickerShow:time2Tag];
        
    }
    
    
    
    if ([str isEqualToString:@"方向"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择方向" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"卖盘",@"买盘", nil];
        
        dirSheet.tag = 21;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self];
        
        [dirSheet release];
        
        
    }
    if ([str isEqualToString:@"货种"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择货种" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"期货",@"现货", nil];
        
        
        dirSheet.tag = 22;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self];
        
        [dirSheet release];
        
        
    }
    if ([str isEqualToString:@"类型"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择类型" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"船税",@"保税", nil];
        
        dirSheet.tag = 24;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self];
        
        [dirSheet release];
        
        
    }
    
    if ([str isEqualToString:@"状态"])
    {
        
        DebugLog(@"alert");
        
        UIActionSheet *dirSheet = [[UIActionSheet alloc]initWithTitle:@"选择状态" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"成交",@"未成交", nil];
        
        dirSheet.tag = 23;
        
        dirSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        
        [dirSheet showInView:self];
        
        [dirSheet release];
        
        
    }
    
    
    
    if ([str isEqualToString:@"交割地"])
    {
        
        if ([self.delegate respondsToSelector:@selector(pushAddress)]) {
            
            [self.delegate pushAddress];
        }

   
    }
    
    if ([str isEqualToString:@"价格"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"选择价格" message:@"\n" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        
        
        UITextField *priceText = [[UITextField alloc]initWithFrame:CGRectMake(10, 45, 264, 30)];
        
        //        priceText.backgroundColor = [UIColor redColor];
        
        priceText.borderStyle = UITextBorderStyleRoundedRect;
        
        //        priceText.text = self.priceAtAalertStr;
        
        priceText.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        priceText.delegate = self;
        
        priceText.tag = 1;
        
        [alert addSubview:priceText];
        
        [priceText release];
        
        alert.delegate = self;
        
        [alert show];
        
        [alert release];
        
    }
    
    
}

  
#pragma mark - actionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex == %d",buttonIndex);
    
    switch (actionSheet.tag) {
        case 21:  //方向
        {
            if (buttonIndex == 0)
            {
                self.dirStr =  @"卖盘";
                
                _searchDao.direction  = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
                self.dirStr =  @"买盘";
                
                _searchDao.direction  = 1;
                
                
                [_tableView reloadData];
                
            }
        }
            break;
        case 22:  // 货种
        {
            
            if (buttonIndex == 0)
            {
                self.goodsStr =  @"期货";
                
                _searchDao.spotfutures = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
                self.goodsStr =  @"现货";
                
                _searchDao.spotfutures = 1;
                
                
                [_tableView reloadData];
                
            }
            
        }
            break;
        case 23:  //状态
        {
            if (buttonIndex == 0)
            {
                self.stateStr =  @"成交";
                
                _searchDao.dealstatus = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
                self.stateStr =  @"未成交";
                
                
                _searchDao.dealstatus = 1;
                
                
                [_tableView reloadData];
                
            }
            
        }
            break;
            
        case 24:  //类型
        {
            if (buttonIndex == 0)
            {
                self.shipStr =  @"船税";
                
                _searchDao.cargobonded = 0;
                
                [_tableView reloadData];
                
            }
            else
            {
                self.shipStr =  @"保税";
                
                _searchDao.cargobonded = 1;
                
                [_tableView reloadData];
                
            }
            
        }
            break;
            
        default:
            break;
    }
    
}
#pragma mark - pickerDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:1];
    
    
    _picker.frame = CGRectMake(0, ScreenHeight + 120, ScreenWidth, 120);
    
    
    [UIView commitAnimations];
    
    
    
}

#pragma mark - alertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    
    [_tableView reloadData];
    
    
    
    DebugLog(@"alertView.frame == %@ ",NSStringFromCGRect(alertView.frame));
}

#pragma mark -  textFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    DebugLog(@"textField = %@",textField);
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    DebugLog(@"textField  == %@",textField.text);
    
    
    self.priceAtAalertStr = textField.text;
    
    return YES;
    
    
    
}

#pragma mark - kata_AddressViewControllerDelegate

//- (void)kata_AddressViewControllerAddressName:(NSString *)aAddressName
//{
//    
//    self.addressStr = aAddressName;
//    
//    [_tableView reloadData];
//    
//}


#pragma mark - pickerShow
- (void)pickerShow:(int)tag
{
    if (isPicker == NO)
    {
        isPicker = YES;
        
        _picker  = [[UIDatePicker alloc]init];
        
        _picker.frame = CGRectMake(0,ScreenHeight, ScreenWidth, 216);
        
        _picker.datePickerMode = UIDatePickerModeDate;
        
        _picker.tag = tag;
        
        [_picker addTarget:self action:@selector(changeTime:) forControlEvents:UIControlEventValueChanged];
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:1];
        
        //        [UIView setAnimationDelegate:self];
        //
        //        [UIView setAnimationDidStopSelector:@selector(animationDidStop:
        //                                                      finished:
        //                                                      context:)];
        
        _picker.frame = CGRectMake(0, ScreenHeight - 216 - 49, ScreenWidth, 216);
        
        
        
        [UIView commitAnimations];
        
        [self addSubview:_picker];
        
    }
    
}




#pragma mark - queryBtnClick

-(void)queryBtnClick
{
    DebugLog(@"查询");
    

    if ([self.delegate respondsToSelector:@selector(push)]) {
        
        [self.delegate push];
        
        
    }
    
    
    
    
}







@end
