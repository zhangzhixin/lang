//
//  kata_MyMEGInsideView.h
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface kata_MyMEGInsideView : UIView
//
//@end

#import "kata_segView.h"
#import "ASIHTTPRequest.h"
#import "kata_LeftTableDelegate.h"

@interface kata_MyMEGInsideView : UIView <kata_segViewDelegate,UITableViewDataSource,UITableViewDelegate>

@end
