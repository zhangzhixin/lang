//
//  kata_MyOfferMEGInsideView.m
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_MyOfferMEGInsideView.h"

#import "KATAConstants.h"

#import "kata_AddressViewController.h"

#import "kata_Tools.h"

#import "kata_SendMyOffer.h"

#import "KATAUtils.h"

#import "kata_RemarkCell.h"

#import "kata_DeliveryTimeCell.h"

#import "kata_DatePickerDao.h"

#import "NSString+MD5.h"

#import "ASIFormDataRequest.h"

#import "MBProgressHUD.h"

#import "kata_UserViewController.h"

#import "kata_MyOfferViewController.h"

@interface kata_MyOfferMEGInsideView ()

{
    kata_Tools *_tools;
    
    int _tag;
    
    kata_DatePickerDao *_datePickerDao;
    
//    kata_SendMyOffer *_sendMyOffer;
}


@property(nonatomic,assign) int notfaze;
@property(nonatomic,assign) int offerpeople;
@property(nonatomic,assign) int direction;
@property(nonatomic,assign) int spotfutures;
@property(nonatomic,assign) int regionid;
@property(nonatomic,assign) int cautionmoney;
@property(nonatomic,assign) int number;
@property(nonatomic,assign) int invoice;
@property(nonatomic,assign) int storagetime;
@property(nonatomic,assign) int validtime;
//@property(nonatomic,assign) int addressId;

@property(nonatomic,retain) NSString *priceStr;

@property(nonatomic,retain) NSString *remarkStr;

@property(nonatomic,retain) kata_SendMyOffer *sendMyOffer;


@property(nonatomic,retain)NSString *sheetTitle;

@property (nonatomic,assign) int  sheetIndex;

@property(nonatomic,retain)NSArray *myOfferMEGInsideArray;

@property(nonatomic,retain)NSString *time1;

@property(nonatomic,retain)NSString *time2;

@property(nonatomic,retain)NSString *timeSp1;

@property(nonatomic,retain)NSString *timeSp2;

@property(nonatomic,retain)NSMutableDictionary *postDic;

@end

@implementation kata_MyOfferMEGInsideView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        

        _myOfferMEGInsideArray  = [[NSArray alloc]initWithObjects:
                              @"勿扰标记", @"报盘人信息", @"交易方向", @"人民币价格", @"期货类型", @"交割时间", @"交割地", @"保证金", @"数量", @"发票",@"免仓时间",@"备注",@"过期时间",
                              nil];
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 44 - 40) style:UITableViewStyleGrouped];


        _tableView.delegate = self;

        _tableView.dataSource = self;

        _tableView.backgroundView = nil;

        [self addSubview:_tableView];
        

        
        
    }
    return self;
}

- (void)dealloc
{
    
    [_postDic release];
    
    [_timeSp1 release];
    
    [_timeSp2 release];

    [_postDic release];
    
    [_selectTime release];
    
    [_addressId release];
    
    [_addressName release];
    
    
    [_time1 release];
    
    [_time2 release];
    
    [_sheetTitle release];
    
    [_tableView release];

    [_myOfferMEGInsideArray release];
    
    [super dealloc];
}



#pragma mark - tableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 100.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *vi = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 120)] autorelease];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.frame = CGRectMake(80, 20, 180, 40);
    
    [button addTarget:self action:@selector(submitMyOffer:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button setTitle:@"发布报盘" forState:UIControlStateNormal];
    
    
    [vi  addSubview:button];
    
    
    
    return vi;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [_myOfferMEGInsideArray count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    int sec = indexPath.section;
    
    int row = indexPath.row;
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",sec,row];


      if (row == 3 || row == 11) { //价格

        kata_RemarkCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];


        if (!cell) {

            cell = [[[kata_RemarkCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier tag:row] autorelease];
            
            

        }

        UILabel *nameLabel = (UILabel *) [cell.contentView viewWithTag:1];

        nameLabel.text = [_myOfferMEGInsideArray objectAtIndex:indexPath.row];

          UITextField *showTextField = (UITextField *) [cell.contentView  viewWithTag:row];
          
          
          if (showTextField.tag == 11) {
              
              self.remarkStr  = cell.remarkStr;
              
          }
          if (showTextField.tag == 3) {
              
              
              self.priceStr  = cell.priceStr;
          }

          
          
          
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        
        

        return cell;


    } else if (row == 5)
    {
        kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[kata_DeliveryTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            cell.tag = row;
            
            cell.delegate = self;
        }
        
        UILabel *nameLab = (UILabel *)[cell viewWithTag:11];
        
        nameLab.text = @"交割时间";
        
        
        UIButton *btn1 = (UIButton *)[cell viewWithTag:1];
        
        btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
        
        [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btn1 setTitle:self.time1 forState:UIControlStateNormal];
        
        UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
        
        
        btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
        
        [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [btn2 setTitle:self.time2 forState:UIControlStateNormal];
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }else if (row == 6)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            

            UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(10,0, 80, 44)];
            
            nameLabel.backgroundColor = [UIColor clearColor];
            
            nameLabel.adjustsFontSizeToFitWidth = YES;
            
            nameLabel.tag = 1;
            
            [cell addSubview:nameLabel];
            
            [nameLabel release];
            
            UILabel *showLabel = [[UILabel  alloc]initWithFrame:CGRectMake(90,0 , 210, 44)];
            
            showLabel.adjustsFontSizeToFitWidth = YES;
            
            showLabel.backgroundColor = [UIColor redColor];
            
            showLabel.tag  = 2;
            
            [cell.contentView addSubview:showLabel];
            
            [showLabel release];

        }
        UILabel *nameLabel = (UILabel *) [cell viewWithTag:1];
        
        nameLabel.text = [_myOfferMEGInsideArray objectAtIndex:indexPath.row];
        
        UILabel *showLabel = (UILabel *) [cell viewWithTag:2];
        
        showLabel.text = self.addressName;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;


        
    }else if (row == 12)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            
            UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(10,0, 80, 44)];
            
            nameLabel.backgroundColor = [UIColor clearColor];
            
            nameLabel.adjustsFontSizeToFitWidth = YES;
            
            nameLabel.tag = 1;
            
            [cell addSubview:nameLabel];
            
            [nameLabel release];
            
            UILabel *showLabel = [[UILabel  alloc]initWithFrame:CGRectMake(90,0 , 210, 44)];
            
            showLabel.adjustsFontSizeToFitWidth = YES;
            
            showLabel.backgroundColor = [UIColor redColor];
            
            showLabel.tag  = 2;
            
            [cell.contentView addSubview:showLabel];
            
            [showLabel release];
            
        }
        UILabel *nameLabel = (UILabel *) [cell viewWithTag:1];
        
        nameLabel.text = [_myOfferMEGInsideArray objectAtIndex:indexPath.row];
        
        UILabel *showLabel = (UILabel *) [cell viewWithTag:2];
        
        showLabel.text = self.selectTime;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
        
        
    }

    
      else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];


        if (!cell) {

            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];

            UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(10,0, 80, 44)];

            nameLabel.backgroundColor = [UIColor clearColor];

            nameLabel.adjustsFontSizeToFitWidth = YES;

            nameLabel.tag = 1;

            [cell addSubview:nameLabel];

            [nameLabel release];

            UILabel *showLabel = [[UILabel  alloc]initWithFrame:CGRectMake(90,0 , 210, 44)];

            showLabel.adjustsFontSizeToFitWidth = YES;

            showLabel.backgroundColor = [UIColor redColor];

            showLabel.tag  = 2;

            [cell.contentView addSubview:showLabel];

            [showLabel release];


        }

        UILabel *nameLabel = (UILabel *) [cell viewWithTag:1];

        nameLabel.text = [_myOfferMEGInsideArray objectAtIndex:indexPath.row];

        UILabel *showLabel = (UILabel *) [cell viewWithTag:2];

        if (row == _tag) {
            
            showLabel.text = self.sheetTitle;

        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];


        return cell;

    }

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


   
    switch (indexPath.row) {
        case 0:
        {
          _tools= [[kata_Tools alloc] initWithMyTitle:@"勿扰标记" oneTile:@"此报盘不显示撮合勿扰标记" twoTile:@"此报盘显示撮合勿扰标记" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:0];

            _tools.delegate = self;

        }
            break;
        case 1:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"报盘人信息" oneTile:@" 网站公布,仅会员可见" twoTile:@"委托第三方撮合" threeTile:@"仅通过短信,QQ或由客服提供" foureTile:@"仅通过短信,QQ或由客服提供,隐藏公司名称" fiveTile:nil sixTile:nil view:self tag:1];
            
            _tools.delegate = self;
        }
            break;
        case 2:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"交易方向" oneTile:@"卖盘" twoTile:@"买盘" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:2];
            
            _tools.delegate = self;
        }
            break;
        case 3:
        {

        }
            break;
        case 4:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"期货类型" oneTile:@"期货" twoTile:@"现货" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:4];
            
            _tools.delegate = self;
        }
            break;
        case 5:  //交割时间
        {

//                if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGInsideViewDelegatePush)])
//                {
//
//                    [self.delegate kata_MyOfferMEGInsideViewDelegatePush];
//                    
//                    
//                }

            
        }
            break;
        case 6:  // 交割地
        {
            
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGInsideViewPushGoodsSource:)]) {
                
                
                [self.delegate  kata_MyOfferMEGInsideViewPushGoodsSource:0];
                
            }

            
            
        }
            break;
        case 7:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"保证金" oneTile:@"0" twoTile:@"5%" threeTile:@"10%" foureTile:nil fiveTile:nil sixTile:nil view:self tag:7];
            
            _tools.delegate = self;
            
        }
            break;
        case 8:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"数量" oneTile:@"500" twoTile:@"1000" threeTile:@"1500" foureTile:@"2000" fiveTile:@">2000" sixTile:@"<500" view:self tag:8];
            
            _tools.delegate = self;
            
        }
            break;
        case 9:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"发票" oneTile:@"本月" twoTile:@"下月" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:9];
            
            _tools.delegate = self;
            
        }
            break;
        case 10:
        {
            _tools= [[kata_Tools alloc] initWithMyTitle:@"免仓时间" oneTile:@"5天" twoTile:@"7天" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:10];
            
            _tools.delegate = self;

        }
            break;
        case 11:
        {
            
        }
            break;

        case 12:
        {
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGInsideViewDelegatePushSelectTime)]) {
                
                [self.delegate kata_MyOfferMEGInsideViewDelegatePushSelectTime];
                
            }

        }
            break;

        default:
            break;
    }



}


#pragma mark - kata_ToolsWithSendMyOffer

- (void)kata_ToolsWithSendMyOffer:(kata_SendMyOffer *)akata_SendMyOffer tag:(int)aTag
{

   [_tools release];

    self.sendMyOffer  = (kata_SendMyOffer *)akata_SendMyOffer;

    self.sheetTitle = _sendMyOffer.sheetSelectTitle;
    
    self.sheetIndex = _sendMyOffer.sheetSelectIndex;

    DebugLog(@"sendMyOffer ==  %@  sheetIndex = %d",_sendMyOffer.sheetSelectTitle,_sendMyOffer.sheetSelectIndex);
    
    _tag = aTag;
    
    switch (_tag) {
        case 0:
        {
            self.notfaze =  self.sheetIndex;
        }
            break;
        case 1:
        {
            self.offerpeople =  self.sheetIndex;
        }
            break;
        case 2:
        {
            self.direction =  self.sheetIndex;
        }
            break;
        case 3:
        {
            //            _sendMyOffer.notfaze =  self.sheetIndex;
        }
            break;
        case 4:
        {
            self.spotfutures =  self.sheetIndex;
        }
            break;
        case 5:
        {
            //            _sendMyOffer.notfaze =  self.sheetIndex;
        }
            break;
        case 6:
        {
            
            int addressId = [[NSString stringWithFormat:@"%@",self.addressId] intValue];
            
            self.sendMyOffer.addressId =  addressId;
            
        }
            break;
        case 7:
        {
            self.cautionmoney  =  self.sheetIndex;
        }
            break;
        case 8:
        {
            self.number =  self.sheetIndex;
        }
            break;
        case 9:
        {
            self.invoice =  self.sheetIndex;
        }
            break;
        case 10:
        {
            self.storagetime =  self.sheetIndex;
        }
            break;
        case 11:
        {
            //            _sendMyOffer.notfaze =  self.sheetIndex;
        }
            break;
        case 12:
        {
//            self.validtime =  self.sheetIndex;
        }
            break;
            
            
        default:
            break;
    }

    
    
    
    [_tableView reloadData];
    

}


#pragma mark -kata_DeliveryTimeCellDelegate

- (void)kata_DeliveryTimeCellMyOfferPickerShow:(int)aTag
{
    
   _datePickerDao = [[kata_DatePickerDao alloc]init];
    
    _datePickerDao.delegate = self;
    
    [self addSubview:[_datePickerDao kata_DatePickerDaoDatePicker:aTag]];
    
//   self.v [datePickerDao kata_DatePickerDaoDatePicker:aTag];

//    [datePickerDao release];
    
}

#pragma mark - kata_DatePickerDaoDelegate

- (void)kata_DatePickerDaoWithTime:(NSString *)aTime tag:(int)aTag
{
    if (aTag == 1) {
        
        self.time1 = aTime;
    }else{
        
        self.time2 = aTime;
    }
    
    [_tableView reloadData];
    
}

- (void)kata_DatePickerDaoWithTimeSp:(NSString *)aTimeSp tag:(int)aTag
{
    if (aTag == 1) {
        
        self.timeSp1 = aTimeSp;
    }else{
        
        self.timeSp2 = aTimeSp;
    }
    

    
}



#pragma mark - submitMyOffer

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"save_offerUser_List";
}

- (NSMutableDictionary *)params
{
    
    
    self.postDic = [[NSMutableDictionary alloc]init];
    
    NSString *nameofpart = @"0";
    
    [_postDic setObject:nameofpart forKey:@"nameofpart"];
    
    int uid = [[NSString stringWithFormat:@"%@",[kata_UserViewController sharedUserViewController].uid] intValue];
   
    [_postDic setObject:[NSNumber numberWithInt:uid] forKey:@"userid"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.notfaze]forKey:@"notfaze"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.offerpeople]forKey:@"offerpeople"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.direction]forKey:@"direction"];
    
    int addressId =  [[NSString stringWithFormat:@"%@",self.addressId] intValue];
    
    [_postDic setObject:[NSNumber numberWithInt:addressId]forKey:@"regionid"];
        
    [_postDic setObject:[NSNumber numberWithInt:self.cautionmoney] forKey:@"cautionmoney"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.storagetime ]forKey:@"storagetime"];
    
    
    [_postDic setObject:[NSNumber numberWithInt:self.validtime]forKey:@"validtime"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.number]forKey:@"number"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.spotfutures]forKey:@"spotfutures"];

    [_postDic setObject:[NSNumber numberWithInt:self.invoice]forKey:@"invoice"];

    int price = [[NSString stringWithFormat:@"%@",self.priceStr] intValue];
    
    [_postDic setObject:[NSNumber numberWithInt:price]forKey:@"quotation"];


    
    if (self.timeSp1) {
        
        [_postDic setObject:self.timeSp1 forKey:@"deliverytime1"];

    }
    
    if (self.timeSp2) {
        
        [_postDic setObject:self.timeSp2 forKey:@"deliverytime2"];
        
    }
    


    if (self.remarkStr) {
        
        [_postDic setObject:self.remarkStr forKey:@"remarks"];

        
    }
    
    
    return _postDic;
    
}
- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}



- (void)submitMyOffer:(id)sender
{
    
    
    
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds  = 20;
    
    request.delegate = self;
    
    [request startAsynchronous];
    
    
	[MBProgressHUD showHUDAddedTo:self animated:YES];
    
    
}

#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [MBProgressHUD hideHUDForView:self animated:YES];

    DebugLog(@"request ==  %@",[request responseString]);
    
    NSDictionary *dic =[NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    

    if ([[dic objectForKey:@"code"] isEqualToString:@"0"]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        if ([[data objectForKey:@"code"] isEqualToString:@"0"]) {
            
            
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGInsideViewPushMyOffer)]) {
                
                [self.delegate kata_MyOfferMEGInsideViewPushMyOffer];
                
            }
                   
            
        }
    
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"发布报盘请求失败");
    
    
}






/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
