//
//  kata_SearchPTAInsideView.h
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol kata_SearchPTAInsideViewDelegate <NSObject>

- (void)push;


@end



@interface kata_SearchPTAInsideView : UIView  <UITableViewDelegate,UITableViewDataSource>
{
    
    id <kata_SearchPTAInsideViewDelegate> delegate;
    
}

@property(nonatomic,assign)    id <kata_SearchPTAInsideViewDelegate> delegate;

@end
