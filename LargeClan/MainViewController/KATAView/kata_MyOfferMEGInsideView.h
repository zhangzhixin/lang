//
//  kata_MyOfferMEGInsideView.h
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "kata_Tools.h"

#import "kata_DeliveryTimeCell.h"

#import "kata_DatePickerDao.h"

#import "ASIHTTPRequest.h"

@protocol kata_MyOfferMEGInsideViewDelegate <NSObject>

- (void)kata_MyOfferMEGInsideViewDelegatePush;

- (void)kata_MyOfferMEGInsideViewDelegatePushSelectTime;



////////////////////////////////////////////////////////////////
- (void)kata_MyOfferMEGInsideViewPushGoodsSource:(int)aGoodsSource;

- (void)kata_MyOfferMEGInsideViewPushMyOffer;


@end



@interface kata_MyOfferMEGInsideView : UIView <UITableViewDataSource,UITableViewDelegate,
    kata_ToolsDelegate,
    kata_DeliveryTimeCellDelegate,
    kata_DatePickerDaoDelegate,
    ASIHTTPRequestDelegate>

{
    id <kata_MyOfferMEGInsideViewDelegate> delegate;
    
}

@property(nonatomic,assign)    id <kata_MyOfferMEGInsideViewDelegate> delegate;

@property(nonatomic,retain) UITableView *tableView;

@property(nonatomic,assign) int selectTimeId;

@property(nonatomic,copy) NSString *selectTime;


@property(nonatomic,copy) NSString *addressName;

@property(nonatomic,copy) NSString *addressId;

@end
