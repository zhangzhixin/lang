//
//  kata_SearchPTAOutView.h
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol kata_SearchPTAOutViewDelegate <NSObject>

- (void)push;


@end


@interface kata_SearchPTAOutView : UIView <UITableViewDataSource,UITableViewDelegate>

{
    id <kata_SearchPTAOutViewDelegate> delegate;
}
@property(nonatomic,retain)    id <kata_SearchPTAOutViewDelegate> delegate;

@end
