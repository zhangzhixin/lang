//
//  kata_MyOfferMEGOutView.h
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "kata_Tools.h"

#import "kata_DatePickerDao.h"

#import "ASIHTTPRequest.h"

#import "kata_DeliveryTimeCell.h"

#import "kata_ShipTimeCell.h"

#import "kata_ShipTime1Cell.h"
#import "kata_ShipTime2Cell.h"

@protocol kata_MyOfferMEGOutViewDelegate <NSObject>

- (void)kata_MyOfferMEGOutViewDelegatePush;

- (void)kata_MyOfferMEGOutViewDelegatePushSelectTime;





- (void)kata_MyOfferMEGOutViewSelectPassNumber;

- (void)kata_MyOfferMEGOutViewPushGoodsSource:(int)aGoodsSource;

- (void)kata_MyOfferMEGOutViewPushRegion:(int)aRegion;


@end



@interface kata_MyOfferMEGOutView : UIView <UITableViewDataSource,UITableViewDelegate,
    kata_ToolsDelegate,
    kata_DatePickerDaoDelegate,
    UITextFieldDelegate,
    ASIHTTPRequestDelegate,
    kata_DeliveryTimeCellDelegate,
    kata_ShipTimeCellDelegate,
    kata_ShipTime2CellDelegate,
    kata_ShipTime1CellDelegate>

{
    id <kata_MyOfferMEGOutViewDelegate> delegate;
    
    
}

@property(nonatomic,assign)    id <kata_MyOfferMEGOutViewDelegate> delegate;

@property(nonatomic,retain) UITableView *tableView;




@property(nonatomic,copy) NSString *selectGoodsSource;

@property(nonatomic,copy) NSString *selectRegion;

@property(nonatomic,copy) NSString *selectGoodsSourceId;

@property(nonatomic,copy) NSString *selectRegionId;




@property(nonatomic,assign) int selectTimeId;

@property(nonatomic,copy) NSString *selectTime;


@property(nonatomic,copy) NSString *passNumberName;

@property(nonatomic,assign) int selectHandNum;



@end
