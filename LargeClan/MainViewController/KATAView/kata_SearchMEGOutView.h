//
//  kata_SearchMEGOutView.h
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol kata_SearchMEGOutViewDelegate <NSObject>

- (void)push;


@end



@interface kata_SearchMEGOutView : UIView <UITableViewDataSource,UITableViewDelegate>
{
    
    id <kata_SearchMEGOutViewDelegate> delegate;

}

@property(nonatomic,assign)    id <kata_SearchMEGOutViewDelegate> delegate;

@end
