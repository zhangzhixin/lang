//
//  kata_SearchMEGInsideView.h
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "kata_AddressViewController.h"
@protocol kata_SearchMEGInsideViewDelegate <NSObject>

- (void)push;


- (void)pushAddress;


@end




@interface kata_SearchMEGInsideView : UIView <UITableViewDataSource,UITableViewDelegate,kata_AddressViewControllerDelegate>
{
    
    id <kata_SearchMEGInsideViewDelegate> delegate;
    
}


@property(nonatomic,assign)     id <kata_SearchMEGInsideViewDelegate> delegate;


@end
