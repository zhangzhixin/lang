//
//  kata_segView.m
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_segView.h"
#import "KATAUtils.h"
#import "KATAConstants.h"
@interface kata_segView ()
{
    UIButton *_button;
    
    int _mainType;
}
//@property(nonatomic,retain)NSMutableArray *insideArrayMEG;
//@property(nonatomic,retain)NSMutableArray *outerArrayMEG;
//@property(nonatomic,retain)NSMutableArray *insideArrayPTA;
//@property(nonatomic,retain)NSMutableArray *outerArrayPTA;

//@property(nonatomic,retain)NSMutableArray *insideIndexArrayMEG;
//@property(nonatomic,retain)NSMutableArray *outerIndexArrayMEG;
//@property(nonatomic,retain)NSMutableArray *insideIndexArrayPTA;
//@property(nonatomic,retain)NSMutableArray *outerIndexArrayPTA;


@end


@implementation kata_segView




- (id)initWithFrame:(CGRect)frame typeIdentifier:(int)aTypeIdentifier
{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _mainType = aTypeIdentifier;
        
        
        NSArray *array = [NSArray arrayWithObjects:@"MEG内盘",@"MEG外盘",@"PTA内盘",@"PTA外盘", nil];

        
        
        for (int i = 0; i < 4; i ++)
        {
            _button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            _button.frame = CGRectMake(i * 80, 0, 80, 40);
            
            [_button.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
            
            _button.tag = i + 1;
            
            if (_button.tag == 1)
            {
                [_button setBackgroundImage:[UIImage imageNamed:@"nav_active.png"] forState:UIControlStateNormal];
                
            }
            else
            {
                [_button setBackgroundImage:[UIImage imageNamed:@"nav.png"] forState:UIControlStateNormal];
                
            }
            
            [_button setTitle:[array objectAtIndex:i] forState:UIControlStateNormal];
            
            
            [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            
            [_button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [self addSubview:_button];
        }
        
    }
    return self;
    
}






#pragma mark - buttonClick

-(void)btnClick:(id)sender
{
    DebugLog(@"seg 点击");
    
    UIButton *btn = (UIButton *)sender;
    
    int tag  = btn.tag;
    
    // 互斥btn
    for (UIView *subView in self.subviews)
    {
        
        if ([subView isKindOfClass: [UIButton class]])
        {
            
            UIButton *btnView = (UIButton *)subView;
            
            if (btnView.tag == btn.tag)
            {
                [btn setBackgroundImage:[UIImage imageNamed:@"nav_active.png"] forState:UIControlStateNormal];
            }
            else
            {
                [btnView setBackgroundImage:[UIImage imageNamed:@"nav.png"] forState:UIControlStateNormal];
            }
            
            
        }
    }
    
    if (_mainType == 100 || _mainType == 102 || _mainType == 202) {
        
        //主页的不同数据源
        
        [[NSNotificationCenter defaultCenter] postNotificationName:INDEX_HEARDVIEW object:[NSNumber numberWithInt:tag]];
        
       
    }
    if (_mainType == 101) {
        
        //搜索页的不同数据源
        [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_TABLEVIEWDATA object:[NSNumber numberWithInt:tag]];
        
    }
    


    
    //主页根据不同的索引， 发送不同的请求。 搜素页根据不同的索引，显示不同的数据
    if ([self.delegate respondsToSelector:@selector(changeIndex:)]) {
        
        [self.delegate changeIndex:btn.tag];
        
    }

    
}

@end
