//
//  kata_MyOfferMEGOutView.m
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_MyOfferMEGOutView.h"


#import "KATAConstants.h"

#import "kata_AddressViewController.h"

#import "kata_Tools.h"

#import "kata_SendMyOffer.h"

#import "KATAConstants.h"

#import "KATAUtils.h"

#import "kata_RemarkCell.h"

#import "kata_DatePickerDao.h"

//#import "kata_SendMyOfferRequest.h"

#import "kata_DeliveryTimeCell.h"


#import "ASIFormDataRequest.h"

#import "MBProgressHUD.h"


#import "kata_ShipTimeCell.h"

#import "kata_UserViewController.h"

#import "NSString+MD5.h"

#import "kata_ShipTime1Cell.h"

#import "kata_ShipTime2Cell.h"


@interface kata_MyOfferMEGOutView ()

{
    kata_Tools *_tools;
    
    int _tag;
    
    kata_DatePickerDao *_datePickerDao;

    
}

@property(nonatomic,assign) int notfaze;
@property(nonatomic,assign) int offerpeople;
@property(nonatomic,assign) int direction;
@property(nonatomic,assign) int spotfutures;
@property(nonatomic,assign) int regionid;
@property(nonatomic,assign) int cautionmoney;
@property(nonatomic,assign) int number;
@property(nonatomic,assign) int invoice;
@property(nonatomic,assign) int storagetime;
@property(nonatomic,assign) int validtime;
@property (nonatomic,assign) int  sheetIndex;
@property (nonatomic,assign) int  detailedlist;
@property (nonatomic,assign) int  paymentmethods;
@property (nonatomic,assign) int  cargobonded;




@property(nonatomic,retain) kata_SendMyOffer *sendMyOffer;

@property(nonatomic,retain)NSArray *myOfferMEGOutArray;

@property (nonatomic,retain) NSString *sheetTitle;

@property(nonatomic,retain)NSString *time1;

@property(nonatomic,retain)NSString *time2;

@property(nonatomic,retain)NSString *timeSp1;

@property(nonatomic,retain)NSString *timeSp2;


@property(nonatomic,retain)NSString *time3;

@property(nonatomic,retain)NSString *time4;

@property(nonatomic,retain)NSString *timeSp3;

@property(nonatomic,retain)NSString *timeSp4;


@property(nonatomic,retain) NSString *priceStr;

@property(nonatomic,retain) NSString *remarkStr;

@property(nonatomic,retain) NSString *handNum;

@property(nonatomic,retain)NSString *surrenderdocuments;


@property(nonatomic,retain)NSMutableDictionary *postDic;

@end

@implementation kata_MyOfferMEGOutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _myOfferMEGOutArray  = [[NSArray alloc]initWithObjects:
                                   @"勿扰标记",@"报盘人信息", @"交易方向", @"美金(CFR)", @"船货/保税", @"货源地", @"预计装船时间", @"预计到港时间", @"转手", @"交单",@"交割地",@"数量(吨)",@"清单可借",@"付款方式",@"备注(限填10个字)",@"过期时间",
                                   nil];
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 44 - 40) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        
        _tableView.dataSource = self;
        
        _tableView.backgroundView = nil;
        
        [self addSubview:_tableView];
        
             
    }
    return self;
}

- (void)dealloc
{
    
    [_postDic release];
    
    [_handNum release];
    
    [_priceStr release];
    
    [_remarkStr release];
    
    [_time1 release];
    
    [_time2 release];
    
    [_timeSp1 release];
    
    [_timeSp2 release];
    
    [_time3 release];
    
    [_time4 release];
    
    [_timeSp3 release];
    
    [_timeSp4 release];
    
    [_sheetTitle release];
    
    [_tableView release];
    
    [_myOfferMEGOutArray release];
    
    [super dealloc];
}



#pragma mark - tableViewDelegate



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 100.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *vi = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 120)] autorelease];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.frame = CGRectMake(80, 20, 180, 40);
    
    [button addTarget:self action:@selector(submitMyOffer:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button setTitle:@"发布报盘" forState:UIControlStateNormal];
    
    
    [vi  addSubview:button];
    
    
    
    return vi;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_myOfferMEGOutArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int sec = indexPath.section;
    
    int row = indexPath.row;
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",sec,row];
    
    
    if (row == 3 || row == 14) { //价格  和 备注
        
        kata_RemarkCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[kata_RemarkCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier tag:row] autorelease];
            
        }
        
        UILabel *nameLabel = (UILabel *) [cell.contentView viewWithTag:1];
        
        nameLabel.text = [_myOfferMEGOutArray objectAtIndex:indexPath.row];
        
         
         UITextField *showTextField = (UITextField *) [cell.contentView  viewWithTag:row];
         
        if (showTextField.tag == 3) {
            
            
            self.priceStr  = cell.priceStr;
            
        }

         if (showTextField.tag == 14) {
             
             self.remarkStr  = cell.remarkStr;
            
         }
       
         
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
         
        return cell;
        
        
     } else if (row == 5 || row == 10  || row == 15 || row == 8)
     {
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         
         
         if (!cell) {
             
             cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
    
             
             UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(0,0, 80, 44)];
             
             nameLabel.backgroundColor = [UIColor clearColor];
             
             nameLabel.adjustsFontSizeToFitWidth = YES;
             
             nameLabel.tag = 1;
             
             [cell.contentView addSubview:nameLabel];
             
             [nameLabel release];
             
             UILabel *showLabel = [[UILabel  alloc]initWithFrame:CGRectMake(80,0, 220, 44)];
             
             showLabel.backgroundColor = [UIColor clearColor];
             
             showLabel.adjustsFontSizeToFitWidth = YES;
             
             showLabel.tag = row;
             
             [cell.contentView addSubview:showLabel];
             
             [showLabel release];
             

         }
         
         UILabel *nameLabel = (UILabel *) [cell.contentView viewWithTag:1];
         
         nameLabel.text = [_myOfferMEGOutArray objectAtIndex:indexPath.row];
         UILabel *showLabel = (UILabel *) [cell.contentView viewWithTag:row];
         
         if (row == 5) {
             showLabel.text = self.selectGoodsSource;
         }
         if (row == 10) {
             showLabel.text = self.selectRegion;
         }
         if (row == 15) {
             showLabel.text = self.selectTime;
         }
         if (row == 8) {
             showLabel.text = self.passNumberName;
         }



         cell.selectionStyle = UITableViewCellSelectionStyleNone;
         
         return cell;
     }
     else if (row == 9)
     {
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         
         
         if (!cell) {
             
             cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
             
             
             UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(0,0, 80, 44)];
             
             nameLabel.backgroundColor = [UIColor clearColor];
             
             nameLabel.adjustsFontSizeToFitWidth = YES;
             
             nameLabel.tag = 1;
             
             [cell.contentView addSubview:nameLabel];
             
             [nameLabel release];
             
             
             UITextField *textFiled = [[UITextField alloc]initWithFrame:CGRectMake(90, 7, 80, 30)];
             
             textFiled.tag = 9;
             
             textFiled.delegate = self;
             
             textFiled.borderStyle  = UITextBorderStyleLine;
             
             [cell.contentView addSubview:textFiled];
             
             [textFiled release];
             
             
             
             UILabel *nameLabel2 = [[UILabel  alloc]initWithFrame:CGRectMake(180,0, 80, 44)];
             
             nameLabel2.backgroundColor = [UIColor clearColor];
             
             nameLabel2.adjustsFontSizeToFitWidth = YES;
             
             nameLabel2.tag = 3;
             
             nameLabel2.text  = @"天";
             
             [cell.contentView addSubview:nameLabel2];
             
             [nameLabel2 release];
             
             
         }
         
         UILabel *nameLabel = (UILabel *) [cell viewWithTag:1];
         
         nameLabel.text = [_myOfferMEGOutArray objectAtIndex:indexPath.row];

         
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
         
         return cell;
     }
    
     else if (row == 6 )
     {
         kata_ShipTime1Cell   *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         
         
         if (!cell) {
             
             cell = [[[kata_ShipTime1Cell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
             
             cell.tag = row;
             
             cell.delegate = self;
             
             
         }
         
         
         UIButton *btn1 = (UIButton *)[cell viewWithTag:2];
         
         btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
         
         [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         
        [btn1 setTitle:self.time1 forState:UIControlStateNormal];
         
         
         [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
         
         
         return cell;
         

     }
    
     else if (row == 7)
     {
         kata_ShipTime2Cell   *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         
         
         if (!cell) {
             
             cell = [[[kata_ShipTime2Cell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
             
             cell.tag = row;
             
             cell.delegate = self;
             
             
         }
         
         
         UIButton *btn1 = (UIButton *)[cell.contentView viewWithTag:2];
         
         btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
         
         [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         
         
        [btn1 setTitle:self.time2 forState:UIControlStateNormal];
 
                 
         [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
         
         
         return cell;
         
         
     }

    
     else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(10,0, 80, 44)];
            
            nameLabel.backgroundColor = [UIColor clearColor];
            
            nameLabel.adjustsFontSizeToFitWidth = YES;
            
            nameLabel.tag = 1;
            
            [cell.contentView addSubview:nameLabel];
            
            [nameLabel release];
            
            UILabel *showLabel = [[UILabel  alloc]initWithFrame:CGRectMake(90,0 , 210, 44)];
            
            showLabel.adjustsFontSizeToFitWidth = YES;
            
            showLabel.backgroundColor = [UIColor redColor];
            
            showLabel.tag  = 2;
            
            [cell.contentView addSubview:showLabel];
            
            [showLabel release];
            
            
        }
        
        UILabel *nameLabel = (UILabel *) [cell viewWithTag:1];
        
        nameLabel.text = [_myOfferMEGOutArray objectAtIndex:indexPath.row];
        
        UILabel *showLabel = (UILabel *) [cell viewWithTag:2];
        
        if (row == _tag) {
            
            showLabel.text = self.sheetTitle;

        }
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        return cell;
        
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGOutViewDelegatePush)])
//    {
//        
//        [self.delegate kata_MyOfferMEGOutViewDelegatePush];
//        
//        
//    }
    

    _tools= [[kata_Tools alloc]init];

    _tools.delegate = self;

    switch (indexPath.row) {
        case 0:
        {
            [_tools initWithMyTitle:@"勿扰标记" oneTile:@"此报盘不显示撮合勿扰标记" twoTile:@"此报盘显示撮合勿扰标记" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:0];

        }
            break;
            
        case 1:
        {
            [_tools initWithMyTitle:@"报盘人信息" oneTile:@" 网站公布,仅会员可见" twoTile:@"委托第三方撮合" threeTile:@"仅通过短信,QQ或由客服提供" foureTile:@"仅通过短信,QQ或由客服提供,隐藏公司名称" fiveTile:nil sixTile:nil view:self tag:1];
            
            
        }
            break;

        case 2:
        {
            [_tools initWithMyTitle:@"交易方向" oneTile:@"卖盘" twoTile:@"买盘" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:2];


        }
            break;
        case 3:
        {
            
        }
            break;
        case 4:
        {
             [_tools initWithMyTitle:@"船货/保税" oneTile:@"船税" twoTile:@"保税" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:4];
        }
            break;
        case 5:   //货源地
        {
            
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGOutViewPushGoodsSource:)]) {
                
                
                [self.delegate kata_MyOfferMEGOutViewPushGoodsSource:1001];
            }
            

            
        }
            break;
        case 6:
        {

        }
            break;
        case 7:
        {

        }
            break;
        case 8:  //转手
        {
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGOutViewSelectPassNumber)]) {
                
                [self.delegate kata_MyOfferMEGOutViewSelectPassNumber];
                
            }

        }
            break;
        case 9:
        {
            
            
        }
            break;
        case 10:  //交割地
        {
            
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGOutViewPushRegion:)]) {
                
                [self.delegate kata_MyOfferMEGOutViewPushRegion:1000];
            }


        }
            break;
        case 11:
        {
            [_tools initWithMyTitle:@"数量" oneTile:@"500" twoTile:@"1000" threeTile:@"1500" foureTile:@"2000" fiveTile:@">2000" sixTile:@"<500" view:self tag:11];
            
        }
            break;
        case 12:
        {
             [_tools initWithMyTitle:@"清单可借" oneTile:@"是" twoTile:@"否" threeTile:nil foureTile:nil fiveTile:nil sixTile:nil view:self tag:12];
        }
            break;
        case 13:
        {
             [_tools initWithMyTitle:@"付款方式" oneTile:@"L/C 90天" twoTile:@"L/C 即时" threeTile:@"T/T" foureTile:@"其他" fiveTile:nil sixTile:nil view:self tag:13];
        }
            break;

        case 14:
        {
            
        }
            break;
        case 15:
        {
            if ([self.delegate respondsToSelector:@selector(kata_MyOfferMEGOutViewDelegatePushSelectTime)]) {
                
                [self.delegate kata_MyOfferMEGOutViewDelegatePushSelectTime];
                
            }

           
        }
            break;
        default:
            break;
    }


    
    
    
}

#pragma mark - kata_ToolsWithSendMyOffer

- (void)kata_ToolsWithSendMyOffer:(kata_SendMyOffer *)akata_SendMyOffer tag:(int)aTag
{
    
    [_tools release];
    
    self.sendMyOffer  = (kata_SendMyOffer *)akata_SendMyOffer;
    
    self.sheetTitle = _sendMyOffer.sheetSelectTitle;
    
    self.sheetIndex = _sendMyOffer.sheetSelectIndex;
    
    DebugLog(@"sendMyOffer ==  %@  sheetIndex = %d",_sendMyOffer.sheetSelectTitle,_sendMyOffer.sheetSelectIndex);
    
    _tag = aTag;
    
    switch (_tag) {
        case 0:
        {
            self.notfaze =  self.sheetIndex;
        }
            break;
        case 1:
        {
            self.offerpeople =  self.sheetIndex;
        }
            break;
        case 2:
        {
            self.direction =  self.sheetIndex;
        }
            break;
        case 3:
        {
            //            _sendMyOffer.notfaze =  self.sheetIndex;
        }
            break;
        case 4:
        {
            self.cargobonded =  self.sheetIndex;
        }
            break;
        case 5:
        {
//            int addressId = [[NSString stringWithFormat:@"%@",self.addressId] intValue];
//            
//            self.sendMyOffer.addressId =  addressId;
        }
            break;
        case 6:
        {
            
           
            
        }
            break;
        case 7:
        {

        
        }
            break;
        case 8:
        {
//            self.number =  self.sheetIndex;
        }
            break;
        case 9:
        {
//            self.invoice =  self.sheetIndex;
        }
            break;
        case 10:
        {
//            self.storagetime =  self.sheetIndex;
        }
            break;
        case 11:
        {
                self.number =  self.sheetIndex;
        }
            break;
        case 12:
        {
            self.detailedlist =  self.sheetIndex;
        }
            break;
        case 13:
        {
            self.paymentmethods =  self.sheetIndex;
        }
            break;
        case 14:
        {
            //            self.validtime =  self.sheetIndex;
        }
            break;
        case 15:
        {
            //            self.validtime =  self.sheetIndex;
        }
            break;

            
        default:
            break;
    }
    
    
    
    
    [_tableView reloadData];
    
    
}


#pragma mark -kata_DeliveryTimeCellDelegate

- (void)kata_DeliveryTimeCellMyOfferPickerShow:(int)aTag
{
    
    _datePickerDao = [[kata_DatePickerDao alloc]init];
    
    _datePickerDao.delegate = self;
    
    [self addSubview:[_datePickerDao kata_DatePickerDaoDatePicker:aTag]];
    
    //   self.v [datePickerDao kata_DatePickerDaoDatePicker:aTag];
    
    //    [datePickerDao release];
    
}

#pragma mark -kata_DeliveryTimeCellDelegate
#pragma mark --


- (void)kata_ShipTime2CellMyOfferPickerShow:(int)aTag

{
    _datePickerDao = [[kata_DatePickerDao alloc]init];
    
    _datePickerDao.delegate = self;
    
    [self addSubview:[_datePickerDao kata_DatePickerDaoDatePicker:aTag]];
    

    
}

- (void)kata_ShipTime1CellMyOfferPickerShow:(int)aTag

{
    _datePickerDao = [[kata_DatePickerDao alloc]init];
    
    _datePickerDao.delegate = self;
    
    [self addSubview:[_datePickerDao kata_DatePickerDaoDatePicker:aTag]];
    
    
    
}


- (void)kata_ShipTimeCellMyOfferPickerShow:(int)aTag
{
    
    _datePickerDao = [[kata_DatePickerDao alloc]init];
    
    _datePickerDao.delegate = self;
    
    [self addSubview:[_datePickerDao kata_DatePickerDaoDatePicker:aTag]];
    

}


#pragma mark - kata_DatePickerDaoDelegate

- (void)kata_DatePickerDaoWithTime:(NSString *)aTime tag:(int)aTag
{
    if (aTag == 1) {
        
        self.time1 = aTime;
        
    }else if (aTag == 2){
        
        self.time2 = aTime;
        
    }else if (aTag == 3){
        
        self.time3 = aTime;
        
    }
    else{

        self.time4 = aTime;
        
    }

    
    [_tableView reloadData];
    
}

- (void)kata_DatePickerDaoWithTimeSp:(NSString *)aTimeSp tag:(int)aTag
{
    if (aTag == 1) {
        
        self.timeSp1 = aTimeSp;
        
    }else if (aTag == 2){
        
        self.timeSp2 = aTimeSp;
        
    }else if (aTag == 3){
        
        self.timeSp3 = aTimeSp;
        
    }
    else{
        
        self.timeSp4 = aTimeSp;
        
    }
    
    
}



#pragma mark - textFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField.tag == 9) {
        
        self.surrenderdocuments = textField.text;

    }
    
//    if (textField.tag == 3) {
//        
//        self.priceStr = textField.text;
//        
//    }
//    if (textField.tag == 14) {
//        
//        self.remarkStr = textField.text;
//        
//    }
//
    
}

#pragma mark - buttonEvent

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"save_offerUser_List";
}

- (NSMutableDictionary *)params
{
    
    _postDic = [[NSMutableDictionary alloc]init];
    
    NSString *nameofpart = @"1";
    
    [_postDic setObject:nameofpart forKey:@"nameofpart"];
    
    int uid = [[NSString stringWithFormat:@"%@",[kata_UserViewController sharedUserViewController].uid] intValue];
    
    [_postDic setObject:[NSNumber numberWithInt:uid] forKey:@"userid"];

    [_postDic setObject:[NSNumber numberWithInt:self.notfaze]forKey:@"notfaze"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.offerpeople]forKey:@"offerpeople"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.direction]forKey:@"direction"];
    
    int goodsSourceId =  [[NSString stringWithFormat:@"%@",self.selectGoodsSourceId] intValue];
    
    [_postDic setObject:[NSNumber numberWithInt:goodsSourceId]forKey:@"goodsSourceid"];
    

    int regionId =  [[NSString stringWithFormat:@"%@",self.selectRegionId] intValue];

    [_postDic setObject:[NSNumber numberWithInt:regionId]forKey:@"regionid"];

    
    
    int price = [[NSString stringWithFormat:@"%@",self.priceStr] intValue];
    
    [_postDic setObject:[NSNumber numberWithInt:price]forKey:@"quotation"];
    
    [_postDic setObject:[NSNumber numberWithInt:_cargobonded]forKey:@"cargobonded"];
    
    
    [_postDic setObject:[NSNumber numberWithInt:_selectHandNum]forKey:@"changehands"];

    
    int surrenderdocuments = [[NSString stringWithFormat:@"%@",_surrenderdocuments] intValue];
    
    [_postDic setObject:[NSNumber numberWithInt:surrenderdocuments]forKey:@"surrenderdocuments"];
    
    
    
    [_postDic setObject:[NSNumber numberWithInt:self.cautionmoney]forKey:@" cautionmoney"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.storagetime ]forKey:@"storagetime"];

    //    [_postDic setObject:[NSNumber numberWithInt:self.validtime]forKey:@"validtime"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.number]forKey:@"number"];
    
    
//    [_postDic setObject:[NSNumber numberWithInt:self.spotfutures]forKey:@"spotfutures"];
    
    
    [_postDic setObject:[NSNumber numberWithInt:self.detailedlist]forKey:@"detailedlist"];
    
    [_postDic setObject:[NSNumber numberWithInt:self.paymentmethods]forKey:@"paymentmethods"];
    

    
//    [_postDic setObject:[NSNumber numberWithInt:self.invoice]forKey:@"invoice"];
    
    
    
    if (self.timeSp1) {
        
        [_postDic setObject:self.timeSp1 forKey:@"loadingtime1"];
        
    }
    
    if (self.timeSp2) {
        
        [_postDic setObject:self.timeSp2 forKey:@"arrivetime1"];
        
    }
    

    if (self.remarkStr) {
        
        [_postDic setObject:self.remarkStr forKey:@"remarks"];
        
        
    }
    
    
    return _postDic;
    
}
- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}



- (void)submitMyOffer:(id)sender
{
    
//    NSString *nameofpart = @"1";
    
//    kata_SendMyOfferRequest *r = [[kata_SendMyOfferRequest alloc]initWithParms:nameofpart notfaze:_notfaze offerpeople:_offerpeople direction:_direction addressId:_addressId cautionmoney:_cautionmoney storagetime:_storagetime validtime:_validtime number:_number spotfutures:_spotfutures invoice:_invoice priceStr:_priceStr timeSp1:_timeSp1 timeSp2:_timeSp2 timeSp3:_timeSp3 timeSp4:_timeSp4 remarkStr:_remarkStr];
//
   
    
    
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds  = 20;
    
    request.delegate = self;
    
    [request startAsynchronous];
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    
}


#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{

    [MBProgressHUD hideHUDForView:self animated:YES];
    
    DebugLog(@"request ==  %@",[request responseString]);
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"发布报盘请求失败");
    
    
}




@end



