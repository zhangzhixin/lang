//
//  kata_SearchPTAOutView.m
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_SearchPTAOutView.h"

#import "kata_SearchMEGOutView.h"

#import "KATASearchDao.h"

#import "KATAUtils.h"

#import "kata_SearchCell.h"
#import "kata_DeliveryTimeCell.h"
@interface kata_SearchPTAOutView ()







@property(nonatomic,retain) KATASearchDao *searchDao;

@property(nonatomic,retain) UITableView *tableView;


@property(nonatomic,retain) NSMutableArray *MEGOutArray;


@property(nonatomic,retain)NSMutableArray *dataArray;

@property(nonatomic,retain)NSString *priceAtAalertStr;

@property(nonatomic,retain)NSString *dirStr;
@property(nonatomic,retain)NSString *priceStr;
@property(nonatomic,retain)NSString *goodsStr;
@property(nonatomic,retain)NSString *shipStr;
@property(nonatomic,retain)NSString *arrivalStr;
@property(nonatomic,retain)NSString *stateStr;
@property(nonatomic,retain)NSString *addressStr;


@property(nonatomic,retain)NSString *dateStr1;
@property(nonatomic,retain)NSString *dateStr2;

@property(nonatomic,retain)NSString *delivelyTimeStr;



@end


@implementation kata_SearchPTAOutView


- (id)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame]) {
        
        
        
        
        _MEGOutArray = [[NSMutableArray alloc]initWithObjects:
                        @"方向",@"价格",@"类型",@"装船时间",@"到港时间",@"状态",@"交割地", nil];
        
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
        
        _tableView.backgroundView = nil;
        
        _tableView.delegate = self;
        
        _tableView.dataSource = self;
        
        [self addSubview:_tableView];
        
        
        _searchDao  = [[KATASearchDao alloc]init];
        
        
        
    }
    
    return self;
    
}
-(void)dealloc
{
    [_MEGOutArray  release];
    
    [_searchDao release];
    
    [super dealloc];
    
}
#pragma mark - tableViewDataSoure
-(float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 85.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *view = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 85.0)] autorelease];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(85, 20, 150, 45);
    
    [button addTarget:self action:@selector(queryBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [button setImage:[UIImage imageNamed:@"but.png"] forState:UIControlStateNormal];
    
    [view addSubview:button];
    
    
    return view;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_MEGOutArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static  NSString *cellIdtifierMEGInside = @"CellMEGInside ";
    
    static  NSString *cellIdtifierMEGInsideTime = @"CellMEGInsideTime";
    
    
    int row = indexPath.row;
    
    
    if (row == 3 || row == 4) {
        
        kata_DeliveryTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInsideTime];
        
        if (!cell) {
            
            cell = [[[kata_DeliveryTimeCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInsideTime] autorelease];
            
            
            //                cell.delegate = self;
            
        }
        
        UILabel *nameLab  = (UILabel *)[cell viewWithTag:11];
        
        nameLab.text = [_MEGOutArray objectAtIndex:indexPath.row];
        
        
        
        
        //            UIButton *btn1 = (UIButton *)[cell viewWithTag:1];
        //
        //            btn1.titleLabel.adjustsFontSizeToFitWidth = YES;
        //
        //            [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //
        //            [btn1 setTitle:self.dateStr1 forState:UIControlStateNormal];
        //
        //
        //            UIButton *btn2 = (UIButton *)[cell viewWithTag:2];
        //
        //
        //            btn2.titleLabel.adjustsFontSizeToFitWidth = YES;
        //
        //            [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //
        //            [btn2 setTitle:self.dateStr2 forState:UIControlStateNormal];
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }else{
        
        kata_SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdtifierMEGInside];
        
        
        if (!cell) {
            
            cell = [[[kata_SearchCell   alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdtifierMEGInside] autorelease];
            
            
            
        }
        
        UILabel *nameLab  = (UILabel *)[cell.contentView viewWithTag:11];
        
        nameLab.text = [_MEGOutArray objectAtIndex:indexPath.row];
        
        if (indexPath.row == ([_MEGOutArray count] - 1) )
        {
            
            cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        
        UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:12];
        
        textFiled.text = [_MEGOutArray objectAtIndex:indexPath.row];
        
        
        if ([textFiled.text isEqualToString:@"方向"])
        {
            textFiled.text = self.dirStr;
        }
        if ([textFiled.text isEqualToString:@"价格"])
        {
            textFiled.text = self.priceAtAalertStr;
        }
        if ([textFiled.text isEqualToString:@"货种"])
        {
            
            textFiled.text = @"";
            textFiled.text = self.goodsStr;
        }
        if ([textFiled.text isEqualToString:@"交割时间"])
        {
            textFiled.text = nil;
        }
        if ([textFiled.text isEqualToString:@"状态"])
        {
            textFiled.text = self.stateStr;
        }
        if ([textFiled.text isEqualToString:@"交割地"])
        {
            
            textFiled.text = self.addressStr;
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }

}


#pragma mark - queryBtnClick

-(void)queryBtnClick
{
    DebugLog(@"查询");
    
    
    if ([self.delegate respondsToSelector:@selector(push)]) {
        
        [self.delegate push];
        
        
    }
    
    
    
    
}





@end
