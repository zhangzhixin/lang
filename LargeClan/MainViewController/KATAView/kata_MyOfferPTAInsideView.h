//
//  kata_MyOfferPTAInsideView.h
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface kata_MyOfferPTAInsideView : UIView
//
//@end

#import "kata_Tools.h"

#import "kata_DatePickerDao.h"

#import "kata_DeliveryTimeCell.h"

#import "ASIHTTPRequest.h"

@protocol kata_MyOfferPTAInsideViewDelegate <NSObject>


- (void)kata_MyOfferPTAInsideViewSelectTime;


- (void)kata_MyOfferPTAInsidePushBrand:(int)aBrand;

- (void)kata_MyOfferPTAInsidePushRegion:(int)aRegion;

@end


@interface kata_MyOfferPTAInsideView : UIView <UITableViewDataSource,UITableViewDelegate,
    kata_ToolsDelegate,
    kata_DatePickerDaoDelegate,
    kata_DeliveryTimeCellDelegate,
    ASIHTTPRequestDelegate>

{
    id <kata_MyOfferPTAInsideViewDelegate> delegate;
    
}

@property(nonatomic,assign)    id <kata_MyOfferPTAInsideViewDelegate> delegate;

@property(nonatomic,retain) UITableView *tableView;

@property(nonatomic,copy) NSString *selectRegion;

@property(nonatomic,copy) NSString *selectBrand;

@property(nonatomic,copy) NSString *selectRegionId;

@property(nonatomic,copy) NSString *selectBrandId;



@property(nonatomic,assign) int selectTimeId;

@property(nonatomic,copy) NSString *selectTime;


@property(nonatomic,copy) NSString *passNumberName;

@property(nonatomic,assign) int passNumberId;



@end
