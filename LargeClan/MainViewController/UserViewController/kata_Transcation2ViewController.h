//
//  kata_Transcation2ViewController.h
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>


#import <UIKit/UIKit.h>

#import "kata_segView.h"
#import "ASIHTTPRequest.h"

#import "kata_LeftTableDelegate.h"

@interface kata_Transcation2ViewController : UIViewController <kata_segViewDelegate,UITableViewDataSource,UITableViewDelegate,
    UIAlertViewDelegate,
    UIActionSheetDelegate>


- (NSMutableDictionary *)paramsTransaction:(NSString *)uid offerId:(NSString *)aOfferId;
- (NSString *)signTransaction;
- (NSString *)methodTransaction;


@end
