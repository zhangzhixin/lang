//
//  kata_PassNumberViewController.m
//  LargeClan
//
//  Created by kata on 14-1-12.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_PassNumberViewController.h"

//@interface kata_PassNumberViewController ()
//
//@end
//
//@implementation kata_PassNumberViewController
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//	// Do any additional setup after loading the view.
//}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//@end




//
//  kata_PastTimeViewController.m
//  LargeClan
//
//  Created by kata on 14-1-10.
//  Copyright (c) 2014年 kata. All rights reserved.
//


#import "KATAConstants.h"

@interface kata_PassNumberViewController ()


@property(nonatomic,retain)NSArray *timeSelectArray;
@end

@implementation kata_PassNumberViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)dealloc
{
    
    [_timeSelectArray release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"转手";
    
    self.navigationItem.hidesBackButton = YES;
    
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
    
    _timeSelectArray = [[NSArray alloc]initWithObjects:@">9手",@"1手",@"2手",@"3手",@"4手",@"5手",@"6手",@"7手",@"8手",@"9手", nil];
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
    
    tableView.backgroundView = nil;
    
    [self.view addSubview:tableView];
    
    [tableView release];
    
    
    
}

- (void)backClick
{
    //    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    //
    //    [tab showTabBar];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - tableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_timeSelectArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        
        
        
    }
    
    
    cell.textLabel.text  = [_timeSelectArray objectAtIndex:indexPath.row];
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.delegate respondsToSelector:@selector(kata_PassNumberViewControllerWithSelectPassNumber: selectPassNumberId:)]) {
        
//        [self.delegate kata_PastTimeViewControllerWithSelectTime:[_timeSelectArray objectAtIndex:indexPath.row] selectTimeId:indexPath.row];
        
        [self.delegate kata_PassNumberViewControllerWithSelectPassNumber:[_timeSelectArray objectAtIndex:indexPath.row] selectPassNumberId:indexPath.row];

        
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
