//
//  kata_sendMyOfferVeiwController.m
//  LargeClan
//
//  Created by kata on 14-1-7.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_sendMyOfferVeiwController.h"

#import "KATAConstants.h"

#import "kata_Tools.h"

#import "KATAUtils.h"


#import "kata_segView.h"

#import "kata_MyOfferMEGInsideView.h"
#import "kata_MyOfferMEGOutView.h"
#import "kata_MyOfferPTAInsideView.h"
#import "kata_MyOfferPTAOutView.h"

#import "kata_UserViewController.h"

#import "kata_PassNumberViewController.h"

#import "kata_PastTimeViewController.h"

#import "kata_UserViewController.h"

@interface kata_sendMyOfferVeiwController ()
{
    kata_Tools *_tools;
    
    kata_MyOfferMEGInsideView *_myMEGInsideView;
    
    kata_MyOfferMEGOutView *_myMEGOutView;
    
    kata_MyOfferPTAInsideView *_myPTAInsideView;
    
    kata_MyOfferPTAOutView *_myPTAOutView;

}


@property(nonatomic,retain)NSArray *sendMyOfferArray;

@end

@implementation kata_sendMyOfferVeiwController

- (void)dealloc
{
    
    [_myPTAInsideView release];
    
    [_myPTAOutView release];

    [_myMEGOutView release];
    
    [_myMEGInsideView release];
    
    [_sendMyOfferArray release];
    
    [super dealloc];
    
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    _sendMyOfferArray  = [[NSArray alloc]initWithObjects:
                          @"勿扰标记", @"报盘类型", @"报盘人信息", @"交易方向", @"人民币价格", @"期货类型", @"交割时间", @"交割地", @"保证金", @"数量", @"发票",@"免仓时间",@"备注",@"过期时间",
                           nil];
    
    
    
    
//    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
//    
//    
//    tableView.delegate = self;
//    
//    tableView.dataSource = self;
//    
//    tableView.backgroundView = nil;
//    
//    [self.view addSubview:tableView];
//    
//    [tableView release];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

    kata_segView *segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40) typeIdentifier:302];
    
    
    segView.delegate = self;
    
    [self.view addSubview:segView];
    
    [segView release];

    
   _myMEGInsideView = [[kata_MyOfferMEGInsideView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight - 40 - 44)];
    
    _myMEGInsideView.delegate = self;
    
    [self.view addSubview:_myMEGInsideView];
    
    
    _myMEGOutView = [[kata_MyOfferMEGOutView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight - 40 - 44)];
    
    _myMEGOutView.delegate = self;
    
//    [self.view addSubview:_myMEGOutView];
    
    
    _myPTAInsideView = [[kata_MyOfferPTAInsideView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight - 40 - 44)];
    
    _myPTAInsideView.delegate = self;
    
//    [self.view addSubview:_myPTAInsideView];
    
    _myPTAOutView = [[kata_MyOfferPTAOutView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, ScreenHeight - 40 - 44)];
    
    _myPTAOutView.delegate = self;
    
//    [self.view addSubview:_myPTAOutView];
    
//    
//    kata_MyOfferMEGInsideViewController *myMEGInsideVC = [[kata_MyOfferMEGInsideViewController alloc]init];
//    
//    myMEGInsideVC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 40 -44);
//    
//    [self.view addSubview:myMEGInsideVC.view];
//    
//    [myMEGInsideVC release];
    
}
- (void)backClick
{
//    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
//    
//    [tab showTabBar];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - segViewDelegate

- (void)changeIndex:(int)index
{
    
    switch (index - 1) {
        case 0:
        {
            
            [_myMEGOutView removeFromSuperview];
            
            [_myPTAInsideView removeFromSuperview];
            
            [_myPTAOutView removeFromSuperview];
            
            [self.view addSubview:_myMEGInsideView];
        }
            break;
        case 1:
        {
            [_myMEGInsideView removeFromSuperview];
            
            [_myPTAInsideView removeFromSuperview];
            
            [_myPTAOutView removeFromSuperview];
            
            [self.view addSubview:_myMEGOutView];
        }
            break;
        case 2:
        {
            [_myMEGOutView removeFromSuperview];
            
            [_myMEGInsideView removeFromSuperview];
            
            [_myPTAOutView removeFromSuperview];
            
            [self.view addSubview:_myPTAInsideView];
        }
            break;
        case 3:
        {
            [_myMEGOutView removeFromSuperview];
            
            [_myPTAInsideView removeFromSuperview];
            
            [_myMEGInsideView removeFromSuperview];
            
            [self.view addSubview:_myPTAOutView];
        }
            break;
            
        default:
            break;
    }
}





#pragma mark - kata_MyOfferMEGInsideViewDelegate


- (void)kata_MyOfferMEGInsideViewPushGoodsSource:(int)aGoodsSource
{
    
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:0 addressType:1000];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];
}


- (void)kata_MyOfferMEGInsideViewDelegatePushSelectTime
{
    
    kata_PastTimeViewController *pastTime = [[kata_PastTimeViewController alloc]init];
    
    pastTime.delegate = self;
    
    [self.navigationController pushViewController:pastTime animated:YES];
    
    [pastTime release];
    
    
}


#pragma mark - kata_MyOfferMEGOutViewDelegate

- (void)kata_MyOfferMEGOutViewPushGoodsSource:(int)aGoodsSource
{
    
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:1 addressType:1001];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];
}

- (void)kata_MyOfferMEGOutViewPushRegion:(int)aRegion
{
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:1 addressType:1000];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];

    
}

#pragma mark - kata_AddressViewControllerDelegate

- (void)kata_AddressViewControllerWithOfferType:(int)aOfferType addressType:(int)aAddressType AddressName:(NSString *)aAddressName addressId:(NSString *)aAddressId
{
    
    
    if (aAddressType == 1000 && aOfferType  == 0) {
        
        _myMEGInsideView.addressName = aAddressName;
        
        _myMEGInsideView.addressId = aAddressId;
        
        [_myMEGInsideView.tableView reloadData];

        
    }
    
    
    
    if (aAddressType == 1000 && aOfferType  == 1) {
        
        _myMEGOutView.selectRegion = aAddressName;
        
        _myMEGOutView.selectRegionId = aAddressId;
        
        [_myMEGOutView.tableView reloadData];
        
        
    }

    if (aAddressType == 1001 && aOfferType == 1) {
        
        _myMEGOutView.selectGoodsSource = aAddressName;
        
        _myMEGOutView.selectGoodsSourceId = aAddressId;
        
        [_myMEGOutView.tableView reloadData];
        

    }
    
    
    if (aAddressType == 1000 && aOfferType == 2) {
        
        _myPTAInsideView.selectRegion = aAddressName;
        
        _myPTAInsideView.selectRegionId = aAddressId;
        
        [_myPTAInsideView.tableView reloadData];
        
    }
    
    

    
    if (aAddressType == 1002 && aOfferType == 2) {
        
        _myPTAInsideView.selectBrand = aAddressName;
        
        _myPTAInsideView.selectBrandId = aAddressId;
        
        [_myPTAInsideView.tableView reloadData];
        
    }

    
    
    if (aAddressType == 1001 && aOfferType == 3) {
        
        
        _myPTAOutView.selectGoodsSource = aAddressName;
        
        _myPTAOutView.selectGoodsSourceId = aAddressId;
        
        [_myPTAOutView.tableView reloadData];

        
    }
    
    if (aAddressType == 1000 && aOfferType == 3) {
        
        
        _myPTAOutView.selectRegion = aAddressName;
        
        _myPTAOutView.selectRegionId = aAddressId;
        
        [_myPTAOutView.tableView reloadData];
        
        
    }
    if (aAddressType == 1002 && aOfferType == 3) {
        
        
        _myPTAOutView.selectBrandName = aAddressName;
        
        _myPTAOutView.selectBrandId = aAddressId;
        
        [_myPTAOutView.tableView reloadData];
        
        
    }

    
  
}


- (void)kata_MyOfferMEGOutViewDelegatePushSelectTime
{
    kata_PastTimeViewController *pastTime = [[kata_PastTimeViewController alloc]init];
    
    pastTime.delegate = self;
    
    [self.navigationController pushViewController:pastTime animated:YES];
    
    [pastTime release];
    

    
}





#pragma mark - kata_PastTimeViewControllerWithSelectTime

- (void)kata_PastTimeViewControllerWithSelectTime:(NSString *)aSelectTime selectTimeId:(int)aSelectTimeId
{
    
    _myMEGInsideView.selectTime = aSelectTime;
    
    _myMEGInsideView.selectTimeId = aSelectTimeId;
    
    [_myMEGInsideView.tableView reloadData];

    
    _myMEGOutView.selectTime = aSelectTime;
    
    _myMEGOutView.selectTimeId = aSelectTimeId;
    
    [_myMEGOutView.tableView reloadData];
    
    
    _myPTAInsideView.selectTime = aSelectTime;
    
    _myPTAInsideView.selectTimeId = aSelectTimeId;
    
    [_myPTAInsideView.tableView reloadData];

    _myPTAOutView.selectTime = aSelectTime;
    
    _myPTAOutView.selectTimeId = aSelectTimeId;
    
    [_myPTAOutView.tableView reloadData];
    
}


/////////////////////////////////////////////
#pragma mark - kata_MyOfferMEGOutViewDelegate

- (void)kata_MyOfferMEGInsideViewPushMyOffer
{
    
    kata_UserViewController *userVC = [[kata_UserViewController alloc]init];
  
    [self.navigationController pushViewController:userVC animated:YES];
    
    [userVC release];
}


- (void)kata_MyOfferMEGOutViewSelectPassNumber
{
    kata_PassNumberViewController *passNumberVC = [[kata_PassNumberViewController alloc]init];
    
    passNumberVC.delegate = self;
    
    
    [self.navigationController pushViewController:passNumberVC  animated:YES];
    
    [passNumberVC release];
    
}

#pragma mark - kata_PassNumberViewControllerDelegate


- (void)kata_PassNumberViewControllerWithSelectPassNumber:(NSString *)aPassNumber selectPassNumberId:(int)aSelectPassNumberId
{
    _myMEGOutView.passNumberName = aPassNumber;
    
    _myMEGOutView.selectHandNum = aSelectPassNumberId;
    
    [_myMEGOutView.tableView reloadData];
    
    
    _myPTAOutView.selectHandNumName = aPassNumber;
    
    _myPTAOutView.selectHandNum = aSelectPassNumberId;
    
    [_myPTAOutView.tableView reloadData];
    
}



//////////////////////////////////////////////////////////

#pragma mark - kata_MyOfferPTAInsideViewDelegate


- (void)kata_MyOfferMEGOutViewPushBrand:(int)aBrand
{
    
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:2 addressType:1002];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];

}

- (void)kata_MyOfferPTAInsideViewSelectTime
{
    
    kata_PastTimeViewController *pastTime = [[kata_PastTimeViewController alloc]init];
    
    pastTime.delegate = self;
    
    [self.navigationController pushViewController:pastTime animated:YES];
    
    [pastTime release];
    
    

}


#pragma mark - kata_MyOfferPTAInsideViewDelegate


- (void)kata_MyOfferPTAInsidePushBrand:(int)aBrand
{
    
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:2 addressType:1002];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];
}


- (void)kata_MyOfferPTAInsidePushRegion:(int)aRegion
{
    
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:2 addressType:1000];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];
    
}

#pragma mark - kata_MyOfferPTAOutViewPushGoodsSource

- (void)kata_MyOfferPTAOutViewPushBrand:(int)aBrand
{
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:3 addressType:1002];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];

}

- (void)kata_MyOfferPTAOutViewPushGoodsSource:(int)aGoodsSource
{
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:3 addressType:1001];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];

    
}

- (void)kata_MyOfferPTAOutViewPushRegion:(int)aRegion
{
    
    kata_AddressViewController *addressVC = [[kata_AddressViewController alloc]initWithOfferType:3 addressType:1000];
    
    addressVC.addressDelegate = self;
    
    [self.navigationController pushViewController:addressVC animated:YES];
    
    [addressVC release];

}

- (void)kata_MyOfferPTAOutViewSelectPassNumber
{
    [self selectPassNumber];
}


- (void)kata_MyOfferPTAOutViewSelectTime
{
    
    kata_PastTimeViewController *pastTime = [[kata_PastTimeViewController alloc]init];
    
    pastTime.delegate = self;
    
    [self.navigationController pushViewController:pastTime animated:YES];
    
    [pastTime release];
    

}



//  几手 
- (void)selectPassNumber
{
    
    kata_PassNumberViewController *passNumberVC = [[kata_PassNumberViewController alloc]init];
    
    passNumberVC.delegate = self;
    
    
    [self.navigationController pushViewController:passNumberVC  animated:YES];
    
    [passNumberVC release];
    

}



@end
