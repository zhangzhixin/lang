//
//  kata_Transcation1ViewController.m
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_Transcation1ViewController.h"

#import "KATAUITabBarController.h"

#import "kata_segView.h"
#import "KATAConstants.h"
#import "kata_LeftCell.h"
#import "kata_TwoTableView.h"
#import "ASIFormDataRequest.h"
#import "kata_UserViewController.h"
#import "NSString+MD5.h"
#import "KATAConstants.h"
#import "KATAUtils.h"
#import "kata_UserViewController.h"
#import "KATAIndex.h"
#import "MBProgressHUD.h"
#import "kata_Tools.h"

@interface kata_Transcation1ViewController ()
{
    
    UITableView * _tableView1;
    UITableView * _tableView2;
    UIScrollView *_scrollView;
    int _didSelectRowTag;
    
}

@property(nonatomic,retain) NSArray *tableView1Array;
@property(nonatomic,retain) NSArray *tableView2Array;
@property(nonatomic,retain) NSMutableDictionary *params;
@property(nonatomic,assign) int nameofpart;
@property(nonatomic,retain) NSMutableArray *leftArray;
@property(nonatomic,retain) NSMutableArray *rightArray;
@property(nonatomic,retain) NSMutableArray *rightHeardArray;

@property(nonatomic,retain) NSString  *priceStr; //价格
@property(nonatomic,retain) NSString   *transcationStr;



@end


@implementation kata_Transcation1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      
        
    }
    return self;
}

- (void)dealloc
{
    
    [_transcationStr release];
    
    [_priceStr release];
    
    [_rightHeardArray release];
    
    [_rightArray release];
    
    [_leftArray release];
    
    [_scrollView release];
    
    [_params release];
    
    [_tableView1Array release];
    
    [_tableView2Array release];
    
    
    [_tableView1 release];
    
    [_tableView2 release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"我的报盘";
    
    self.navigationItem.hidesBackButton = YES;
    
    KATAUITabBarController *tabBarController = (KATAUITabBarController *)self.tabBarController;
    
    [tabBarController hideTabBar];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
    
    _tableView2Array = [[NSArray alloc]initWithObjects:
                        @"报盘编号",@"方向",@"美金（CFR）",@"类型",@"货源地",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"清单可借",@"付款方式",@"报盘状态",@"成交价格",@"成交状态",@"备注", nil];
    
    _leftArray = [[NSMutableArray alloc]init];
    
    _rightArray = [[NSMutableArray alloc]init];
    
    _priceStr =  @"输入价格";
    
    
    _tableView1 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 80, ScreenHeight - 40 - 44) style:UITableViewStylePlain];
    
    _tableView1.tag = 1;
    
    _tableView1.delegate =self;
    
    _tableView1.dataSource = self;
    
    
    [self.view addSubview:_tableView1];
    
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(80, 0, 240, ScreenHeight - 40 - 44)];
    
    
    _scrollView.delegate = self;
    
    _scrollView.contentSize = CGSizeMake([_tableView2Array count] *LBLWIDTH + 20, 0);
    
    [self.view addSubview:_scrollView];
    
    
    _tableView2 = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [_tableView2Array count] *LBLWIDTH, ScreenHeight - 40 - 44) style:UITableViewStylePlain];
    
    _tableView2.tag = 2;
    
    _tableView2.delegate = self;
    
    _tableView2.dataSource = self;
    
    [_scrollView addSubview:_tableView2];
    
    
    
    [self sendRequest];
    
    
}
#pragma mark - buttonEvent

-(void)backClick
{
    
    [self.navigationController  popToRootViewControllerAnimated:YES];
    
}

#pragma mark - rightTableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (tableView.tag == 1) {
        
        UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        vi.backgroundColor = [UIColor blueColor];
        
        UILabel *numberLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        numberLbl.text = @"报盘时间";
        
        numberLbl.adjustsFontSizeToFitWidth = YES;
        
        numberLbl.textAlignment = NSTextAlignmentCenter;
        
        [vi addSubview:numberLbl];
        
        [numberLbl release];
        
        return [vi autorelease];
        
        
    }
    
    else {
        
        UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, LBLWIDTH * [self.tableView2Array count], 44)];
        
        vi.backgroundColor = [UIColor blueColor];
        
        
        for (int i = 0; i < [self.tableView2Array count]; i++) {
            
            UILabel *numberLbl = [[UILabel alloc]initWithFrame:CGRectMake(LBLWIDTH * i, 0, LBLWIDTH, 44)];
            
            numberLbl.text = [_tableView2Array objectAtIndex:i];
            
            numberLbl.adjustsFontSizeToFitWidth = YES;
            
            numberLbl.textAlignment = NSTextAlignmentCenter;
            
            [vi addSubview:numberLbl];
            
            [numberLbl release];
            
        }
        
        return [vi autorelease];
        
    }
    
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_leftArray  count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //左 //左
    if (tableView.tag == 1) {  //左
        
        static  NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[[kata_LeftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
        }
        
        
        UILabel *numberLab = (UILabel *)[cell viewWithTag:112];
        
        if ([_leftArray count] !=  0) {
            KATAIndex *indexObj = [_leftArray objectAtIndex:indexPath.row];
            
            //报盘编号
            
            [numberLab setFont:[UIFont  systemFontOfSize:8.0f]];
            
            //            numberLab.text  = indexObj.code;
            
            numberLab.text  = indexObj.auditingstatus;
            
            
            
            UIImageView *imageView1 = (UIImageView *)[cell viewWithTag:11];
            
            
            if ([indexObj.notfaze isEqualToString:@"0"]) {
                
                [imageView1 setImage:[UIImage imageNamed:@"w.png"]];
                
            }else
            {
                
                imageView1.backgroundColor = [UIColor clearColor];
                
            }
            
            
        }
        
        
        UIImageView *imageView2 = (UIImageView *)[cell viewWithTag:12];
        
        [imageView2 setImage:[UIImage imageNamed:@"rz.png"]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //    cell.textLabel.text = @"left";
        
        return cell;
        
        
        
    }else  //右 //右
        
    {
        static  NSString *CellIdentifier0 = @"Cell0";
        
        
        
        KATAIndex *indexObj = [_rightArray objectAtIndex:indexPath.row];
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier0];
        
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier0] autorelease];
            
            for (int  i = 0; i < [_tableView2Array  count];  i ++)
            {
                
                if ((i >= 0 && i < [_tableView2Array count] )|| i == [_tableView2Array count]) {
                    
                    
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    lab.textAlignment = NSTextAlignmentCenter;
                    
                    [cell addSubview:lab];
                    
                    [lab release];
                }
                
            }
            
        }
        
        //编号
        UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
        
        timeLab.text = indexObj.code;
        
        //方向
        UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
        
        directionLab.text = indexObj.direction;
        
        //美金
        UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
        
        quotationLab.text  = indexObj.quotation;
        
        //类型
        UILabel *cargobonded = (UILabel *)[cell viewWithTag:13];
        
        cargobonded.text = indexObj.cargobonded;
        
        //货源地
        UILabel *goodssourcename = (UILabel *)[cell viewWithTag:14];
        
        goodssourcename.text = indexObj.goodssourcename;
        
        //                    //预装船
        UILabel *loadingtime1 = (UILabel *)[cell viewWithTag:15];
        
        loadingtime1.text = indexObj.loadingtime1;
        
        //                    //预到港
        UILabel *loadingtime2 = (UILabel *)[cell viewWithTag:16];
        
        loadingtime2.text = indexObj.loadingtime2;
        
        
        //转手
        UILabel *changehands = (UILabel *)[cell viewWithTag:17];
        
        changehands.text = indexObj.changehands;
        
        
        //交单
        UILabel *surrenderdocuments = (UILabel *)[cell viewWithTag:18];
        
        surrenderdocuments.text = indexObj.surrenderdocuments;
        
        
        //交割地
        UILabel *deliveryname = (UILabel *)[cell viewWithTag:19];
        
        deliveryname.text = indexObj.deliveryname;
        
        //数量
        UILabel *number = (UILabel *)[cell viewWithTag:20];
        number.text = indexObj.number;
        
        //  清单可借
        UILabel *detailedlist = (UILabel *)[cell viewWithTag:21];
        
        detailedlist.text = indexObj.detailedlist;
        
        
        //                    //付款方式
        UILabel *paymentmethods = (UILabel *)[cell viewWithTag:22];
        
        paymentmethods.text  = indexObj.paymentmethods;
        
        //                    //报盘状态
        UILabel *state = (UILabel *)[cell viewWithTag:23];
        
        state.text  = indexObj.state;
        
        //                    //价格
        UILabel *auditingstatus = (UILabel *)[cell viewWithTag:24];
        
        auditingstatus.text  = self.priceStr;
        
        //                    成交状态
        UILabel *transcationLbl = (UILabel *)[cell viewWithTag:25];
        
        transcationLbl.text   = indexObj.dealstatus;
        
//        transcationLbl.text  = self.transcationStr;
        
        //                   备注
        UILabel *remarksLbl = (UILabel *)[cell viewWithTag:26];
        
        remarksLbl.text  = indexObj.remarks;
        

        

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
  
    }
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    _didSelectRowTag = indexPath.row;
    
    
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"选择操作" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"输入价格",@"成交",@"取消", nil];
    
    [sheet showInView:self.view];
    
    [sheet release];
 
}

#pragma mark -  actionSheetDelegate  && alertViewDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    KATAIndex *indexObj = [_rightArray objectAtIndex:_didSelectRowTag];

    
    switch (buttonIndex) {
        case 0:
        {
            if ([indexObj.dealprice isEqualToString:@"0"]) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"输入价格" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定",@"取消", nil];
                
                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                
                [alert show];
                
                [alert release];
                
            }else{
                
                [kata_Tools myAlertView:@"温馨提示" message:@"次报盘已经成交，不可在修改价格" cancelButtonTitle:@"确定" otherButtonTitles:nil];
          }
            
        }
            break;
        case 1:  //成交报盘
        {
            
            NSString *uid = [kata_UserViewController sharedUserViewController].uid;
            
            KATAIndex *indexDao = [[self.rightArray objectAtIndex:_didSelectRowTag] retain];
            
            DebugLog(@"indexDao =  %@  _didSelectRowTag = %d  self.rightArray ==  %@",indexDao.offerId,_didSelectRowTag,self.rightArray);
            
            NSString *Offerid = indexDao.offerId;
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:SERVER_URI]];
            
            [request setPostValue:[self signTransaction] forKey:@"sign"];
            [request setPostValue:[self methodTransaction] forKey:@"method"];
            [request setPostValue:[self appkey] forKey:@"appkey"];
            [request setPostValue:[self paramsTransaction:uid offerId:Offerid] forKey:@"params"];
            [request  setPostValue:[NSNumber numberWithLong:[self time]] forKey:@"time"];
            [request setDidFailSelector:@selector(transactionFail:)];
            [request setDidFinishSelector:@selector(transactionFinish:)];
            
            request.delegate = self;
            
            request.timeOutSeconds = 20;
            
            [request startAsynchronous];
            
        }
            break;
        case 2:  // 取消
        {
            
        }
            break;
            
            
        default:
            break;
    }
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
        {
            
            UITextField *textField = [alertView textFieldAtIndex:0];
            
            self.priceStr = textField.text;
            
            
            [_tableView2 reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_didSelectRowTag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            _didSelectRowTag = 0;
            
            
        }
            break;
            
        case 1: // 取消
        {
            
            
            
        }
            break;
            
        default:
            break;
    }
    
}

- (void)transactionFinish:(ASIHTTPRequest *)request
{
    DebugLog(@"request =  %@",[request responseString]);
    
    
    
}

- (void)transactionFail:(ASIHTTPRequest *)request
{
    
    
    
    
    
}


#pragma mark - dataParams  &&  transcationParams

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offeruser_get";
}

- (NSMutableDictionary *)params
{
    
    //    DebugLog(@"self.nameofpart ==  %d",self.nameofpart);
    
    //    NSString *nameofpart = [NSString stringWithFormat:@"%d",self.nameofpart];
    
    NSString *nameofpart = @"1";
    
    NSString *uid = [kata_UserViewController sharedUserViewController].uid;
    
    self.params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   
                   nameofpart,@"nameofpart",
                   
                   uid,@"userid",
                   
                   nil];
    
    return _params;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

// 成交参数

- (NSString *)signTransaction
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self methodTransaction],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}
- (NSString *)methodTransaction
{
    return @"index_offeruser_get";
}

- (NSMutableDictionary *)paramsTransaction:(NSString *)uid offerId:(NSString *)aOfferId

{
    
    NSString *nameofpart = @"1";
    
    self.params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   nameofpart,@"nameofpart",
                   aOfferId  ,@"offerid",
                   uid,@"userid",
                   self.priceStr,@"Dealprice",
                   nil];
    
    return _params;
    
}

- (void)sendRequest
{
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL  URLWithString:SERVER_URI]];
    
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    
    request.timeOutSeconds = 20;
    
    request.delegate  = self;
    
    [request startAsynchronous];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}


#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
    // 左  // 左
    
    [_leftArray removeAllObjects];
    
    DebugLog(@"dic ==  %@",dic);
    
    NSString *code = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"code"] intValue]];
    
    if ([dic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        if ([offer_results count] != 0) {
            
            for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                //报盘时间
                if ([dic objectForKey:@"offertime"]  != [NSNull null])
                    
                {
                    long long time = [[[dic objectForKey:@"offertime"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
                    
                    NSLog(@"confromTimesp ==  %@",confromTimesp);
                    
                    
                    indexObj.auditingstatus = [self date:confromTimesp];
                    
                }
                
                
                //                NSString *megCode = [dic objectForKey:@"code"];
                //
                //                indexObj.code = megCode;
                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                
                [self.leftArray addObject:indexObj];
                
                [indexObj release];
                
            }
            
        }
        if ([offer_results count] != 0 && [_leftArray count ] != 0) {
            
            [_tableView1 reloadData];
            
        }
        
    }
    
    
    DebugLog(@"    indexObj.code  ==  %d",    [_leftArray count] );
    
    [_tableView1 reloadData];
    
    
    
    
    
    
    
    // 右  // 右
    
    
    [_rightArray removeAllObjects];
    
    
    DebugLog(@"dic ==  %@  dic retainCount ===  %d",dic,[dic retainCount]);
    
    //    NSString *code = [dic objectForKey:@"code"];
    
    if ([dic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        //        DebugLog(@"dic ==  %@  dic retainCount ===  %d",dic,[dic retainCount]);
        
        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        
        if ([offer_results count] != 0) {
            
            for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                // 报盘ID
                if ([dic objectForKey:@"id"] != [NSNull null]) {
                    
                    indexObj.offerId = [dic objectForKey:@"id"];
                }
                
                
                //报盘时间
                if ([dic objectForKey:@"auditingDate"]  != [NSNull null])
                    
                {
                    long long time = [[[dic objectForKey:@"auditingDate"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
                    
                    //        NSLog(@"confromTimesp ==  %@",confromTimesp);
                    
                    
                    indexObj.auditingstatus = [self date:confromTimesp];
                    
                }
                
                
                //交割时间
                
                if ([dic objectForKey:@"deliverytime2"]  != [NSNull null] && [dic objectForKey:@"deliverytime2"] != [NSNull null]) {
                    
                    
                    
                    long long deliveryTime1 = [[[dic objectForKey:@"deliverytime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime1 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.deliverytime1 = [self deliveryDate:confromTimesp1];
                    
                    
                    long long deliveryTime2 = [[[dic objectForKey:@"deliverytime2"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp2 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime2 / 1000)];
                    
                    
                    indexObj.deliverytime2 = [self deliveryDate:confromTimesp2];
                    
                    NSString *deliveryStr1  = [indexObj.deliverytime1 stringByReplacingOccurrencesOfString:@"GMT+" withString:@"-"];
                    
                    NSString *deliveryStr2  = [indexObj.deliverytime2 stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                    
                    indexObj.deliverytime = (NSMutableString *)[deliveryStr1 stringByAppendingString:deliveryStr2];
                    
                    DebugLog(@"indexObj.deliverytime ==  %@",indexObj.deliverytime);
                    
                    
                }
                
                
                //预计装船开始时间
                if ([dic objectForKey:@"loadingtime1"]  != [NSNull null]) {
                    
                    
                    
                    long long loadingtime1 = [[[dic objectForKey:@"loadingtime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime1 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.loadingtime1 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                }
                
                //预计装船结束时间
                
                if ([dic objectForKey:@"arrivetime1"]  != [NSNull null]) {
                    
                    
                    
                    long long loadingtime2 = [[[dic objectForKey:@"arrivetime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime2 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.loadingtime2 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                }
                //
                //货源地
                
                if ( [dic objectForKey:@"goodsSource"] != [NSNull null]) {
                    
                    
                    NSDictionary *goodsSource  =[dic objectForKey:@"goodsSource"];
                    
                    NSString *goodssourcename = [goodsSource objectForKey:@"goodssourcename"];
                    
                    indexObj.goodssourcename = goodssourcename;
                    
                }
                
                //
                //交割地
                
                if ( [dic objectForKey:@"region"] != [NSNull null]) {
                    
                    NSDictionary *regionDic  = [dic objectForKey:@"region"];
                    
                    NSString *deliveryname = [regionDic objectForKey:@"deliveryname"];
                    
                    indexObj.deliveryname = deliveryname;
                    
                    
                }
                
                // 转手
                if ( [dic objectForKey:@"changehands"] != [NSNull null]) {
                    
                    int  changehands =  [[dic objectForKey:@"changehands"] intValue];
                    
                    DebugLog(@"changehands = %d",changehands);
                    
                    indexObj.changehands = [NSString stringWithFormat:@"%d手",changehands];
                    
                    DebugLog(@"indexObj.changehands = %@",indexObj.changehands);
                }
                
                // 交单天数
                
                if ( [dic objectForKey:@"surrenderdocuments"] != [NSNull null]) {
                    
                    int  surrenderdocuments =  [[dic objectForKey:@"surrenderdocuments"] intValue];
                    
                    indexObj.surrenderdocuments = [NSString stringWithFormat:@"%d天",surrenderdocuments];
                    
                }
                
                // 付款方式
                
                if ( [dic objectForKey:@"paymentmethods"] != [NSNull null]) {
                    
                    int  paymentmethods =  [[dic objectForKey:@"paymentmethods"] intValue];
                    
                    
                    if (paymentmethods == 0) {
                        
                        indexObj.paymentmethods = @"L/C90 天";
                        
                    }else if (paymentmethods == 1) {
                        
                        indexObj.paymentmethods = @"L/C 即期";
                        
                    }
                    else if (paymentmethods == 2) {
                        
                        indexObj.paymentmethods = @"L/T ";
                        
                    }
                    else{
                        indexObj.paymentmethods = @"其他";
                        
                    }
                    
                    
                    
                }
                
                // 单据类型
                
                if ( [dic objectForKey:@"detailedlist"] != [NSNull null]) {
                    
                    int  detailedlist = [[dic objectForKey:@"detailedlist"] intValue];
                    
                    if (detailedlist == 0) {
                        
                        indexObj.detailedlist = @"是";
                        
                    }else
                    {
                        indexObj.detailedlist = @"否";
                        
                    }
                    
                }
                
                //品名
                
                if ( [dic objectForKey:@"nameofpart"] != [NSNull null]){
                    
                    int nameofpart = [[dic objectForKey:@"nameofpart"] intValue];
                    
                    indexObj.nameofpart = nameofpart;
                    
                    
                }
                //税 类型
                
                if ( [dic objectForKey:@"cargobonded"] != [NSNull null]){
                    
                    
                    int cargobonded = (int)[dic objectForKey:@"cargobonded"];
                    
                    
                    if (cargobonded == 0) {
                        
                        indexObj.cargobonded = @"船税";
                    }else
                    {
                        indexObj.cargobonded = @"保税";
                        
                    }
                    
                }
                
                
                //配送
                if ( [dic objectForKey:@"dispatching"] != [NSNull null]){
                    
                    int dispatching = [[dic objectForKey:@"dispatching"] intValue];
                    
                    if (dispatching == 0) {
                        
                        indexObj.dispatching = @"自提";
                        
                    }else{
                        
                        indexObj.dispatching = @"送到";
                    }
                }
                
                //品牌
                if ( [dic objectForKey:@"brand"] != [NSNull null]){
                    
                    indexObj.brand =  [[dic objectForKey:@"brand"] objectForKey:@"brandname"];
                    
                }
                
                // 成交价格
                
                if ( [dic objectForKey:@"dealprice"] != [NSNull null]){
                    
                    indexObj.dealprice = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"dealprice"] intValue]];
                    
                }
                

                
                
                
                
                
                
                
                
                //        //价格
                
                NSString *quotation  = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"quotation"] intValue]];
                
                indexObj.quotation = quotation;
                
                
                //方向
                int  direction =  [[dic objectForKey:@"direction"] intValue];
                
                DebugLog(@"directiondirection ==  %d",direction);
                
                if (direction == 0) {
                    
                    indexObj.direction = @"卖盘";
                    
                }else
                {
                    indexObj.direction = @"买盘";
                    
                }
                
                
                
                NSString *megCode = [dic objectForKey:@"code"];
                
                indexObj.code = megCode;
                
                
                //有效期
                int offerstate = [[dic objectForKey:@"offerstate"] intValue];
                
                if (offerstate == 0) {
                    
                    indexObj.offerstate = @"有效";
                    
                }else{
                    
                    indexObj.offerstate = @"过期";
                    
                }
                //是否成交
                int dealstatus = [[dic objectForKey:@"dealstatus"] intValue];
                
                if (dealstatus == 0) {
                    
                    indexObj.dealstatus = @"成交";
                    
                }else{
                    
                    indexObj.dealstatus = @"未成交";
                    
                }
                
                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                //
                
                //审核状态
                
                int auditingstatus = [[dic objectForKey:@"auditingstatus"] intValue];
                
                if (offerstate == 0) {
                    
                    indexObj.auditingstatus = @"未审核";
                    
                }else if(offerstate == 1)
                {
                    indexObj.auditingstatus = @"通过";
                    
                }
                else{
                    
                    indexObj.auditingstatus = @"失败";
                    
                }
                
                
                
                
                //是否删除
                BOOL isdeleted = (BOOL) [dic objectForKey:@"isdeleted"];
                
                //是否发布
                BOOL iscancel = (BOOL) [dic objectForKey:@"iscancel"];
                
                
                //期货类型
                int spotfutures = (int) [dic objectForKey:@"spotfutures"];
                
                
                if (spotfutures == 0) {
                    
                    indexObj.spotfutures = @"期货";
                    
                }else
                {
                    indexObj.spotfutures = @"现货";
                    
                }
                
                //吨数
                int number = (int) [dic objectForKey:@"number"];
                
                
                if (number == 0) {
                    
                    indexObj.number = @"500";
                    
                }else  if (number == 1)
                {
                    indexObj.spotfutures = @"1000";
                    
                }
                else  if (number == 2)
                {
                    indexObj.number = @"1500";
                    
                }
                else  if (number == 3)
                {
                    indexObj.number = @"2000";
                    
                }
                else  if (number == 4)
                {
                    indexObj.number = @"2000";
                    
                }
                else
                {
                    indexObj.number = @"500";
                    
                }
                
                
                //是否免仓
                int storagetime = (int) [dic objectForKey:@"storagetime"];
                
                if (storagetime == 0) {
                    
                    indexObj.storagetime = @"五天";
                    
                }else{
                    
                    indexObj.storagetime = @"七天";
                }
                
                
                
                //保证金
                int cautionmoney = (int) [dic objectForKey:@"cautionmoney"];
                
                if (cautionmoney == 0) {
                    
                    indexObj.cautionmoney = @"0";
                    
                }else if (spotfutures == 1)
                {
                    indexObj.cautionmoney = @"5%";
                    
                }else
                {
                    indexObj.cautionmoney = @"10%";
                    
                }
                
                //备注
                NSString *remarks = (NSString *) [dic objectForKey:@"remarks"];
                
                
                indexObj.remarks = remarks;
                
                //报盘人
                
                int offerpeople = (int)[dic objectForKey:@"offerpeople"];
                
                //发票
                int invoice = (int)[dic objectForKey:@"invoice"];
                
                if (invoice == 0) {
                    
                    indexObj.invoice = @"本月";
                    
                }else
                {
                    indexObj.invoice = @"下月";
                    
                }
                
                
                //勿扰
                
                
                if ( [dic objectForKey:@"notfaze"] != [NSNull null]) {
                    
                    int  notfaze = [[dic objectForKey:@"notfaze"] intValue];
                    
                    if (notfaze == 0) {
                        
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }else
                    {
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }
                    
                }
                
                [_rightArray addObject:indexObj];
                
                [indexObj release];
                
            }
            
            
            if ([offer_results count] != 0 && [_rightArray count ] != 0) {
                
                [_tableView2 reloadData];
                
            }
            
        }
        
        
        
        
        
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"我的报盘请求失败");
}

#pragma mark - dateFromString


- (NSString *)date:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
    
    return auditingDate;
}


- (NSString *)deliveryDate:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
    
    return auditingDate;
}


#pragma mark - commonality

- (void)buttonTitleWithView:(UIView *)aView
{
    
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end








