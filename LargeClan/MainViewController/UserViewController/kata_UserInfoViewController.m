//
//  kata_UserInfoViewController.m
//  LargeClan
//
//  Created by kata on 13-12-23.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_UserInfoViewController.h"
#import "kata_segView.h"
#import "KATAUtils.h"
#import "kata_TwoTableView.h"
#import "KATAUITabBarController.h"
#import "KATAGETArrayDao.h"
#import "NSString+MD5.h"
#import "ASIFormDataRequest.h"
#import "KATAConstants.h"
@interface kata_UserInfoViewController()
{
    
   
}
@property(nonatomic,assign) int nameofpart;

@property(nonatomic,retain)NSString *userId;

@property(nonatomic,retain)NSMutableDictionary *params;

@end

@implementation kata_UserInfoViewController

- (id)initWithUserId:(NSString *)aUserId
{
    if (self = [super init]) {
        
        
        self.userId = aUserId;
        
    }
    return self;
}

- (void)dealloc
{
    [_userId release];
    
    [_params release];
    
    [super dealloc];
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self initViewDidLoadData];
        
}

#pragma mark - initViewDidLoad

- (void)initViewDidLoadData
{
    self.title = @"我的报盘";
    
    self.navigationItem.hidesBackButton = YES;
    
    KATAUITabBarController *tabBarController = (KATAUITabBarController *)self.tabBarController;
    
    [tabBarController hideTabBar];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
    
    
//    kata_segView *segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40) typeIdentifier:202];
//    
//    segView.delegate = self;
//    
//    [self.view addSubview:segView];
//    
//    [segView release];
    
//    
//    kata_TwoTableView *twoTableView  = [[kata_TwoTableView alloc]initWithFrame:CGRectMake(0, 44, ScreenWidth, ScreenHeight - 44)];
//    
//    
//    [self.view addSubview:twoTableView];
//    
//    [twoTableView release];
    
    
    
    
    [self sendRequest];
    

}

- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
//    [[NSNotificationCenter   defaultCenter] postNotificationName:MYOFFERDATA object:dic];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:MYOFFERDATA object:self userInfo:dic];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"我的报盘请求失败");
}







#pragma mark - 

- (void)changeIndex:(int)index
{
  
    //发送不同的请求，等到相应类型的报盘,我的报盘
    
    self.nameofpart = index - 1;
    
    
    [self sendRequest];
    
    
}

#pragma mark - parameter

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offeruser_get";
}

- (NSMutableDictionary *)params
{
    
    DebugLog(@"self.nameofpart ==  %d",self.nameofpart);
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",self.nameofpart];
   
    self.params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
               
               nameofpart,@"nameofpart",
               
               _userId,@"userid",
               
               nil];
    
    return _params;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

- (void)sendRequest
{
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL  URLWithString:SERVER_URI]];
    
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    
    request.timeOutSeconds = 20;
    
    request.delegate  = self;
    
    [request startAsynchronous];
    
    
}




@end
