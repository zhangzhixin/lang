//
//  kata_UserViewController.h
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "kata_RegistViewController.h"
@interface kata_UserViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate,ASIHTTPRequestDelegate,
    kata_RegistViewControllerDelegate>


+(kata_UserViewController *)sharedUserViewController;

@property(nonatomic,copy)NSString *uid;

@end
