//
//  kata_PastTimeViewController.h
//  LargeClan
//
//  Created by kata on 14-1-10.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol kata_PastTimeViewControllerDelegate <NSObject>

- (void)kata_PastTimeViewControllerWithSelectTime:(NSString *)aSelectTime selectTimeId:(int)aSelectTimeId;

@end

@interface kata_PastTimeViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    id <kata_PastTimeViewControllerDelegate> delegate;
    
}
@property(nonatomic,assign)    id <kata_PastTimeViewControllerDelegate> delegate;


@end
