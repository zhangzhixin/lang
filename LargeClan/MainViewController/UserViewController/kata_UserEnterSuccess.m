//
//  kata_UserEnterSuccess.m
//  LargeClan
//
//  Created by kata on 14-1-7.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_UserEnterSuccess.h"
#import "KATAConstants.h"

#import "kata_UserInfoViewController.h"

#import "kata_SearchViewController.h"
#import "KATAUITabBarController.h"


#import "kata_sendMyOfferVeiwController.h"
#import "kata_TransactionViewController.h"
#import "kata_AmendPasswordViewController.h"
#import "kata_AmendInformationViewController.h"

#import "kata_AmendMyOfferViewController.h"

#import "kata_MyOfferViewController.h"

@interface kata_UserEnterSuccess ()


@property(nonatomic,retain)NSString *userId;

@property(nonatomic,retain)NSArray *enterSuccess;
@end

@implementation kata_UserEnterSuccess
- (id)initWithUid:(NSString *)aUid;
{
    if (self = [super init]) {
    
        self.userId = aUid;
    
    }
    return self;
    
}

- (void)dealloc{
    
    [_userId release];
    [_enterSuccess release];
    
    [super dealloc];
    
}
- (void)viewDidLoad
{
    self.title = @"个人中心";
    
    self.navigationItem.hidesBackButton = YES;

    _enterSuccess = [[NSArray alloc]initWithObjects:
               @"我的报盘",  @"成交录入", @"发布报盘",  @"资料修改", @"密码修改",@"注销" ,nil];
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
    
    tableView.backgroundView = nil;
    
    [self.view addSubview:tableView];
    
    [tableView release];

    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

}
- (void)backClick
{
    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    
    [tab showTabBar];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - tableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
   return  [_enterSuccess count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        

        
        
    }
    
    cell.textLabel.text = [_enterSuccess objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            kata_MyOfferViewController *myOfferVC = [[kata_MyOfferViewController alloc]init];
            
            [self.navigationController pushViewController:myOfferVC animated:YES];
            
            [myOfferVC release];

        }
            break;
        case 1:
        {

            kata_TransactionViewController *transactionVC = [[kata_TransactionViewController alloc]init];
            
            [self.navigationController pushViewController:transactionVC animated:YES];
            
            [transactionVC release];
            
        }
            break;
        case 2:
        {
            kata_sendMyOfferVeiwController *sendMyOfferVC = [[kata_sendMyOfferVeiwController alloc]init];
            
            [self.navigationController pushViewController:sendMyOfferVC animated:YES];
            
            [sendMyOfferVC release];
        }
            break;
        case 3:
        {
            kata_AmendInformationViewController *informationVC = [[kata_AmendInformationViewController alloc]init];
            
            [self.navigationController pushViewController:informationVC animated:YES];
            
            [informationVC release];
            
        }
            break;
        case 4:
        {
            
            kata_AmendPasswordViewController *passwordVC = [[kata_AmendPasswordViewController alloc]init];
            
            [self.navigationController pushViewController:passwordVC animated:YES];
            
            
            [passwordVC release];
            
            
        }
            break;
        case 5: // 注销
        {
            
            UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:@"温馨提示：" message:@"确定注销账号吗？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
            
            [alert show];
            
            [alert release];
            
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark - 


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
        {
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
            break;
        case 1:
        {
            
        }
            break;
        default:
            break;
    }
    
}


@end
