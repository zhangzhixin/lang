//
//  kata_MyOfferViewController.m
//  LargeClan
//
//  Created by kata on 14-1-14.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_MyOfferViewController.h"
#import "KATAUITabBarController.h"

#import "kata_segView.h"
#import "KATAConstants.h"
#import "kata_LeftCell.h"
#import "kata_TwoTableView.h"
#import "ASIFormDataRequest.h"
#import "kata_UserViewController.h"
#import "NSString+MD5.h"
#import "KATAConstants.h"
#import "KATAUtils.h"
#import "kata_UserViewController.h"

#import "KATAIndex.h"

#import "MBProgressHUD.h"

#import "kata_MyOffer0ViewController.h"
#import "kata_MyOffer1ViewController.h"
#import "kata_MyOffer2ViewController.h"
#import "kata_MyOffer3ViewController.h"

#import "kata_MyMEGInsideView.h"

#import "kata_sendMyOfferVeiwController.h"

#import "kata_AmendMyOfferViewController.h"


@interface kata_MyOfferViewController ()
{

    UITableView * _tableView1;
    UITableView * _tableView2;
    UIScrollView *_scrollView;
    
    kata_MyOffer1ViewController *_myOffer1VC;
    
    kata_MyOffer0ViewController *_myOffer0VC;
    
    kata_MyOffer2ViewController *_myOffer2VC;
    
    kata_MyOffer3ViewController *_myOffer3VC;
}

@property(nonatomic,retain)NSArray *tableView1Array;
@property(nonatomic,retain)NSArray *tableView2Array;
@property(nonatomic,retain)NSMutableDictionary *params;
@property(nonatomic,assign) int nameofpart;
@property(nonatomic,retain)NSMutableArray *leftArray;
@property(nonatomic,retain)NSMutableArray *rightArray;
@property(nonatomic,retain)NSMutableArray *rightHeardArray;

@end


@implementation kata_MyOfferViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        
    
    }
    return self;
}

- (void)dealloc
{
    [_myOffer0VC release];
    
    [_myOffer1VC release];
    
    [_myOffer2VC release];
    
    [_myOffer3VC release];
    
    [_rightHeardArray release];
    
    [_rightArray release];
    
    [_leftArray release];
    
    [_scrollView release];
    
    [_params release];
    
    [_tableView1Array release];
    
    [_tableView2Array release];
    
    
    [_tableView1 release];
    
    [_tableView2 release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"我的报盘";
    
    self.navigationItem.hidesBackButton = YES;
    
    KATAUITabBarController *tabBarController = (KATAUITabBarController *)self.tabBarController;
    
    [tabBarController hideTabBar];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    
    kata_segView *segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40) typeIdentifier:202];
    
    segView.delegate = self;
    
    [self.view addSubview:segView];
    
    [segView release];


    
    _myOffer0VC = [[kata_MyOffer0ViewController alloc]init];
    
    _myOffer0VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);
    
    _myOffer0VC.delegate = self;
    
    [self.view addSubview:_myOffer0VC.view];
    

    _myOffer1VC = [[kata_MyOffer1ViewController alloc]init];
    
    _myOffer1VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);

    _myOffer2VC = [[kata_MyOffer2ViewController alloc]init];
    
    _myOffer2VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);

    _myOffer3VC = [[kata_MyOffer3ViewController alloc]init];
    
    _myOffer3VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);
    
    
}
#pragma mark - buttonEvent

-(void)backClick
{
    
    [self.navigationController  popViewControllerAnimated:YES];
    
}
#pragma mark - changeIndex

- (void)changeIndex:(int)index
{
    
    //发送不同的请求，等到相应类型的报盘,我的报盘
    
    self.nameofpart = index - 1;
    
    switch (self.nameofpart) {
        case 0:
        {

            [_myOffer1VC.view removeFromSuperview];
            [_myOffer2VC.view removeFromSuperview];
            [_myOffer3VC.view removeFromSuperview];
            
            [self.view addSubview:_myOffer0VC.view];

        }
            break;
        case 1:
        {
            
            [_myOffer0VC.view removeFromSuperview];
            [_myOffer2VC.view removeFromSuperview];
            [_myOffer3VC.view removeFromSuperview];
            
            
            [self.view addSubview:_myOffer1VC.view];
            
        }
            break;
        case 2:
        {
            [_myOffer1VC.view removeFromSuperview];
            [_myOffer3VC.view removeFromSuperview];
            
            [_myOffer0VC.view removeFromSuperview];
            
            [self.view addSubview:_myOffer2VC.view];
            
        }
            break;
        case 3:
        {
            [_myOffer1VC.view removeFromSuperview];
            [_myOffer2VC.view removeFromSuperview];
            
            [_myOffer0VC.view removeFromSuperview];
            
            [self.view addSubview:_myOffer3VC.view];
            
        }
            break;
            
        default:
            break;
    }
       
}

#pragma mark -  kata_MyOffer0ViewControllerPushSendMyOffer

- (void)kata_MyOffer0ViewControllerPushSendMyOffer
{
    
//    kata_sendMyOfferVeiwController *sendMyOffer = [[kata_sendMyOfferVeiwController alloc]init];
//    
//    [self.navigationController pushViewController:sendMyOffer animated:YES];
//    
//    
//    [sendMyOffer release];

    
}


//修改我的报盘
- (void)kata_MyOffer0ViewControllerPushAmendMyOffer
{
    
    kata_AmendMyOfferViewController *amendMyOfferVC = [[kata_AmendMyOfferViewController alloc]init];
    
    [self.navigationController pushViewController:amendMyOfferVC animated:YES];
    
    [amendMyOfferVC  release];

    
    
}
/*
#pragma mark - buttonEvent

-(void)backClick
{

    [self.navigationController  popToRootViewControllerAnimated:YES];
    
}

- (void)buttonClick:(id)sender
{
    
    
}

#pragma mark - 
#pragma mark - rightTableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (tableView.tag == 1) {
        
        UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        vi.backgroundColor = [UIColor blueColor];
        
            UILabel *numberLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
            
            numberLbl.text = @"报盘时间";
            
            numberLbl.adjustsFontSizeToFitWidth = YES;
            
            numberLbl.textAlignment = NSTextAlignmentCenter;
            
            [vi addSubview:numberLbl];
            
            [numberLbl release];
        
        return [vi autorelease];

        
    }
    
   else {
        
        UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, LBLWIDTH * [self.tableView2Array count], 44)];
        
        vi.backgroundColor = [UIColor blueColor];
        
        
        for (int i = 0; i < [self.tableView2Array count]; i++) {
            
            UILabel *numberLbl = [[UILabel alloc]initWithFrame:CGRectMake(LBLWIDTH * i, 0, LBLWIDTH, 44)];
            
            numberLbl.text = [_tableView2Array objectAtIndex:i];
            
            numberLbl.adjustsFontSizeToFitWidth = YES;
            
            numberLbl.textAlignment = NSTextAlignmentCenter;
            
            [vi addSubview:numberLbl];
            
            [numberLbl release];
            
        }
        
        return [vi autorelease];

    }
    
    
    
  
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [_leftArray  count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     //左 //左
    if (tableView.tag == 1) {  //左
        
        static  NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[[kata_LeftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
        }
        
        
        UILabel *numberLab = (UILabel *)[cell viewWithTag:112];
        
        if ([_leftArray count] !=  0) {
            KATAIndex *indexObj = [_leftArray objectAtIndex:indexPath.row];
            
            //报盘编号
            
            [numberLab setFont:[UIFont  systemFontOfSize:8.0f]];
            
//            numberLab.text  = indexObj.code;
            
            numberLab.text  = indexObj.auditingstatus;

            
            
            UIImageView *imageView1 = (UIImageView *)[cell viewWithTag:11];
            
            
            if ([indexObj.notfaze isEqualToString:@"0"]) {
                
                [imageView1 setImage:[UIImage imageNamed:@"w.png"]];
                
            }else
            {
                
                imageView1.backgroundColor = [UIColor clearColor];
                
            }


        }
        
        
        UIImageView *imageView2 = (UIImageView *)[cell viewWithTag:12];
        
        [imageView2 setImage:[UIImage imageNamed:@"rz.png"]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //    cell.textLabel.text = @"left";
        
        return cell;
        
        
        
    }else  //右 //右
 
    {
        static  NSString *CellIdentifier0 = @"Cell0";
        
        static  NSString *CellIdentifier1 = @"Cell1";
        
        static  NSString *CellIdentifier2 = @"Cell2";
        
        static  NSString *CellIdentifier3 = @"Cell3";
        
        
        //    if ([_rightContentArr count] == 0) {
        //
        
        
        KATAIndex *indexObj = [_rightArray objectAtIndex:indexPath.row];
        
        
        int nameofpart = indexObj.nameofpart;
        
        if (nameofpart == 0) {
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier0];
            
            
            
            if (!cell) {
                
                cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier0] autorelease];
                
                for (int  i = 0; i < [_rightArray  count];  i ++)
                {
                    
                    if ((i >= 0 && i < 12 )|| i == [_rightArray count]) {
                        
                        
                        
                        UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                        
                        [lab setFont:[UIFont systemFontOfSize:14]];
                        
                        lab.tag =  i + 10;
                        
                        lab.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lab];
                        
                        [lab release];
                        

                    }
                    
                    if (i == 14) {
                        
                        UITextField *textFiled = [[UITextField alloc]initWithFrame:CGRectMake(14 *LBLWIDTH, 7, LBLWIDTH, 30)];
                        
                        textFiled.borderStyle = UITextBorderStyleLine;
                        
                        
                        [cell addSubview:textFiled];
                        
                        [textFiled release];
                    }
                    
                    if ( i == 12 || i == 13  ||  i == 15 ) {
                        
                        
                        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                        
                        button.tag =  i + 10;
                        
                        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
                        
                        button.backgroundColor = [UIColor redColor];
                        
                        button.frame =  CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44);
                        
                        [cell  addSubview:button];
                        
                        
                        
                    }
                    
                    
                    
                }
                
                
                
            }
            
            
          
    
            UIButton *button = (UIButton *)[cell viewWithTag:22];
            
            [button setTitle:@"修改报盘" forState:UIControlStateNormal];
            
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            UIButton *button2 = (UIButton *)[cell viewWithTag:23];
            
            [button2 setTitle:@"取消报盘" forState:UIControlStateNormal];
            
            [button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            UIButton *button3 = (UIButton *)[cell viewWithTag:25];
            
            [button3 setTitle:@"成交" forState:UIControlStateNormal];
            
            [button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            


            
            
            
                     //编号
            UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
            
            timeLab.text = indexObj.code;
            
            //方向
            UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
            
            directionLab.text = indexObj.direction;
            
            //人民币
            UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
            
            quotationLab.text  = indexObj.quotation;
            
            //货种
            UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:13];
            
            spotfuturesLab.text = indexObj.spotfutures;
            
            
            //                    //交割时间
            UILabel *deliveryLab = (UILabel *)[cell viewWithTag:14];
            
            deliveryLab.text = indexObj.deliverytime;
            
            //                    //交割地
            UILabel *deliverynameLab = (UILabel *)[cell viewWithTag:15];
            
            deliverynameLab.text = indexObj.deliveryname;
            
            
            //保证金
            UILabel *cautionmoneyLab = (UILabel *)[cell viewWithTag:16];
            
            cautionmoneyLab.text = indexObj.cautionmoney;
            
            
            //数量
            UILabel *numberLab = (UILabel *)[cell viewWithTag:17];
            
            numberLab.text = indexObj.number;
            
            
            //发票
            UILabel *invoiceLab = (UILabel *)[cell viewWithTag:18];
            
            invoiceLab.text = indexObj.invoice;
            
            //免仓
            UILabel *storagetimeLab = (UILabel *)[cell viewWithTag:19];
            storagetimeLab.text = indexObj.storagetime;
            
            //                    //状态
            UILabel *offerstateLab = (UILabel *)[cell viewWithTag:20];
            
            offerstateLab.text = indexObj.state;
            
            
            //                    //审核状态
            UILabel *auditingstatus = (UILabel *)[cell viewWithTag:21];
            
            auditingstatus.text  = indexObj.auditingstatus;
//
//            //                    //修改报盘
//            UILabel *remarksLab2 = (UILabel *)[cell viewWithTag:22];
//            
//            remarksLab2.text  = indexObj.remarks;
//            
//            //                    //发布操作
//            UILabel *remarksLab3 = (UILabel *)[cell viewWithTag:23];
//            
//            remarksLab3.text  = indexObj.remarks;
//            
//            //                    //成交价格
//            UILabel *remarksLab4 = (UILabel *)[cell viewWithTag:24];
//            
//            remarksLab4.text  = indexObj.remarks;
//            
//
//            //                    //成交状态
//            UILabel *remarksLab5 = (UILabel *)[cell viewWithTag:25];
//            
//            remarksLab5.text  = indexObj.remarks;
            
            
            //                    //备注
//            UILabel *remarksLab = (UILabel *)[cell viewWithTag:26];
//            
//            remarksLab.text  = indexObj.remarks;

            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
        }else if(nameofpart == 1){
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            
            
            
            if (!cell) {
                
                cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
                
                for (int  i = 0; i < [_rightArray  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    lab.textAlignment = NSTextAlignmentCenter;
                    
                    [cell addSubview:lab];
                    
                    [lab release];
                    
                    
                    if (lab.tag == 21) {
                        
                        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                        
                        imageView.tag = 222;
                        
                        //                        imageView.backgroundColor = [UIColor redColor];
                        
                        [lab addSubview:imageView];
                        
                        [imageView release];
                        
                        
                    }
                    
                }
                
            }
            
            UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
            
            [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
            
            //时间
            UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
            
            timeLab.text = indexObj.auditingstatus;
            
            // 方向
            UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
            
            directionLab.text = indexObj.direction;
            
            //美金
            UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
            
            quotationLab.text  = indexObj.quotation;
            
            //货种
            UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:13];
            
            spotfuturesLab.text = indexObj.cargobonded;
            
            
            //                    //货源地
            UILabel *goodssourcenameLab = (UILabel *)[cell viewWithTag:14];
            
            goodssourcenameLab.text = indexObj.goodssourcename;
            
            
            //                    //预计到港时间  开始
            
            UILabel *loadingtime1Lab = (UILabel *)[cell viewWithTag:15];
            
            loadingtime1Lab.text = indexObj.loadingtime1;
            
            
            //                    //预计到港时间 结束
            
            UILabel *loadingtime2Lab = (UILabel *)[cell viewWithTag:16];
            
            loadingtime2Lab.text = indexObj.loadingtime2;
            
            
            
            //转手
            UILabel *changehandsLab = (UILabel *)[cell viewWithTag:17];
            
            changehandsLab.text = indexObj.changehands;
            
            //交单
            UILabel *surrenderdocumentsLab = (UILabel *)[cell viewWithTag:18];
            
            surrenderdocumentsLab.text = indexObj.surrenderdocuments;
            
            //交割地
            UILabel *regionLab = (UILabel *)[cell viewWithTag:19];
            
            regionLab.text = indexObj.deliveryname;
            
            //数量
            UILabel *numberLab = (UILabel *)[cell viewWithTag:20];
            
            numberLab.text = indexObj.number;
            
            //                   报盘人
            //                UILabel *offerstateLab = (UILabel *)[cell2Out viewWithTag:20];
            
            //                offerstateLab.text = indexObj.offerstate;
            
            //                    //清单
            
            UILabel *detailedlistLab = (UILabel *)[cell viewWithTag:21];
            
            detailedlistLab.text  = indexObj.detailedlist;
            
            //                    //付款方式
            UILabel *paymentmethodsLab = (UILabel *)[cell viewWithTag:22];
            
            paymentmethodsLab.text  = indexObj.paymentmethods;
            
            //                    //状态
            UILabel *offerstateLab = (UILabel *)[cell viewWithTag:23];
            
            offerstateLab.text  = indexObj.state;
            
            
            //                    //备注
            UILabel *remarksLab = (UILabel *)[cell viewWithTag:24];
            
            remarksLab.text  = indexObj.remarks;
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
            
            
            
        }else if(nameofpart == 2){
            
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            
            
            
            if (!cell) {
                
                cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                
                for (int  i = 0; i < [_rightHeardArray  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    lab.textAlignment = NSTextAlignmentCenter;
                    
                    [cell addSubview:lab];
                    
                    [lab release];
                    
                    if (lab.tag == 19) {
                        
                        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                        
                        imageView.tag = 222;
                        
                        //                    imageView.backgroundColor = [UIColor redColor];
                        
                        [lab addSubview:imageView];
                        
                        [imageView release];
                        
                        
                    }
                    
                }
                
                
                
            }
            
            
            UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
            
            [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
            
            //时间
            UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
            
            timeLab.text = indexObj.auditingstatus;
            
            //方向
            UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
            
            directionLab.text = indexObj.direction;
            
            //人民币
            UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
            
            quotationLab.text  = indexObj.quotation;
            
            //配送
            UILabel *dispatchingLab = (UILabel *)[cell viewWithTag:13];
            
            dispatchingLab.text = indexObj.dispatching;
            
            //品牌
            UILabel *brandid = (UILabel *)[cell viewWithTag:14];
            
            brandid.text = indexObj.brand;
            
            
            //货种
            UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:15];
            
            spotfuturesLab.text = indexObj.spotfutures;
            
            
            //交割时间
            UILabel *deliverytimeLab = (UILabel *)[cell viewWithTag:16];
            
            deliverytimeLab.text = indexObj.deliverytime;
            
            
            //交割地
            
            UILabel *deliverynameLab = (UILabel *)[cell viewWithTag:17];
            
            deliverynameLab.text = indexObj.deliveryname;
            
            
            
            //数量
            UILabel *numberLab = (UILabel *)[cell viewWithTag:18];
            
            numberLab.text = indexObj.number;
            
            //报盘人
            UILabel *offerpeopleLab = (UILabel *)[cell viewWithTag:19];
            
            //        offerpeopleLab.text = indexObj.offerpeople;
            
            //保证金
            UILabel *cautionmoneyLab = (UILabel *)[cell viewWithTag:20];
            
            cautionmoneyLab.text = indexObj.cautionmoney;
            
            //发票
            UILabel *invoiceLab = (UILabel *)[cell viewWithTag:21];
            
            invoiceLab.text = indexObj.invoice;
            
            //   状态
            UILabel *stateLab = (UILabel *)[cell viewWithTag:22];
            
            stateLab.text = indexObj.state;
            
            //                    //备注
            UILabel *remarksLab = (UILabel *)[cell viewWithTag:23];
            
            remarksLab.text  = indexObj.remarks;
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
            
        }else {
            
            
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
            
            
            
            if (!cell) {
                
                cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
                
                for (int  i = 0; i < [_rightHeardArray  count];  i ++)
                {
                    
                    UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                    
                    [lab setFont:[UIFont systemFontOfSize:14]];
                    
                    lab.tag =  i + 10;
                    
                    lab.textAlignment = NSTextAlignmentCenter;
                    
                    [cell addSubview:lab];
                    
                    [lab release];
                    
                    
                    if (lab.tag == 22) {
                        
                        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                        
                        imageView.tag = 222;
                        
                        //                    imageView.backgroundColor = [UIColor redColor];
                        
                        [lab addSubview:imageView];
                        
                        [imageView release];
                        
                        
                    }
                    
                }
                
            }
            
            UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
            
            [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
            
            UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
            
            timeLab.text = indexObj.auditingstatus;
            
            
            UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
            
            directionLab.text = indexObj.direction;
            
            //美金
            UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
            
            quotationLab.text  = indexObj.quotation;
            
            //货种
            UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:13];
            
            spotfuturesLab.text = indexObj.cargobonded;
            
            
            //                    //货源地
            UILabel *goodssourcenameLab = (UILabel *)[cell viewWithTag:14];
            
            goodssourcenameLab.text = indexObj.goodssourcename;
            
            //品牌
            
            UILabel *brandLab = (UILabel *)[cell viewWithTag:15];
            
            brandLab.text = indexObj.brand;
            
            
            
            //                    //预计到港时间  开始
            
            UILabel *loadingtime1Lab = (UILabel *)[cell viewWithTag:16];
            
            loadingtime1Lab.text = indexObj.loadingtime1;
            
            
            //                    //预计到港时间 结束
            
            UILabel *loadingtime2Lab = (UILabel *)[cell viewWithTag:17];
            
            loadingtime2Lab.text = indexObj.loadingtime2;
            
            
            
            //转手
            UILabel *changehandsLab = (UILabel *)[cell viewWithTag:18];
            
            changehandsLab.text = indexObj.changehands;
            
            //交单
            UILabel *surrenderdocumentsLab = (UILabel *)[cell viewWithTag:19];
            
            surrenderdocumentsLab.text = indexObj.surrenderdocuments;
            
            //交割地
            UILabel *deliverynameLab = (UILabel *)[cell viewWithTag:20];
            
            deliverynameLab.text = indexObj.deliveryname;
            
            //数量
            UILabel *numberLab = (UILabel *)[cell viewWithTag:21];
            
            numberLab.text = indexObj.number;
            
            //                   报盘人
            
            UILabel *offerstateLab = (UILabel *)[cell viewWithTag:22];
            
            //                offerstateLab.text = indexObj.offerstate;
            
            //                    //清单
            
            UILabel *detailedlistLab = (UILabel *)[cell viewWithTag:23];
            
            detailedlistLab.text  = indexObj.detailedlist;
            
            //                    //付款方式
            UILabel *paymentmethodsLab = (UILabel *)[cell viewWithTag:24];
            
            paymentmethodsLab.text  = indexObj.paymentmethods;
            
            //                    //状态
            UILabel *stateLab = (UILabel *)[cell viewWithTag:25];
            
            stateLab.text  = indexObj.state;
            
            
            //                    //备注
            UILabel *remarksLab = (UILabel *)[cell viewWithTag:26];
            
            remarksLab.text  = indexObj.remarks;
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
            
        }
        
        
        
    
    }



}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}


#pragma mark - parameter

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offeruser_get";
}

- (NSMutableDictionary *)params
{
    
    DebugLog(@"self.nameofpart ==  %d",self.nameofpart);
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",self.nameofpart];
    
    NSString *uid = [kata_UserViewController sharedUserViewController].uid;
    
    self.params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   
                   nameofpart,@"nameofpart",
                   
                   uid,@"userid",
                   
                   nil];
    
    return _params;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

- (void)sendRequest
{
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL  URLWithString:SERVER_URI]];
    
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    
    request.timeOutSeconds = 20;
    
    request.delegate  = self;
    
    [request startAsynchronous];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}


#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    
      // 左  // 左
    
    [_leftArray removeAllObjects];
    
    DebugLog(@"dic ==  %@",dic);
    
    NSString *code = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"code"] intValue]];
    
    if ([dic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        if ([offer_results count] != 0) {
            
            for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                //报盘时间
                if ([dic objectForKey:@"offertime"]  != [NSNull null])
                    
                {
                    long long time = [[[dic objectForKey:@"offertime"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
                    
                    NSLog(@"confromTimesp ==  %@",confromTimesp);
                    
                    
                    indexObj.auditingstatus = [self date:confromTimesp];
                    
                }
                

//                NSString *megCode = [dic objectForKey:@"code"];
//                
//                indexObj.code = megCode;
                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                
                [self.leftArray addObject:indexObj];
                
                [indexObj release];
                
            }
            
        }
        if ([offer_results count] != 0 && [_leftArray count ] != 0) {
            
            [_tableView1 reloadData];
            
        }
        
    }
    
    
    DebugLog(@"    indexObj.code  ==  %d",    [_leftArray count] );
    
    [_tableView1 reloadData];

    
    
    
    
    
    
    // 右  // 右
    
    
    [_rightArray removeAllObjects];
    
    
    DebugLog(@"dic ==  %@  dic retainCount ===  %d",dic,[dic retainCount]);
    
    //    NSString *code = [dic objectForKey:@"code"];
    
    if ([dic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        //        DebugLog(@"dic ==  %@  dic retainCount ===  %d",dic,[dic retainCount]);
        
        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        
        if ([offer_results count] != 0) {
            
            for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                //报盘时间
                if ([dic objectForKey:@"auditingDate"]  != [NSNull null])
                    
                {
                    long long time = [[[dic objectForKey:@"auditingDate"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
                    
                    //        NSLog(@"confromTimesp ==  %@",confromTimesp);
                    
                    
                    indexObj.auditingstatus = [self date:confromTimesp];
                    
                }
                
                
                //交割时间
                
                if ([dic objectForKey:@"deliverytime2"]  != [NSNull null] && [dic objectForKey:@"deliverytime2"] != [NSNull null]) {
                    
                    
                    
                    long long deliveryTime1 = [[[dic objectForKey:@"deliverytime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime1 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.deliverytime1 = [self deliveryDate:confromTimesp1];
                    
                    
                    long long deliveryTime2 = [[[dic objectForKey:@"deliverytime2"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp2 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime2 / 1000)];
                    
                    NSLog(@"confromTimesp2 ==  %@",confromTimesp2);
                    
                    indexObj.deliverytime2 = [self deliveryDate:confromTimesp2];
                    
                    NSString *deliveryStr1  = [indexObj.deliverytime1 stringByReplacingOccurrencesOfString:@"GMT+" withString:@"-"];
                    
                    NSString *deliveryStr2  = [indexObj.deliverytime2 stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                    
                    indexObj.deliverytime = (NSMutableString *)[deliveryStr1 stringByAppendingString:deliveryStr2];
                    
                    DebugLog(@"indexObj.deliverytime ==  %@",indexObj.deliverytime);
                    
                    
                }
                
                
                //预计装船开始时间
                if ([dic objectForKey:@"loadingtime1"]  != [NSNull null]) {
                    
                    
                    
                    long long loadingtime1 = [[[dic objectForKey:@"loadingtime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime1 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.loadingtime1 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                }
                
                //预计装船结束时间
                
                if ([dic objectForKey:@"arrivetime1"]  != [NSNull null]) {
                    
                    
                    
                    long long loadingtime2 = [[[dic objectForKey:@"arrivetime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime2 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.loadingtime2 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                }
                //
                //货源地
                
                if ( [dic objectForKey:@"goodsSource"] != [NSNull null]) {
                    
                    
                    NSDictionary *goodsSource  =[dic objectForKey:@"goodsSource"];
                    
                    NSString *goodssourcename = [goodsSource objectForKey:@"goodssourcename"];
                    
                    indexObj.goodssourcename = goodssourcename;
                    
                }
                
                //
                //交割地
                
                if ( [dic objectForKey:@"region"] != [NSNull null]) {
                    
                    NSDictionary *regionDic  = [dic objectForKey:@"region"];
                    
                    NSString *deliveryname = [regionDic objectForKey:@"deliveryname"];
                    
                    indexObj.deliveryname = deliveryname;
                    
                    
                }
                
                // 转手
                if ( [dic objectForKey:@"changehands"] != [NSNull null]) {
                    
                    int  changehands =  [[dic objectForKey:@"changehands"] intValue];
                    
                    DebugLog(@"changehands = %d",changehands);
                    
                    indexObj.changehands = [NSString stringWithFormat:@"%d手",changehands];
                    
                    DebugLog(@"indexObj.changehands = %@",indexObj.changehands);
                }
                
                // 交单天数
                
                if ( [dic objectForKey:@"surrenderdocuments"] != [NSNull null]) {
                    
                    int  surrenderdocuments =  [[dic objectForKey:@"surrenderdocuments"] intValue];
                    
                    indexObj.surrenderdocuments = [NSString stringWithFormat:@"%d天",surrenderdocuments];
                    
                }
                
                // 付款方式
                
                if ( [dic objectForKey:@"paymentmethods"] != [NSNull null]) {
                    
                    int  paymentmethods =  [[dic objectForKey:@"paymentmethods"] intValue];
                    
                    
                    if (paymentmethods == 0) {
                        
                        indexObj.paymentmethods = @"L/C90 天";
                        
                    }else if (paymentmethods == 1) {
                        
                        indexObj.paymentmethods = @"L/C 即期";
                        
                    }
                    else if (paymentmethods == 2) {
                        
                        indexObj.paymentmethods = @"L/T ";
                        
                    }
                    else{
                        indexObj.paymentmethods = @"其他";
                        
                    }
                    
                    
                    
                }
                
                // 单据类型
                
                if ( [dic objectForKey:@"detailedlist"] != [NSNull null]) {
                    
                    int  detailedlist = [[dic objectForKey:@"detailedlist"] intValue];
                    
                    if (detailedlist == 0) {
                        
                        indexObj.detailedlist = @"是";
                        
                    }else
                    {
                        indexObj.detailedlist = @"否";
                        
                    }
                    
                }
                
                //品名
                
                if ( [dic objectForKey:@"nameofpart"] != [NSNull null]){
                    
                    int nameofpart = [[dic objectForKey:@"nameofpart"] intValue];
                    
                    indexObj.nameofpart = nameofpart;
                    
                    
                }
                //税 类型
                
                if ( [dic objectForKey:@"cargobonded"] != [NSNull null]){
                    
                    
                    int cargobonded = (int)[dic objectForKey:@"cargobonded"];
                    
                    
                    if (cargobonded == 0) {
                        
                        indexObj.cargobonded = @"船税";
                    }else
                    {
                        indexObj.cargobonded = @"保税";
                        
                    }
                    
                }
                
                
                //配送
                if ( [dic objectForKey:@"dispatching"] != [NSNull null]){
                    
                    int dispatching = [[dic objectForKey:@"dispatching"] intValue];
                    
                    if (dispatching == 0) {
                        
                        indexObj.dispatching = @"自提";
                        
                    }else{
                        
                        indexObj.dispatching = @"送到";
                    }
                }
                
                //品牌
                if ( [dic objectForKey:@"brand"] != [NSNull null]){
                    
                    indexObj.brand =  [[dic objectForKey:@"brand"] objectForKey:@"brandname"];
                    
                }
                
                
                //        //价格
                
                NSString *quotation  = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"quotation"] intValue]];
                
                indexObj.quotation = quotation;
                
                
                //方向
                int  direction =  [[dic objectForKey:@"direction"] intValue];
                
                DebugLog(@"directiondirection ==  %d",direction);
                
                if (direction == 0) {
                    
                    indexObj.direction = @"卖盘";
                    
                }else
                {
                    indexObj.direction = @"买盘";
                    
                }
                
                
                
                NSString *megCode = [dic objectForKey:@"code"];
                
                indexObj.code = megCode;
                
                
                //有效期
                int offerstate = [[dic objectForKey:@"offerstate"] intValue];
                
                if (offerstate == 0) {
                    
                    indexObj.offerstate = @"有效";
                    
                }else{
                    
                    indexObj.offerstate = @"过期";
                    
                }
                //是否成交
                int dealstatus = [[dic objectForKey:@"dealstatus"] intValue];
                
                if (dealstatus == 0) {
                    
                    indexObj.dealstatus = @"/成交";
                    
                }else{
                    
                    indexObj.dealstatus = @"/未成交";
                    
                }
                
                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                //
                
                //审核状态
                
                int auditingstatus = [[dic objectForKey:@"auditingstatus"] intValue];
                
                if (offerstate == 0) {
                    
                    indexObj.auditingstatus = @"未审核";
                    
                }else if(offerstate == 1)
                {
                    indexObj.auditingstatus = @"通过";

                }
                else{
                    
                    indexObj.auditingstatus = @"失败";
                    
                }

                
        
                
                //是否删除
                BOOL isdeleted = (BOOL) [dic objectForKey:@"isdeleted"];
                
                //是否发布
                BOOL iscancel = (BOOL) [dic objectForKey:@"iscancel"];
                
                
                //期货类型
                int spotfutures = (int) [dic objectForKey:@"spotfutures"];
                
                
                if (spotfutures == 0) {
                    
                    indexObj.spotfutures = @"期货";
                    
                }else
                {
                    indexObj.spotfutures = @"现货";
                    
                }
                
                //吨数
                int number = (int) [dic objectForKey:@"number"];
                
                
                if (number == 0) {
                    
                    indexObj.number = @"500";
                    
                }else  if (number == 1)
                {
                    indexObj.spotfutures = @"1000";
                    
                }
                else  if (number == 2)
                {
                    indexObj.number = @"1500";
                    
                }
                else  if (number == 3)
                {
                    indexObj.number = @"2000";
                    
                }
                else  if (number == 4)
                {
                    indexObj.number = @"2000";
                    
                }
                else
                {
                    indexObj.number = @"500";
                    
                }
                
                
                //是否免仓
                int storagetime = (int) [dic objectForKey:@"storagetime"];
                
                if (storagetime == 0) {
                    
                    indexObj.storagetime = @"五天";
                    
                }else{
                    
                    indexObj.storagetime = @"七天";
                }
                
                
                
                //保证金
                int cautionmoney = (int) [dic objectForKey:@"cautionmoney"];
                
                if (cautionmoney == 0) {
                    
                    indexObj.cautionmoney = @"0";
                    
                }else if (spotfutures == 1)
                {
                    indexObj.cautionmoney = @"5%";
                    
                }else
                {
                    indexObj.cautionmoney = @"10%";
                    
                }
                
                //备注
                NSString *remarks = (NSString *) [dic objectForKey:@"remarks"];
                
                
                indexObj.remarks = remarks;
                
                //报盘人
                
                int offerpeople = (int)[dic objectForKey:@"offerpeople"];
                
                //发票
                int invoice = (int)[dic objectForKey:@"invoice"];
                
                if (invoice == 0) {
                    
                    indexObj.invoice = @"本月";
                    
                }else
                {
                    indexObj.invoice = @"下月";
                    
                }
                
                
                //勿扰
                
                
                if ( [dic objectForKey:@"notfaze"] != [NSNull null]) {
                    
                    int  notfaze = [[dic objectForKey:@"notfaze"] intValue];
                    
                    if (notfaze == 0) {
                        
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }else
                    {
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }
                    
                }
                
                [_rightArray addObject:indexObj];
                
                [indexObj release];
                
            }
            
            
            if ([offer_results count] != 0 && [_rightArray count ] != 0) {
                
                [_tableView2 reloadData];
                
            }
            
        }
        
        
        
        
        
    }


}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"我的报盘请求失败");
}

#pragma mark -

- (void)changeIndex:(int)index
{
    
    //发送不同的请求，等到相应类型的报盘,我的报盘
    
    self.nameofpart = index - 1;
    
    
    switch (self.nameofpart) {
        case 0:
        {
            
              [self sendRequest];
            
            
            
            

        }
            break;
        case 1:
        {
           _myOffer1VC  = [[kata_MyOffer1ViewController alloc ]init];
            
                        
            [self.view addSubview:_myOffer1VC.view];
            
//            [myOffer1VC release];
            
        }
            break;
        case 2:
        {
            
        }
            break;
        case 3:
        {
            
        }
            break;
        default:
            break;
    }
    
    
    
    
}
#pragma mark - dateFromString


- (NSString *)date:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
    
    return auditingDate;
}


- (NSString *)deliveryDate:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
    
    return auditingDate;
}


#pragma mark - commonality

- (void)buttonTitleWithView:(UIView *)aView
{
    
    
    
}


*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
