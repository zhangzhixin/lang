//
//  kata_AmendPasswordViewController.h
//  LargeClan
//
//  Created by kata on 14-1-7.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface kata_AmendPasswordViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,
    UITextFieldDelegate,
    ASIHTTPRequestDelegate>

@end
