//
//  kata_AmendInformationViewController.h
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
@interface kata_AmendInformationViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,
    UITextFieldDelegate,
    UIActionSheetDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UIPickerViewDataSource,UIPickerViewDelegate,
    ASIHTTPRequestDelegate>

@end
