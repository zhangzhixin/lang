//
//  kata_MyOffer1ViewController.h
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "kata_segView.h"
#import "ASIHTTPRequest.h"

#import "kata_LeftTableDelegate.h"

@protocol kata_MyOffer1ViewControllerDelegate <NSObject>

- (void)kata_MyOffer1ViewControllerPushAmendMyOffer;


@end

@interface kata_MyOffer1ViewController : UIViewController <kata_segViewDelegate,UITableViewDataSource,UITableViewDelegate,
    UIActionSheetDelegate,
    UIAlertViewDelegate>
{
    
    id <kata_MyOffer1ViewControllerDelegate> delegate;
    
}
@property(nonatomic,assign)     id <kata_MyOffer1ViewControllerDelegate> delegate;


- (NSMutableDictionary *)paramsTransaction:(NSString *)uid offerId:(NSString *)aOfferId;
- (NSString *)signTransaction;
- (NSString *)methodTransaction;

@end
