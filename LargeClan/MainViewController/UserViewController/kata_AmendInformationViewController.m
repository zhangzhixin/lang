//
//  kata_AmendInformationViewController.m
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_AmendInformationViewController.h"

#import "KATAConstants.h"

#import "KATAUtils.h"

#import "ASIFormDataRequest.h"

#import "NSString+MD5.h"
#import "kata_addressDao.h"

@interface kata_AmendInformationViewController ()

{
    UITableView *_tableView;
    
    UIImagePickerController *_imagePicker1;
    UIImagePickerController *_imagePicker2;
    
    UIImageView *_imageView;
    UIImageView *_imageView2;

    
    int _cellImageViewRow;
    
    UIPickerView *_picker;
}
@property(nonatomic,retain)NSMutableDictionary *postDic;

@property(nonatomic,retain)NSArray *informintionArray;

@property(nonatomic,retain)NSString *offerTypeStr;

@property(nonatomic,retain)NSMutableArray *provinceArray;
@property(nonatomic,retain)NSMutableArray *cityArray;

@property(nonatomic,retain)NSString *provinceStr;
@property(nonatomic,retain)NSString *cityStr;

@end

@implementation kata_AmendInformationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)dealloc
{
    
    [_picker release];
    
    [_provinceStr release];
    
    [_cityStr release];
    
    [_provinceArray release];
    
    [_cityArray release];
    
    [_postDic release];
    
    [_imageView release];
    
    [_imageView2 release];
    
    [_imagePicker1 release];
    
    [_imagePicker2 release];
    
    [_tableView release];
    
    [_offerTypeStr release];
    
    [_informintionArray release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"资料修改";
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

    
    
    
   _informintionArray = [[NSArray alloc]initWithObjects:
                 @"公司类型", @"公司名称", @"公司简称",@"公司电话",@"实际操盘人",@"短信条数",@"QQ",@"经营范围",@"MSN",@"地址",@"详细地址",@"公司网址",@"注意：",@"上传营业执照",@"上传名片的照片",nil];
    

    _cityArray = [[NSMutableArray alloc]init];
    
    _provinceArray = [[NSMutableArray alloc]init];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 44) style:UITableViewStyleGrouped];
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    _tableView.backgroundView = nil;
    
    [self.view addSubview:_tableView];
    
    
    
    _provinceStr =  @"请选择省份";
    
    _cityStr =  @"请选择城市";

    


}
- (void)backClick
{

    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark - tableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 100.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *vi = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)] autorelease];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.frame = CGRectMake(80, 44, 180, 40);
    
    [button addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button setTitle:@"提交修改" forState:UIControlStateNormal];
    
    
    [vi  addSubview:button];
    
    
    
    return vi;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 12 ) {
        
        NSString *contentStr = @"注意：需要认证的用户请上传营业执照和名片，通过认证后可以在您所发布的报盘中显示企业认证标志!";
        
        CGSize ts = [contentStr sizeWithFont:[UIFont systemFontOfSize:16.0] constrainedToSize:CGSizeMake(220, CGFLOAT_MAX) lineBreakMode:(NSLineBreakByCharWrapping)];
        
        return ts.height;
    
    }else if (indexPath.row  == 13 || indexPath.row == 14){
        
        
       NSString *contentStr  = @"证件必须是清晰彩色原件电子版，可以是扫描件或者数码拍摄照片";
        
        CGSize  ts = [contentStr sizeWithFont:[UIFont systemFontOfSize:16.0] constrainedToSize:CGSizeMake(300, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        
        return ts.height + 200 + 44;
        
    }
    else {
        
        return 44;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_informintionArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int sec = indexPath.section;
    
    int row = indexPath.row;
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d,%d",sec,row];
        
    if (row == 3) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            [self cellNameLabelWithView:cell.contentView];
            
            UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(80, 7, 80, 30)];
            
            textField.borderStyle = UITextBorderStyleLine;
            
            textField.backgroundColor  = [UIColor clearColor];
            
//            textField.backgroundColor = [UIColor blueColor];
            
            textField.delegate = self;
            
            textField.tag = 2;
            
            textField.placeholder = @"区号";
            
            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            [cell.contentView addSubview:textField];
            
            [textField release];
            
            UITextField *textField2 = [[UITextField alloc]initWithFrame:CGRectMake(170, 7, 130, 30)];
            
//          textField2.backgroundColor = [UIColor blueColor];
            
            textField2.backgroundColor  = [UIColor clearColor];

            textField2.borderStyle = UITextBorderStyleLine;

            textField2.placeholder = @"号码";

            textField2.delegate = self;
            
            textField2.tag = 3;
            
            textField2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            [cell.contentView addSubview:textField2];
            
            [textField2 release];


            
        }
        
        UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];
        
        nameLbl.text =[_informintionArray objectAtIndex:indexPath.row];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }else if (row == 7){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
            [self cellNameLabelWithView:cell.contentView];
            
            
            UILabel *showLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 0, 220, 44)];
            
            showLabel.tag = 2;
            
            [showLabel setBackgroundColor:[UIColor clearColor]];
            
        
            [cell.contentView addSubview:showLabel];
            
        }
        
        
        UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];
        
        nameLbl.text =[_informintionArray objectAtIndex:indexPath.row];

    
        UILabel *showLabel = (UILabel *)[cell.contentView viewWithTag:2];
        
        showLabel.text = self.offerTypeStr;

        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }else if (row == 9){
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            
            [self cellNameLabelWithView:cell.contentView];
            


            UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button1.frame = CGRectMake(80, 0, 120, 44);
            
            [button1 addTarget:self action:@selector(selectProvince:) forControlEvents:UIControlEventTouchUpInside];
            
            button1.tag  = 2;
            
            [button1 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            
            [button1 setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView  addSubview:button1];
            
            UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button2.frame = CGRectMake(200, 0, 120, 44);
            
            [button2 addTarget:self action:@selector(selectCity:) forControlEvents:UIControlEventTouchUpInside];
            
            button2.tag  = 3;

            [button2 setBackgroundColor:[UIColor clearColor]];

            [button2 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            
            [cell.contentView  addSubview:button2];

            
            
        }
        
        UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];
        
        nameLbl.text =[_informintionArray objectAtIndex:indexPath.row];
        
        UIButton *button1  = (UIButton *)[cell.contentView viewWithTag:2];
        
        [button1 setTitle:_provinceStr forState:UIControlStateNormal];

        UIButton *button2  = (UIButton *)[cell.contentView viewWithTag:3];
        
        [button2 setTitle:_cityStr forState:UIControlStateNormal];


        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }else if (row == 12){ //注意
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            
//            [self cellNameLabelWithView:cell.contentView];
            
            UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 44)];
            
            lab.backgroundColor = [UIColor clearColor];
            
            lab.textAlignment = NSTextAlignmentLeft;
            
//            [lab setAdjustsFontSizeToFitWidth:YES];
            
            lab.numberOfLines = 0;
            
            lab.tag = 2 ;
            
            [cell.contentView addSubview:lab];
            
            [lab release];

            
        }
        
        
        UILabel *contentLabel = (UILabel *)[cell.contentView viewWithTag:2];
        
        
        contentLabel.text = @"注意：需要认证的用户请上传营业执照和名片，通过认证后可以在您所发布的报盘中显示企业认证标志!";
        
          CGSize ts = [contentLabel.text sizeWithFont:[UIFont systemFontOfSize:16.0f] constrainedToSize:CGSizeMake(220, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        
        contentLabel.frame = CGRectMake(80, 0, 220, ts.height);
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return cell;
        

    }else if (row == 13 || row == 14){ //注意
        
        _cellImageViewRow = row;
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            
            [self cellNameLabelWithView:cell.contentView];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(80, 0, 220, 44);
            
            [button setTitle:@"点击选择" forState:UIControlStateNormal];
            
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            button.tag  =  row;
            
            [button addTarget:self action:@selector(selectImage:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:button];
            
            UILabel *lab  = [[UILabel alloc]init];
            
            lab.text = @"证件必须是清晰彩色原件电子版，可以是扫描件或者数码拍摄照片";
            
            CGSize  ts = [lab.text sizeWithFont:[UIFont systemFontOfSize:16.0] constrainedToSize:CGSizeMake(300, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
            
            lab.frame =  CGRectMake(0, 44, 300, ts.height);
            
            lab.backgroundColor = [UIColor clearColor];
            
            lab.numberOfLines = 0;
            
            lab.tag = 3;
            
            
            
            [cell.contentView addSubview:lab];
            
            [lab release];
            
            if (_cellImageViewRow == 13) {
                
                _imageView  = [[UIImageView alloc]initWithFrame:CGRectMake(10, 44 + ts.height + 10, 280, 200)];
                
                
                _imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                [cell.contentView addSubview:_imageView];
            }
            
            if (_cellImageViewRow == 14) {
                
                _imageView2  = [[UIImageView alloc]initWithFrame:CGRectMake(10, 44 + ts.height + 10, 280, 200)];
                
                
                
                _imageView2.contentMode = UIViewContentModeScaleAspectFit;
                
                [cell.contentView addSubview:_imageView2];
            }
            
         
            
            
        }
        UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];

        nameLbl.text =[_informintionArray objectAtIndex:indexPath.row];
        
        
//        UILabel *showLabel = (UILabel *)[cell.contentView viewWithTag:3];
        
        
//        _imageView = (UIImageView *)[cell.contentView viewWithTag:row];
        
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else{
        
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            
            [self cellNameLabelWithView:cell.contentView];
            
            UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(80, 7, 220, 30)];
            
            textField.borderStyle = UITextBorderStyleLine;

            textField.backgroundColor = [UIColor clearColor];
            
            textField.delegate = self;
            
            textField.tag = 2;
            
            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            [cell.contentView addSubview:textField];
            
            [textField release];
            
        }
        
        
        
        UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];
        
        nameLbl.text =[_informintionArray objectAtIndex:indexPath.row];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return cell;

        
    }
    
}


#pragma mark - 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int row = indexPath.row;
    
    if (row == 7) {
        
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"经营范围" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"MEG",@"PTA",@"MEG和PTA", nil];
        
        
        [actionSheet showInView:self.view];
        
        
        [actionSheet release];
        
    }
    
}

#pragma mark - 

- (void)provinceRequestFinish:(ASIFormDataRequest *)request
{
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    DebugLog(@"dic = %@",dic);
    
    if ([[dic objectForKey:@"code"] isEqualToString:@"0"]) {

        NSArray *array = [[dic objectForKey:@"data"] objectForKey:@"areas"];
        
        for (int i = 0; i < [array count]; i ++) {
            
            
            kata_addressDao *addressDao = [[kata_addressDao alloc]init];
            
            NSDictionary  *dic = [array objectAtIndex:i];
            
            addressDao.provinceName = [dic objectForKey:@"name"];
            
            addressDao.provinceId = [dic objectForKey:@"id"];
            
            [self.provinceArray addObject:addressDao];
            
            
        }
        
        
    }
    
//    [self showPickerViewWithTag:1];
}
- (void)provinceRequestFail:(ASIHTTPRequest *)request
{
    
    
}
- (void)cityRequestFinish:(ASIHTTPRequest *)request
{
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    DebugLog(@"dic ==  %@",dic);
    
    if ([[dic objectForKey:@"code"] isEqualToString:@"0"]) {
        
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        if (data != [NSNull null]) {
            
            NSArray *array = [[dic objectForKey:@"data"] objectForKey:@"areas"];
            
            for (int i = 0; i < [array count]; i ++) {
                
                
                kata_addressDao *addressDao = [[kata_addressDao alloc]init];
                
                
                
                NSDictionary  *dic = [array objectAtIndex:i];
                
                
                addressDao.cityName = [dic objectForKey:@"name"];
                
                addressDao.cityId = [dic objectForKey:@"id"];
                
                
                
                [self.cityArray addObject:addressDao];
                
            }
            

            
        }
        
       
    }
    
//    [self showPickerViewWithTag:2];

    
}
- (void)cityRequestFail:(ASIHTTPRequest *)request
{
    
    
}


#pragma mark - sign

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"provinces_list_get";
}

- (NSDictionary *)params
{
    
    
    _postDic = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                 nil] autorelease];
    
    return _postDic;
    
}
- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark - buttonEvent

- (void)submit:(id)sender
{
    
    
}

- (void)selectProvince:(id)sender
{
    
    
    
    

    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:SERVER_URI]];
    
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];

    [request setDidFinishSelector:@selector(provinceRequestFinish:)];
    
    [request setDidFailSelector:@selector(provinceRequestFail:)];
    
    request.timeOutSeconds = 20;
    
    request.delegate = self;
    
    [request startSynchronous];
    
    
    
    [self showPickerViewWithTag:1];

}

- (void)selectCity:(id)sender
{
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:SERVER_URI]];
    
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:CITY_LIST forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    [request setDidFinishSelector:@selector(cityRequestFinish:)];
    
    [request setDidFailSelector:@selector(cityRequestFail:)];
    
    request.timeOutSeconds = 20;
    
    request.delegate = self;
    
    [request startSynchronous];

    [self showPickerViewWithTag:2];
}

- (void)selectImage:(id)sender
{
    
    UIButton *butt = (UIButton *)sender;
    
    
    UIImagePickerController *  imagePicker = [[UIImagePickerController alloc]init];
    
    imagePicker.delegate = self;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    imagePicker.allowsImageEditing = YES;    //图片可以编辑
    //需要添加委托
    [self presentModalViewController:imagePicker animated:YES];
    
    
    
}

#pragma mark - pickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
   
    if (pickerView.tag == 1) {
        
       return [self.provinceArray count];
        
    }else{
        
        return [self.cityArray count];
        
    }

}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView.tag == 1) {
        
        
        kata_addressDao *addressDao  = [self.provinceArray objectAtIndex:row];

        return addressDao.provinceName;

    }else{

        kata_addressDao *addressDao  = [self.cityArray objectAtIndex:row];
        
        return addressDao.cityName;
        
    }

}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView.tag == 1) {
        
        kata_addressDao *addressDao = [self.provinceArray objectAtIndex:row];
        
        self.provinceStr = addressDao.provinceName;
        
        
        [_tableView reloadData];
        
        _picker.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 216);
        
        
    }else{
        
        kata_addressDao *addressDao = [self.provinceArray objectAtIndex:row];
        
        self.cityStr = addressDao.cityName;
        
        
        [_tableView reloadData];
        
        _picker.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 216);

        
    }
    
    
    
}

#pragma mark - textFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark  - actionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
        {
            self.offerTypeStr = @"MEG";
        }
            break;
        case 1:
        {
            self.offerTypeStr = @"PTA";
        }
            break;
        case 2:
        {
            self.offerTypeStr = @"MEG和PTA";
        }
            break;
            
        default:
            break;
    }
    
    
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:7 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - commonality


- (UILabel *)cellNameLabelWithView:(UIView *)aView
{
    
    
    UILabel *lab = [[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)] autorelease];
    
//    lab.backgroundColor = [UIColor redColor];
    
    lab.backgroundColor = [UIColor clearColor];
    
    [lab setAdjustsFontSizeToFitWidth:YES];
    
    
    lab.tag = 1 ;
    
    [aView addSubview:lab];
    
    return lab;
    
}

#pragma mark -

- (void)showPickerViewWithTag:(int)aTag
{
    
//    UIPickerView *picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 216, ScreenWidth, 216)];
    
    _picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, ScreenHeight, ScreenWidth, 216)];

    _picker.tag = aTag;
    
    _picker.showsSelectionIndicator = YES;
    
    _picker.delegate =  self;
    
    _picker.dataSource = self;

    
    [UIView animateWithDuration:3 animations:^(void){

        _picker.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 216);

    }completion:^(BOOL finished){
       
        _picker.frame = CGRectMake(0, ScreenHeight - 216, ScreenWidth, 216);

        
    }];
    
    [self.view addSubview:_picker];
    
    
}

#pragma mark -

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
{
    
    DebugLog(@"image ==  %@",image);
    
    if (_cellImageViewRow == 13) {
    
        [_imageView setImage:image];

        [_tableView reloadData];

    
    }
    
    if (_cellImageViewRow == 14) {
        
        [_imageView2 setImage:image];

        [_tableView reloadData];

        
    }
    
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200, 200)];
//
//    [imageView setImage:image];
//    
//    [_tableView addSubview:imageView];
//    
//
//    if (picker ==_imagePicker1) {
//    
//       
//            
//            [_imageView setImage:image];
//            
//            [_tableView reloadData];
//     
//    }
//    
//    if (picker ==_imagePicker2) {
//        
//        if (_imageView.tag == 14) {
//            
//            [_imageView setImage:image];
//            
//            [_tableView reloadData];
//        }
//        
//    }

    
    
    
        //关闭相册界面
    [picker dismissModalViewControllerAnimated:YES];
    
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [self dismissModalViewControllerAnimated:YES];

}











@end
