//
//  kata_FindPasswordViewController.m
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_FindPasswordViewController.h"



#import "KATAConstants.h"

#import "KATAUITabBarController.h"

@interface kata_FindPasswordViewController ()


@property(nonatomic,retain)NSArray *amendArr;

@end

@implementation kata_FindPasswordViewController

- (void)dealloc
{
    [_amendArr release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    _amendArr = [[NSArray alloc]initWithObjects:
                 @"手机号码", @"免费获的验证码", @"手机验证码", @"新密码", @"确认密码", nil];
    
    
    
    
    self.title = @"找回密码";
    
    self.navigationItem.hidesBackButton = YES;
    
    KATAUITabBarController *tabBarController = (KATAUITabBarController *)self.tabBarController;
    
    [tabBarController hideTabBar];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

    
    
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
    
    tableView.backgroundView = nil;
    
    [self.view addSubview:tableView];
    
    [tableView release];
    
    
}

#pragma mark - buttonEvent
-(void)backClick
{
    [self.navigationController  popViewControllerAnimated:YES];
    
}


#pragma mark - tableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_amendArr count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    
    int sec = indexPath.section;
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"%d%d",sec,row];
    
    if (row == 1) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            
                UIButton *butt = [UIButton buttonWithType:UIButtonTypeCustom];
                
                butt.frame = CGRectMake(0, 0, 300, 44);
                
                [butt setTintColor:[UIColor blackColor]];
                
                [butt setTitle:@"免费获得验证码" forState:(UIControlStateNormal)];
            
                [butt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
                [butt addTarget:self action:@selector(sendVerification:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.contentView addSubview:butt];
                
            
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;

        return cell;

        
    }else{
        
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (!cell) {
            
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            
            UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
            
            lab.backgroundColor = [UIColor clearColor];
            
            [lab setAdjustsFontSizeToFitWidth:YES];
            
            
            lab.tag = 1 ;
            
            [cell.contentView addSubview:lab];
            
            [lab release];
            
            UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(80, 7, 220, 30)];
            
            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            textField.backgroundColor = [UIColor clearColor];
            
            textField.tag = 2;
            
            textField.borderStyle = UITextBorderStyleLine;
            
            [cell.contentView addSubview:textField];
            
            [textField release];
            
            
            
        }
        
        UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];
        
        nameLbl.text =[_amendArr objectAtIndex:indexPath.row];
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        return cell;

    }
    


}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}
#pragma mark - sendVerification

- (void)sendVerification:(id)sender
{
    
    
    
    
    
}




@end
