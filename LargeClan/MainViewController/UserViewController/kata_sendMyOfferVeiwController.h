//
//  kata_sendMyOfferVeiwController.h
//  LargeClan
//
//  Created by kata on 14-1-7.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "kata_Tools.h"

#import "kata_segView.h"

#import "kata_MyOfferMEGInsideView.h"
#import "kata_MyOfferMEGOutView.h"
#import "kata_MyOfferPTAInsideView.h"
#import "kata_MyOfferPTAOutView.h"

#import "kata_PastTimeViewController.h"
#import "kata_PassNumberViewController.h"
#import "kata_AddressViewController.h"
@interface kata_sendMyOfferVeiwController : UIViewController <UITableViewDataSource,UITableViewDelegate,
    kata_ToolsDelegate,
    UITextFieldDelegate,
    kata_segViewDelegate,

    kata_AddressViewControllerDelegate,
    kata_MyOfferMEGInsideViewDelegate,
    kata_MyOfferMEGOutViewDelegate,
    kata_MyOfferPTAInsideViewDelegate,
    kata_MyOfferPTAOutViewDelegate,
    kata_PastTimeViewControllerDelegate,
    kata_PassNumberViewControllerDelegate>

@end
