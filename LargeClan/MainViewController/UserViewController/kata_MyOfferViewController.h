//
//  kata_MyOfferViewController.h
//  LargeClan
//
//  Created by kata on 14-1-14.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "kata_segView.h"
#import "ASIHTTPRequest.h"

#import "kata_LeftTableDelegate.h"
#import "kata_MyOffer0ViewController.h"
@interface kata_MyOfferViewController : UIViewController <kata_segViewDelegate,UITableViewDataSource,UITableViewDelegate,
    kata_MyOffer0ViewControllerDelegate,
    UIActionSheetDelegate>

@end
