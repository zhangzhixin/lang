//
//  kata_MyOffer0ViewController.h
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "kata_segView.h"
#import "ASIHTTPRequest.h"

#import "kata_LeftTableDelegate.h"

@protocol kata_MyOffer0ViewControllerDelegate <NSObject>

- (void)kata_MyOffer0ViewControllerPushSendMyOffer;

- (void)kata_MyOffer0ViewControllerPushAmendMyOffer;


@end

@interface kata_MyOffer0ViewController : UIViewController <kata_segViewDelegate,UITableViewDataSource,UITableViewDelegate,
    UITextFieldDelegate,UIActionSheetDelegate,
    UIAlertViewDelegate>

{
    id <kata_MyOffer0ViewControllerDelegate> delegate;
    
}
@property(nonatomic,retain)    id <kata_MyOffer0ViewControllerDelegate> delegate;

@end
