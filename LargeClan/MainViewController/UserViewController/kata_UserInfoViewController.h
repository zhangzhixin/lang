//
//  kata_UserInfoViewController.h
//  LargeClan
//
//  Created by kata on 13-12-23.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "kata_segView.h"
#import "ASIHTTPRequest.h"

@interface kata_UserInfoViewController : UIViewController <kata_segViewDelegate,ASIHTTPRequestDelegate>

- (id)initWithUserId:(NSString *)aUserId;


@end
