//
//  kata_AmendPasswordViewController.m
//  LargeClan
//
//  Created by kata on 14-1-7.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_AmendPasswordViewController.h"
#import "KATAConstants.h"
#import "ASIFormDataRequest.h"

#import "MBProgressHUD.h"
#import "NSString+MD5.h"

#import "KATAUtils.h"
#import "kata_UserViewController.h"

#import "kata_Tools.h"

@interface kata_AmendPasswordViewController ()


@property(nonatomic,retain)NSArray *amendArr;

@property(nonatomic,retain)NSMutableDictionary *postDic;

@property(nonatomic,retain)NSString *password;
@property(nonatomic,retain)NSString *password1;
@property(nonatomic,retain)NSString *password2;

@end

@implementation kata_AmendPasswordViewController

- (void)dealloc
{
    [_postDic release];
    
    [_password  release];
    
    [_password1 release];
    
    [_password2 release];
        
    [_amendArr release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"密码修改";
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    

    
    _amendArr = [[NSArray alloc]initWithObjects:
                 @"原始密码", @"新密码", @"重复新密码", nil];
    
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
    
    tableView.backgroundView = nil;
    
    [self.view addSubview:tableView];
    
    [tableView release];
    
    
}

#pragma mark - tableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 100.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    UIView *vi = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)] autorelease];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.frame = CGRectMake(80, 44, 180, 40);
    
    [button addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button setTitle:@"提交修改" forState:UIControlStateNormal];
    
    
    [vi  addSubview:button];
    
    
    
    return vi;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
     return [_amendArr count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        

        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        lab.backgroundColor = [UIColor clearColor];
        
        [lab setAdjustsFontSizeToFitWidth:YES];
        
        
        lab.tag = 1 ;
        
        [cell.contentView addSubview:lab];
        
        [lab release];
        
        
        UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(80, 7, 220, 30)];
        
        textField.backgroundColor = [UIColor clearColor];
        
        textField.delegate = self;
        
        textField.tag =  10 + indexPath.row;
        
        textField.borderStyle = UITextBorderStyleLine;
        
        textField.secureTextEntry = YES;
        
        
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        [cell.contentView addSubview:textField];
        
        [textField release];
        
    }

    
    
    UILabel *nameLbl = (UILabel *)[cell.contentView viewWithTag:1];
    
    nameLbl.text =[_amendArr objectAtIndex:indexPath.row];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return cell;
}


#pragma mark - admin
- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"user_changePassword_update";
}

- (NSDictionary *)params
{
    
    
    _postDic = [[NSMutableDictionary alloc]init];
    
    
    int uid = [[NSString stringWithFormat:@"%@",[kata_UserViewController sharedUserViewController].uid] intValue];
    

    [_postDic setObject:[NSNumber numberWithInt:uid] forKey:@"userid"];

    if (_password  && _password1 && _password2) {
        
        [_postDic setObject:[self.password MD5] forKey:@"password"];

        [_postDic setObject:[self.password1 MD5] forKey:@"password1"];
        [_postDic setObject:[self.password2 MD5] forKey:@"password2"];

    }
    
    
    return _postDic;
    
}
- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark - buttonEvent

- (void)submit:(id)sender
{
    
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds  = 20;
    
    request.delegate = self;
    
    [request startAsynchronous];
    
    
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];

//    if ([self.password1 isEqualToString:self.password2]) {
//        
//        
//        [self.navigationController popViewControllerAnimated:YES];
//        
//    }else
//    {
//        [kata_Tools myAlertView:@"温馨提示:" message:@"两次密码不一致" cancelButtonTitle:@"确定" otherButtonTitles:nil];
//    }
    

    
}


- (void)backClick
{

    //    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    //    [tab showTabBar];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - 

- (void)requestFinished:(ASIHTTPRequest *)request
{
    DebugLog(@"request =  %@",[request responseString]);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
    NSDictionary * dic  = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
    if ([[dic objectForKey:@"code"] isEqualToString:@"0"]) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];

      
    }
    
    
    

    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"修改请求失败 requestFailed");

    
}

#pragma mark - textFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    switch (textField.tag) {
        case 10:
        {
            self.password =  textField.text;
        }
            break;
        case 11:
        {
            self.password1 =  textField.text;

        }
            break;

        case 12:
        {
            self.password2 =  textField.text;

        }
            break;

            
        default:
            break;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
