//
//  kata_PassNumberViewController.h
//  LargeClan
//
//  Created by kata on 14-1-12.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface kata_PassNumberViewController : UIViewController
//
//@end


@protocol kata_PassNumberViewControllerDelegate <NSObject>

- (void)kata_PassNumberViewControllerWithSelectPassNumber:(NSString *)aPassNumber selectPassNumberId:(int)aSelectPassNumberId;

@end

@interface kata_PassNumberViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    id <kata_PassNumberViewControllerDelegate> delegate;
    
}
@property(nonatomic,assign)    id <kata_PassNumberViewControllerDelegate> delegate;


@end
