//
//  kata_AmendMyOfferViewController.m
//  LargeClan
//
//  Created by kata on 14-1-20.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_AmendMyOfferViewController.h"

#import "kata_Tools.h"
#import "KATAConstants.h"
#import "KATAUITabBarController.h"







@interface kata_AmendMyOfferViewController ()


@property(nonatomic,retain) NSMutableArray  *dataArray;
@end


@implementation kata_AmendMyOfferViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.title = @"修改报盘";
    
    self.navigationItem.hidesBackButton = YES;
    
    KATAUITabBarController *tabBarController = (KATAUITabBarController *)self.tabBarController;
    
    [tabBarController hideTabBar];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];
    

    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 44) style:UITableViewStylePlain];
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
    
    [self.view addSubview:tableView];
    
   
    _dataArray  = [[NSMutableArray alloc]initWithObjects:
                               @"勿扰标记", @"报盘人信息", @"交易方向", @"人民币价格", @"期货类型", @"交割时间", @"交割地", @"保证金", @"数量", @"发票",@"免仓时间",@"备注",@"过期时间",
                               nil];
    
    
    
    
    
    
}

- (void)backClick
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - tableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 100.0f;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    UIView *vi = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 120)] autorelease];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.frame = CGRectMake(80, 20, 180, 40);
    
    [button addTarget:self action:@selector(submitMyOffer:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button setTitle:@"提交修改" forState:UIControlStateNormal];
    
    [vi  addSubview:button];
    
    return vi;
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_dataArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int sec = indexPath.section;
    
    int row = indexPath.row;
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",sec,row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell) {
        
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];

        
    
        
        
        
        
        
    }
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;

}


- (void)submitMyOffer:(id)sender
{
    
    
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
