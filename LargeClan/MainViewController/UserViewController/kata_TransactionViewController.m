//
//  kata_TransactionViewController.m
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_TransactionViewController.h"
#import "kata_TwoTableView.h"
#import "kata_segView.h"
#import "KATAConstants.h"




#import "kata_Transcation0ViewController.h"
#import "kata_Transcation1ViewController.h"
#import "kata_Transcation2ViewController.h"
#import "kata_Transcation3ViewController.h"


@interface kata_TransactionViewController ()
{
    
    kata_Transcation0ViewController *_trans0VC;
    
    kata_Transcation1ViewController *_trans1VC;
    
    kata_Transcation2ViewController *_trans2VC;
    
    kata_Transcation3ViewController *_trans3VC;

}
@property(nonatomic,assign) int nameofpart;

@end

@implementation kata_TransactionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    
    [_trans0VC release];
    [_trans1VC release];
    [_trans2VC release];
    [_trans3VC release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"成交录入";
    
    self.navigationItem.hidesBackButton = YES;
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

    
  kata_segView *segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, 320, 40) typeIdentifier:100];
    
    segView.delegate = self;
    
    [self.view addSubview:segView];
    
    [segView release];
    
       
    
    _trans0VC = [[kata_Transcation0ViewController alloc]init];
    
    _trans0VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);
    [self.view addSubview:_trans0VC.view];
    
    
    _trans1VC = [[kata_Transcation1ViewController alloc]init];
    
    _trans1VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);
    
    
    _trans2VC = [[kata_Transcation2ViewController alloc]init];
    
    _trans2VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);
    
    _trans3VC = [[kata_Transcation3ViewController alloc]init];
    
    _trans3VC.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight - 44 - 40);
    
    
}
- (void)backClick
{    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - changeIndex

- (void)changeIndex:(int)index
{
    
    //发送不同的请求，等到相应类型的报盘,我的报盘
    
    self.nameofpart = index - 1;
    
    
    switch (self.nameofpart) {
        case 0:
        {
            
            [_trans1VC.view removeFromSuperview];
            [_trans2VC.view removeFromSuperview];
            [_trans3VC.view removeFromSuperview];
            
            [self.view addSubview:_trans0VC.view];
            
        }
            break;
        case 1:
        {
            
            [_trans0VC.view removeFromSuperview];
            [_trans3VC.view removeFromSuperview];
            [_trans2VC.view removeFromSuperview];
            
            [self.view addSubview:_trans1VC.view];
        }
            break;
        case 2:
        {
            [_trans1VC.view removeFromSuperview];
            [_trans0VC.view removeFromSuperview];
            [_trans3VC.view removeFromSuperview];
            
            [self.view addSubview:_trans2VC.view];
            
        }
            break;
        case 3:
        {
            [_trans1VC.view removeFromSuperview];
            [_trans2VC.view removeFromSuperview];
            [_trans0VC.view removeFromSuperview];
            
            [self.view addSubview:_trans3VC.view];
        }
            break;
        default:
            break;
    }
}

#pragma mark  - 


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
