//
//  kata_UserEnterSuccess.h
//  LargeClan
//
//  Created by kata on 14-1-7.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface kata_UserEnterSuccess : UIViewController <UITableViewDataSource,UITableViewDelegate,
                                                    UIAlertViewDelegate>

- (id)initWithUid:(NSString *)aUid;
@end
