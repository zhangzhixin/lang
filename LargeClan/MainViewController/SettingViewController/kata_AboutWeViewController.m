//
//  kata_AboutWeViewController.m
//  LargeClan
//
//  Created by kata on 13-12-8.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_AboutWeViewController.h"
#import "KATAConstants.h"
@interface kata_AboutWeViewController ()

@end

@implementation kata_AboutWeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationItem setHidesBackButton:YES];
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.title  = @"关于我们";
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

//    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
//    
//    lab.numberOfLines = 0;
//    
//    lab.text = @"  环滨-大宗商品交易信息平台是首家通过网络发布大宗商品买盘和卖盘的电子商务交易平台，致力于打造一个全新信息化的交易模式，让交易更专业、优速、可靠、透明和便捷，使买盘和卖盘真正实时对接。\n  用户可直接通过网站平台、手机客户端查询合适匹配的报盘信息，并可通过短信、QQ、电话直接和报盘人联系；与此同时，用户也可以随时发布报盘信息，当意向客户关注到您的报盘时，平台也会通过短信、QQ提醒您，使买卖双方交流更顺畅，信息更对称，同时也使您随时随地掌握商品行情，做出正确决策。\n  平台特别设置了第三方撮合功能，您可以通过此平台委托专业的第三方撮合公司为你匹配信息，完成交易。第三方撮合公司也可以在此平台发布报盘，一次的发布信息，就能让平台的所有客户浏览到您的报盘信息，让信息匹配更快捷，让交易多一种方式。";
//    
//    [self.view addSubview:lab];
    

//    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 44)];
//    
//    textView.text = @"
//    环滨-大宗商品交易信息平台是首家通过网络发布大宗商品买盘和卖盘的电子商务交易平台，致力于打造一个全新信息化的交易模式，让交易更专业、优速、可靠、透明和便捷，使买盘和卖盘真正实时对接。
//    
//    用户可直接通过网站平台、手机客户端查询合适匹配的报盘信息，并可通过短信、QQ、电话直接和报盘人联系；与此同时，用户也可以随时发布报盘信息，当意向客户关注到您的报盘时，平台也会通过短信、QQ提醒您，使买卖双方交流更顺畅，信息更对称，同时也使您随时随地掌握商品行情，做出正确决策。
//    
//    平台特别设置了第三方撮合功能，您可以通过此平台委托专业的第三方撮合公司为你匹配信息，完成交易。第三方撮合公司也可以在此平台发布报盘，一次的发布信息，就能让平台的所有客户浏览到您的报盘信息，让信息匹配更快捷，让交易多一种方式。
//    "
//    
//    [self.view addSubview:textView];
//    
//    [textView release];
//    






}
#pragma mark - backClick

-(void)backClick
{
    
    [self.navigationController  popToRootViewControllerAnimated:YES];
    
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
