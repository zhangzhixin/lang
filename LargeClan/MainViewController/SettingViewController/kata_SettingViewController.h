//
//  kata_SettingViewController.h
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface kata_SettingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@end
