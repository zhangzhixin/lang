//
//  kata_IndexViewController.m
//  LargeClan
//
//  Created by kata on 13-11-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//



#import "kata_IndexViewController.h"

#import "kata_testViewController.h"

#import "KATAUITabBarController.h"

#import "KATAUtils.h"

#import "kata_segView.h"

#import "ASIFormDataRequest.h"

#import "KATAConstants.h"

#import "NSString+MD5.h"

#import "KATAIndex.h"

#import "kata_TwoTableView.h"

#import "MBProgressHUD.h"
@interface kata_IndexViewController ()
{
    UIScrollView *_scrollView;
    
    UITableView *_tableView1;
    UITableView *_tableView2;
    kata_segView *_segView;
    
    int _type;
}

@property(nonatomic,retain)NSMutableArray *dataInfo2;

@property(nonatomic,retain)NSDictionary *postDic;

@property(nonatomic,retain)NSArray *insideIndexArrayMEG; //默认


@property(nonatomic,retain)NSMutableArray *firstRowArrayInTableView2; //默认


@property(nonatomic,retain)NSArray *dataArray;

@property(nonatomic,retain)NSArray *array1;
@property(nonatomic,retain)NSArray *array2;

@property(nonatomic,retain)NSArray *rightNameArray;

@end

@implementation kata_IndexViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        
    }
    return self;
}

-(void)dealloc
{
    [_dataInfo2 release];
    
    [_firstRowArrayInTableView2 release];
    
    _segView.delegate = nil;
    [_segView release];

    
    
    
    
    [super dealloc];
}






-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    
    [self sendRequest];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initViewDidLoadData];
    
}

#pragma mark - initViewDidLoadData

- (void)initViewDidLoadData
{
    
    self.title = @"查询报盘";
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    KATAUITabBarController *tab  = (KATAUITabBarController  *)self.tabBarController;
    [tab showTabBar];
    
    
    //编辑
    UIImage* img1 =[UIImage imageNamed:@"edit.png"];
    UIImage* img2 =[UIImage imageNamed:@"edit_active.png"];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame =CGRectMake(0, 0, 32, 32);
    
    [btn setBackgroundImage:img1 forState:UIControlStateNormal];
    
    [btn setBackgroundImage:img2 forState:UIControlStateHighlighted];
    
    
    [btn addTarget: self action: @selector(editButtonClick) forControlEvents: UIControlEventTouchUpInside];
    
    UIBarButtonItem* item=[[UIBarButtonItem alloc]initWithCustomView:btn];
    
    self.navigationItem.rightBarButtonItem = item;
    
    [item release];

    
    
    //数据
    
    _dataInfo2 = [[NSMutableArray alloc]init];
    
    
    _segView = [[kata_segView alloc]initWithFrame:CGRectMake(0, 0, 320, 40) typeIdentifier:100];
    
    _segView.delegate = self;
    
    [self.view addSubview:_segView];
    
    kata_TwoTableView *twoTableView = [[kata_TwoTableView alloc]initWithFrame:CGRectMake(0, 44, ScreenWidth, ScreenHeight)];
    
    [self.view addSubview:twoTableView];
    
    [twoTableView release];
    
//    [self sendRequest];

    
}
- (void)sendRequest
{
    NSString *urlStr  = [NSString stringWithFormat:@"%@",SERVER_URI];
    
    ASIFormDataRequest    *request  = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    [request setPostValue:[self sign] forKey:@"sign"];
    
    [request setPostValue:[self method] forKey:@"method"];
    
    [request setPostValue:[self appkey] forKey:@"appkey"];
    
    [request setPostValue:[self params] forKey:@"params"];
    
    [request setPostValue:[NSString stringWithFormat:@"%ld",[self time]] forKey:@"time"];
    
    request.timeOutSeconds  = 20;
    
    request.delegate = self;
    
    [request startAsynchronous];
    
    
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}



#pragma mark -uiscrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
//    _tableView1.contentOffset =  scrollView.contentOffset;
//    
//    _tableView2.contentOffset =   _tableView1.contentOffset;
    

}


#pragma mark -custom

-(void)buttonClick:(id)sender
{
    
    kata_testViewController *testVC = [[kata_testViewController alloc]initWithNibName:nil bundle:nil];
    

    testVC.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:testVC animated:YES];
    
    [testVC release];
    
    
}


- (void)changeIndex:(int)index
{
    _type = index - 1;
    
    [self sendRequest];

}
#pragma mark - sign

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return @"index_offer_get";
}

- (NSDictionary *)params
{
    
    
    NSString *nameofpart = [NSString stringWithFormat:@"%d",_type];
    
    
    _postDic = [[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                   nameofpart,@"nameofpart",
                 
                    nil] autorelease];
    
    
    return _postDic;
    
}
- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

#pragma mark - ASIHttpDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    DebugLog(@"request == %@",[request responseString]);
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableLeaves error:nil];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:DEFAULT_INDEXData object:dic];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DEFAULT_INDEXData object:self userInfo:dic];
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    DebugLog(@"requestFailedrequestFailed");
    
    
    
}

#pragma mark - editItem
-(void)editButtonClick
{
    
    DebugLog(@"编辑");
    
}

#pragma mark - dateFromString


- (NSString *)date:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
    
    return auditingDate;
}


- (NSString *)deliveryDate:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
    
    return auditingDate;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
