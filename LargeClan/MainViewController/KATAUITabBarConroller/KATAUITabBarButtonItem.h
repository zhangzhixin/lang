//
//  kata_UITabBarButtonItem.h
//  CityStar
//
//  Created by 黎 斌 on 12-8-3.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KATAUITabBarButtonItem : UIButton
{
	UILabel * tabBarItemTitle;
}
@property (nonatomic, retain) UILabel * tabBarItemTitle;

- (void)setFont:(UIFont *)font;

@end
