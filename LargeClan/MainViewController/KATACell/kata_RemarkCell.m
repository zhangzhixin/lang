//
//  kata_RemarkCell.m
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_RemarkCell.h"

@implementation kata_RemarkCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier tag:(int)aTag
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UILabel *nameLabel = [[UILabel  alloc]initWithFrame:CGRectMake(0,10, 80, 44)];
        
        nameLabel.backgroundColor = [UIColor clearColor];
        
        nameLabel.adjustsFontSizeToFitWidth = YES;
        
        nameLabel.textAlignment = NSTextAlignmentLeft;
        
        nameLabel.tag = 1;
        
        [self.contentView addSubview:nameLabel];
        
        [nameLabel release];
        
      UITextField *showTextField = [[UITextField  alloc]initWithFrame:CGRectMake(90,0 , 210, 44)];
        
        showTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        showTextField.delegate  = self;
        
        showTextField.backgroundColor = [UIColor blueColor];
        
        showTextField.tag  = aTag;
        
        [self.contentView addSubview:showTextField];
        
        [showTextField release];
        

    }
    return self;
}

- (void)dealloc
{
    
    [_remarkStr release];
    
    [_priceStr release];
    
    [super dealloc];
}
#pragma mark -  textFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 11 ) {
        
        self.remarkStr = textField.text;
        
        [self superview].frame = CGRectMake(0, -200, 320, 568);
        
    }
    if (textField.tag == 12 ) {
        
        self.remarkStr = textField.text;
        
        [self superview].frame = CGRectMake(0, -300, 320, 568);
        
    }
    if (textField.tag == 15 ) {
        
        self.remarkStr = textField.text;
        
        [self superview].frame = CGRectMake(0, -300, 320, 568);
        
    }
    


}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 3) {
        
        self.priceStr = textField.text;
    }
    if (textField.tag == 11) {
        
        self.remarkStr = textField.text;
        
        [self superview].frame = CGRectMake(0, 0, 320, 568 - 44 - 40);

    }
    
    if (textField.tag == 12) {
        
        self.remarkStr = textField.text;
        
        [self superview].frame = CGRectMake(0, 0, 320, 568 - 44 - 40);
        
    }

    if (textField.tag == 15) {
        
        self.remarkStr = textField.text;
        
        [self superview].frame = CGRectMake(0, 0, 320, 568 - 44 - 40);
        
    }
    

    

}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
