//
//  kata_SearchCell.m
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_SearchCell.h"

@implementation kata_SearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        UILabel *nameLab  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        nameLab.tag = 11;
        
        nameLab.backgroundColor = [UIColor clearColor];
        
        //        nameLab.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:nameLab];
        
        [nameLab release];
        
        UITextField *textFiled = [[UITextField alloc]initWithFrame:CGRectMake(90, 7, 220, 30)];
        
        textFiled.userInteractionEnabled = NO;
        textFiled.tag = 12;
        
        textFiled.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        //        textFiled.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:textFiled];
        
        [textFiled release];

        
    }
    return self;
}
@end
