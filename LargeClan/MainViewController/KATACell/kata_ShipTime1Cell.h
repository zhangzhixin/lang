//
//  kata_ShipTime1Cell.h
//  LargeClan
//
//  Created by kata on 14-1-13.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface kata_ShipTime1Cell : UITableViewCell
//
//@end


@protocol kata_ShipTime1CellDelegate <NSObject>

- (void)kata_ShipTime1CellMyOfferPickerShow:(int)aTag;

@end

@interface kata_ShipTime1Cell : UITableViewCell
{
    id <kata_ShipTime1CellDelegate> delegate;
    
}
@property(nonatomic,assign)    id <kata_ShipTime1CellDelegate> delegate;

@end
