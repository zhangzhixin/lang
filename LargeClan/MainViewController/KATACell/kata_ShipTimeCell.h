//
//  kata_ShipTimeCell.h
//  LargeClan
//
//  Created by kata on 14-1-13.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface kata_ShipTimeCell : UITableViewCell
//
//@end

@protocol kata_ShipTimeCellDelegate <NSObject>

- (void)kata_ShipTimeCellMyOfferPickerShow:(int)aTag;

@end


@interface kata_ShipTimeCell : UITableViewCell <UITextFieldDelegate>
{
    id <kata_ShipTimeCellDelegate> delegate;
}
@property(nonatomic,assign)    id <kata_ShipTimeCellDelegate> delegate;


@end
