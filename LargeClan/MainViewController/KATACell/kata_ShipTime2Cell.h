//
//  kata_ShipTime2Cell.h
//  LargeClan
//
//  Created by kata on 14-1-13.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol kata_ShipTime2CellDelegate <NSObject>

- (void)kata_ShipTime2CellMyOfferPickerShow:(int)aTag;

@end

@interface kata_ShipTime2Cell : UITableViewCell
{
    id <kata_ShipTime2CellDelegate> delegate;
    
}
@property(nonatomic,assign)    id <kata_ShipTime2CellDelegate> delegate;

@end
