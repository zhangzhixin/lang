//
//  kata_LeftCell.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_LeftCell.h"

@implementation kata_LeftCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        UIImageView *imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 20, 20)];
        
        imageView1.backgroundColor = [UIColor greenColor];
        
        imageView1.tag = 11;
        
        [self addSubview:imageView1];
        
        [imageView1 release];
        
        
        UIImageView *imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 22, 20, 20)];
        
        
        //            imageView2.backgroundColor = [UIColor blueColor];
        
        imageView2.tag = 12;
        
        [self addSubview:imageView2];
        
        [imageView2 release];
        
        
        
        
        
//        UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 44)];
//        
//        timeLab.tag =111;
//        
//        
////     timeLab.backgroundColor = [UIColor blueColor];
//        
//        //            timeLab.adjustsFontSizeToFitWidth = YES;
//        
//        [self addSubview:timeLab];
//        
//        [timeLab release];
        
        UILabel *numberLab = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 44)];
        
        numberLab.tag = 112;
        
        //            [numberLab setFont:[UIFont systemFontOfSize:12.0f]];
        
        //            numberLab.backgroundColor = [UIColor greenColor];
        
        //            numberLab.adjustsFontSizeToFitWidth = YES;
        
        [self addSubview:numberLab];
        
        [numberLab release];

        
        
    }
    return self;
    
}

@end
