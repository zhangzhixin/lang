//
//  kata_RegSecondCell.m
//  LargeClan
//
//  Created by kata on 13-12-12.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_RegSecondCell.h"
#import "KATAUtils.h"

@interface kata_RegSecondCell ()
{
    UITextField *_inputText;
}
@end


@implementation kata_RegSecondCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 7, 90, 30)];
        
        
        nameLabel.tag = 1;
        
        nameLabel.backgroundColor = [UIColor clearColor];
        
        //        nameLabel.backgroundColor = [UIColor blueColor];
        
        [self addSubview:nameLabel];
        
        [nameLabel release];
        
        
        _inputText = [[UITextField alloc]initWithFrame:CGRectMake(100, 7, 200, 30)];
        
        //        inputText.backgroundColor = [UIColor greenColor];
        
        _inputText.delegate = self;
        
        _inputText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        _inputText.tag = 2;
        
        [self addSubview:_inputText];
        
        [_inputText release];

    
    
    
    }
    return self;
}
#pragma mark -windowDown
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch=[[event allTouches] anyObject];
    
    if (touch.tapCount >=1) {
        
        [_inputText resignFirstResponder];
        
    }
    
}

#pragma mark - textFiledDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [_inputText resignFirstResponder];
    
    return YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
