//
//  kata_DeliveryTimeCell.h
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol kata_DeliveryTimeCellDelegate <NSObject>

- (void)pickerShow:(int)tag;


- (void)kata_DeliveryTimeCellMyOfferPickerShow:(int)aTag;


@end

@interface kata_DeliveryTimeCell : UITableViewCell <UITextFieldDelegate>
{
    
    id <kata_DeliveryTimeCellDelegate> delegate;
}
@property(nonatomic,assign)    id <kata_DeliveryTimeCellDelegate> delegate;


@end
