//
//  kata_RemarkCell.h
//  LargeClan
//
//  Created by kata on 14-1-9.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface kata_RemarkCell : UITableViewCell <UITextFieldDelegate>

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier tag:(int)aTag;



@property(nonatomic,copy)NSString *priceStr;
@property(nonatomic,copy)NSString *remarkStr;

@end
