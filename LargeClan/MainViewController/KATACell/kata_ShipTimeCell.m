//
//  kata_ShipTimeCell.m
//  LargeClan
//
//  Created by kata on 14-1-13.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_ShipTimeCell.h"

//@implementation kata_ShipTimeCell
//
//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}
//
//@end



#import "KATAUtils.h"

@interface kata_ShipTimeCell ()

{
    BOOL isPicker;
    
}

@property(nonatomic,retain)UILabel *nameLbl1;


@property(nonatomic,retain)UILabel *nameLbl2;

//@property(nonatomic,retain)UITextField *timeTxt1;
//
//@property(nonatomic,retain)UITextField *timeTxt2;
//

@property(nonatomic,retain)UIButton *timeBtn1;
@property(nonatomic,retain)UIButton *timeBtn2;

@property(nonatomic,retain)UIDatePicker *picker;


@end


@implementation kata_ShipTimeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        
        _nameLbl1 = [[UILabel alloc]initWithFrame:CGRectZero];
        
        //        _nameLbl1.text = @"交割时间";
        
        _nameLbl1.backgroundColor = [UIColor clearColor];
        
        _nameLbl1.tag = 11;
        
        [self addSubview:_nameLbl1];
        
        
        
        _nameLbl2 = [[UILabel alloc]initWithFrame:CGRectZero];
        
        _nameLbl2.text = @"至";
        
        _nameLbl2.backgroundColor = [UIColor clearColor];
        
        _nameLbl2.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_nameLbl2];
        
        
        
        _timeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _timeBtn1.backgroundColor = [UIColor redColor];
        
        _timeBtn1.tag = 1;
        
        [_timeBtn1 addTarget:self action:@selector(selectTime1:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_timeBtn1];
        
        _timeBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _timeBtn2.backgroundColor = [UIColor blueColor];
        
        _timeBtn2.tag = 2;
        
        [_timeBtn2 addTarget:self action:@selector(selectTime2:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_timeBtn2];
        
        
    }
    return self;
}

- (void)dealloc
{
    
    [_nameLbl1 release];
    
    
    [_nameLbl2 release];
    
    
    
    [super dealloc];
}

- (void)layoutSubviews
{
    
    _nameLbl1.frame = CGRectMake(10, 0, 80, 44);
    
    
    
    _timeBtn1.frame = CGRectMake(10 + _nameLbl1.frame.origin.x  +_nameLbl1.frame.size.width, 0, 90, 44);
    
    
    
    _nameLbl2.frame = CGRectMake( _timeBtn1.frame.origin.x + _timeBtn1.frame.size.width - 10, 0, 40, 44);
    
    
    _timeBtn2.frame = CGRectMake(_nameLbl2.frame.origin.x +_nameLbl2.frame.size.width, 0, 90, 44);
    
    
    
}

#pragma mark - textFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoBoardHidden:) name:UIKeyboardWillShowNotification object:nil];
    
}


#pragma mark - buttonClick


- (void)selectTime1:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(kata_ShipTimeCellMyOfferPickerShow:)]) {
        
        
        [self.delegate kata_ShipTimeCellMyOfferPickerShow:3];
        
    }

    
}

- (void)selectTime2:(id)sender
{
    
    if ([self.delegate respondsToSelector:@selector(kata_ShipTimeCellMyOfferPickerShow:)]) {
        
        [self.delegate kata_ShipTimeCellMyOfferPickerShow:4];
        
    }

    
}




@end
