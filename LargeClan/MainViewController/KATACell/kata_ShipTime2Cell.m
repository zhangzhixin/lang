//
//  kata_ShipTime2Cell.m
//  LargeClan
//
//  Created by kata on 14-1-13.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_ShipTime2Cell.h"

@implementation kata_ShipTime2Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
        
        nameLabel.text = @"预计到港时间";
        
        nameLabel.backgroundColor = [UIColor clearColor];
        
        nameLabel.tag = 1;
        
        [self.contentView addSubview:nameLabel];
        
        [nameLabel release];
        
        
        UIButton  *timeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        timeBtn.frame = CGRectMake(80, 0, 220, 44);

        timeBtn.backgroundColor = [UIColor blueColor];
        
                timeBtn.tag = 2;
        
        [timeBtn addTarget:self action:@selector(selectTime:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:timeBtn];

    }
    return self;
}
#pragma mark - buttonClick

- (void)selectTime:(id)sender
{
    
    if ([self.delegate respondsToSelector:@selector(kata_ShipTime2CellMyOfferPickerShow:)]) {
        
        [self.delegate kata_ShipTime2CellMyOfferPickerShow:2];
        
    }
    
    

    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
