//
//  Constants.h
//  jiangbei
//
//  Created by mac on 13-9-6.
//  Copyright (c) 2013年 mac. All rights reserved.
//

#ifndef jiangbei_Constants_h
#define jiangbei_Constants_h
#import "AppDelegate.h"

#define gXferMgr    ([XRxferManager sharedManager])
// Notificatios
// 网络断开通知
#define kNetworkDisconnectedNotification        @"kNetworkDisconnectedNotification"

#define kNetworkReconnectedNotification         @"kNetworkReconnectedNotification"



#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define creatTravelTime     @"CreatTravelTime"
#define sureTravelTime     @"SureTravelTime"

#define activity ([[[xrMyActivity alloc] init] autorelease])

#define appdelegate ((AppDelegate*)[[UIApplication sharedApplication]delegate])
//URL
#define searchTicketURL     @"http://wap.qunar.com/"
#define trainURL            @"http://t.qunar.com/index.jsp?bd_source=&v=2"
#define gongjiaocheURL      @"http://wap.busditu.com"
#define imgBaseURL          @"http://221.131.216.18:8001/zhihui_nb/uploadfiles/"

//服务器请求地址
#define SearchURL           @"http://221.131.216.18:8001/zhihui_nb/inter_Search_keyword.html"



//#define SearchURL           @"http://218.108.76.2:8080/zhihui_nb/inter_Search_keyword.html"
#define searchResultURL     @"http://221.131.216.18:8001/zhihui_nb/inter_Search_keyword_detail.html"
#define commentURL          @"http://221.131.216.18:8001/zhihui_nb/inter_UserComment_submit_UserComment.html"
#define commentListURL      @"http://221.131.216.18:8001/zhihui_nb/inter_UserComment_scenic_UsercommentList.html"
#define LoginURL            @"http://221.131.216.18:8001/zhihui_nb/inter_Login_login.html"
#define registerURL         @"http://221.131.216.18:8001/zhihui_nb/inter_Register_register.html"
#define myCommentURL        @"http://221.131.216.18:8001/zhihui_nb/inter_MyApply_myComments.html"
#define myStoreURL          @"http://221.131.216.18:8001/zhihui_nb/inter_MyApply_myCollection.html"
#define myYZURL             @"http://221.131.216.18:8001/zhihui_nb/inter_MyApply_mySeal.html"
#define sendMyOpinionURL    @"http://221.131.216.18:8001/zhihui_nb/inter_Feedback_feedBack.html"
#define getYZURL            @"http://221.131.216.18:8001/zhihui_nb/inter_ScenicSearch_scenicSealCollection.html"
#define searchYZURL         @"http://221.131.216.18:8001/zhihui_nb/inter_Seal_searchSeal.html"

#define allYZURL            @"http://221.131.216.18:8001/zhihui_nb/inter_Seal_sealList.html"


#define GetShiPinURL        @"http://221.131.216.18:8001/zhihui_nb/inter_Video_video.html"

#define JiangBeiGeneralURL  @"http://221.131.216.18:8001/zhihui_nb/inter_Introduce_jbIntroduce.html"
#define lyZxURL             @"http://221.131.216.18:8001/zhihui_nb/inter_Consult_travel_consult.html"

#define netMessageURL       @"http://221.131.216.18:8001/zhihui_nb/inter_Consult_travel_detail.html"



#define factMessageURL      @"http://221.131.216.18:8001/zhihui_nb/inter_Shiyong_qitaShiyongMessage.html"
//交通简介
#define HKURL               @"http://221.131.216.18:8001/zhihui_nb/inter_Flay_flay_Introduce.html"
//售票点
#define sellTicketURL       @"http://221.131.216.18:8001/zhihui_nb/inter_Ticket_ticket.html"
//企业活动
#define qyActivityURL       @"http://221.131.216.18:8001/zhihui_nb/inter_Enterprise_enterpriseList.html"

//导游
#define  guideURL           @"http://221.131.216.18:8001/zhihui_nb/inter_TourGuide_tourGuide.html"
//行程
#define XCTypeURL           @"http://221.131.216.18:8001/zhihui_nb/inter_TripPlanning_chooseList.html"
#define makeSureXCURL       @"http://221.131.216.18:8001/zhihui_nb/inter_TripPlanning_chooseTure.html"
#define XCguihuaURL         @"http://221.131.216.18:8001/zhihui_nb/inter_TripPlanning_showPlanning.html"
#define cancleXCURL         @"http://221.131.216.18:8001/zhihui_nb/inter_TripPlanning_cancelTrip.html"
#define keepXCURL           @"http://221.131.216.18:8001/zhihui_nb/inter_TripPlanning_tripSetTimeOk.html"
#define GuiHuaTimeURL       @"http://221.131.216.18:8001/zhihui_nb/inter_TripPlanning_showTripOkList.html"

#define weatherURL          @"http://webservice.webxml.com.cn/WebServices/WeatherWS.asmx/getWeather"
//收藏
#define shouCangURL         @"http://221.131.216.18:8001/zhihui_nb/inter_ScenicSearch_scenicCollection.html"
//#define JingDianListURL     @"http://221.131.216.18:8001/zhihui_nb/inter_ScenicSearch_scenicSearch.html?IOS=IOS&"
#define JingDianListURL     @"http://221.131.216.18:8001/zhihui_nb/inter_ScenicSearch_scenicSearch.html"
#define gongLueURL          @"http://221.131.216.18:8001/zhihui_nb/inter_TravelStrategy_travelList.html"
#define glDetaileURL        @"http://221.131.216.18:8001/zhihui_nb/inter_TravelStrategy_travelDetail.html"



//请求命令

#define kCommandShouCang                @"shoucang"
#define kCommandType                    @"kCommandType"
#define kCommandSearchByKey             @"searchByKey"
#define kCommandDownImg                 @"downImg"
#define kCommandYinZhangImg             @"downYinZhangImg"
#define kCommandDownUserCenterYZ        @"downUserCenterYZ"    

#define kCommandRegisterVC              @"registerVC"
#define kCommandComment                 @"comment"
#define kCommandCommentList             @"commentList"
#define kCommandSearchResultDetaile     @"searchResultDetaile"     
#define kCommandLogin                   @"login"
#define kCommandMyComment               @"myComment"
#define kCommandMyStore                 @"myStore"
#define kCommandSendMyOpinion           @"sendMyOpinion"
#define kCommandGetYingZhang            @"getYinZhang"
#define kCommandMyYinZhang              @"myYinZhang"

#define kCommandGetShiPinURL            @"getShiPinURL"

#define kCommandSearchYinZhang          @"searchYinZhang"

#define kCommandGetAllYZ                @"getAllYZ"
#define kCommandDownAllImg              @"downAllImg"
//
#define kCommandJBGeneral               @"jbGeneral"
#define kCommandLYZiXun                 @"lyZiXun"    
#define kCommandWeiBoZiXun              @"WeiBoZiXun"
#define kCommandDownWeiBoImg            @"downWeiBoImg"
#define kCommandNetMessage              @"netMessage"
#define kCommandFactMessage             @"factMessage"
#define kCommandTrafficDetaile          @"trafficDetaile"
#define kCommandSellTicketPlace         @"sellTicketPlace"
#define kCommandQYActivity              @"QYActivity"
//导游
#define kCommandGuideInfo               @"guideInfo"
//行程
#define kCommandXCType                  @"xingchengType"
#define kCommandXCInfoSure              @"xingchengsure"
#define kCommandXCSelectVC              @"xingchengselect"
#define kCommandXCGuiHua                @"xingchengguihua"
#define kCommandCreatTravel             @"creatTravel"
#define kCommandCancleXC                @"cancleXC"
#define kCommandKeepXingCheng           @"keepXingCheng"
#define kCommandGuiHuaTime              @"guihuaTime"
#define kCommandJingDianList            @"jingdianList"
#define kCommandWeather                 @"weather"
#define kCommandDownYuYin               @"downYuYin"
#define kCommandGongLue                 @"gonglue"
#define kCommandGLDetaile               @"gonglueDetaile"

//代理
#define kXferLoginVC                    @"loginVC"
#define kXferSearchVC                   @"searchVC"
#define kXferSearchResultVC             @"searchResultVC"
#define kXferScenicVC                   @"scenicVC"
#define kXferCommentVC                  @"commentVC"

#define kXferRegister                   @"register"
#define kXferUserCenter                 @"userCenter"
#define kXferMyOpinion                  @"myOpinion"
#define kXferYinZhangVC                 @"yinzhangVC"
#define kXferStampVC                    @"stampVC"
#define kXferGeneralVC                  @"generalVC"

#define kXferTravelVC                   @"travelVC"
#define kXferNetMessageVC               @"netMessageVC"
#define kXferFactMessageVC              @"factMessageVC"
#define kXferTrafficVC                  @"trafficVC"
#define kXferQYActivityVC               @"qyActivityVC"
#define kXferSecondVC                   @"secondVC"
#define kXferThirdVC                    @"thirdVC"
#define kXferSelectVC                   @"selectVC"
#define kXferCreatTravelVC              @"creatTravelVC"
#define kXferNongJiaLeVC                @"nongjialeVC"
#define kXferFoodVC                     @"foodVC"
#define kXferZhuSuVC                    @"zhusuVC"
//#define kXferSearchResultVC             @"searchResultVC"
#define kXferYuLeVC                     @"yuleVC"
#define kXferBuyVC                      @"buyVC"
#define kXferJingDianFirstVC            @"jingDianFirstVC"

#define kXferNearVC                     @"nearVC"
#define kXferGongLueVC                  @"gonglueVC"


#endif
