//
//  KATADebug.h
//  CityStar
//
//  Created by 黎 斌 on 12-8-20.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KATADebug : NSObject

+ (void)console:(NSString *)format data:(id)data;

+ (void)printClassName:(UIView *)pView;

@end
