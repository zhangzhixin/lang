//
//  NSString+Addtions.h
//  ZYLeibieTest
//
//  Created by wxg on 13-7-11.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//1.类别的第一大作用  该类可以作为扩充的一个工具类
//声明一个类别  不能有继承关系 而且类名是之前已经声明过的
// 在类名后添加一个(),(Addtions)  Addtions是扩充NSString类的一个类的别名
//类别只能扩充方法，不能直接扩充属性

//类别里面可以重写系统原有的方法，但是时候会调用该类别的方法，是不确定的

//2.声明私有方法

//3.分散类的实现，可以把同一个类的方法的声明和实现 写在不同的文件中  例如 A类中如果有和B类密切相关的方法 有时候会把A的那些方法写到B类的文件中


@interface NSString (Addtions) 

-(NSString *)trim;

-(BOOL)isContainFromString:(NSString *)aString;

+(NSString *)getFilepath:(NSString *)aPath;

+(NSString *)getFilePathWithDirectory:(NSString *)aDirectory fileName:(NSString *)aFileName;

+ (NSAttributedString *)changeColorFromB:(NSString *)aString color:(UIColor *)aColor;
@end



