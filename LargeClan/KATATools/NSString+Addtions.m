//
//  NSString+Addtions.m
//  ZYLeibieTest
//
//  Created by wxg on 13-7-11.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "NSString+Addtions.h"


@implementation NSString (Addtions)

//+ (NSAttributedString *)changeColorFromB:(NSString *)aString color:(UIColor *)aColor{
//    if (aString == nil) {
//        return nil;
//    }
//    NSArray *array = [aString componentsSeparatedByString:@"<B>"];
//    if (![aString isContainFromString:@"<B>"]) {
//        return [[[NSMutableAttributedString alloc] initWithString:aString] autorelease];
//    }else if (array.count == 1) {
//        NSString *firstStr = [array objectAtIndex:0];
//        NSArray *secondArray = [firstStr componentsSeparatedByString:@"</B>"];
//        NSString *secondStr = [secondArray objectAtIndex:0];
//        NSString *thirdStr = [secondArray objectAtIndex:1];
//        NSMutableAttributedString *first = [[NSMutableAttributedString alloc] initWithString:secondStr];
//        [first addAttribute:NSForegroundColorAttributeName value:aColor range:NSMakeRange(0,firstStr.length)];
//        NSMutableAttributedString *second = [[NSMutableAttributedString alloc] initWithString:thirdStr];
//        [first appendAttributedString:second];
//        return first;
//    }else if(array.count>1){
//        NSString *firstStr = [array objectAtIndex:0];
//        NSString *secondStrTemp = [array objectAtIndex:1];
//        NSArray *secondArray = [secondStrTemp componentsSeparatedByString:@"</B>"];
//        if (secondArray.count > 1) {
//            NSString *secondStr = [secondArray objectAtIndex:0];
//            NSString *thirdStr = [secondArray objectAtIndex:1];
//            NSMutableAttributedString *first = [[NSMutableAttributedString alloc] initWithString:firstStr];
//            NSMutableAttributedString *second = [[NSMutableAttributedString alloc] initWithString:secondStr];
//            [second addAttribute:NSForegroundColorAttributeName value:aColor range:NSMakeRange(0,secondStr.length)];
//            NSMutableAttributedString *third = [[NSMutableAttributedString alloc] initWithString:thirdStr];
//            [first appendAttributedString:second];
//            [first appendAttributedString:third];
//            return first;
//        }else{
//            NSString *secondStr = [secondArray objectAtIndex:0];
//            NSMutableAttributedString *first = [[NSMutableAttributedString alloc] initWithString:firstStr];
//            NSMutableAttributedString *second = [[NSMutableAttributedString alloc] initWithString:secondStr];
//            [second addAttribute:NSForegroundColorAttributeName value:aColor range:NSMakeRange(0,secondStr.length)];
//            [first appendAttributedString:second];
//            return first;
//        }
//    }
//}

//-(BOOL)isEqualToString:(NSString *)aString
//{
//	//NSLog(@"**************");
//	
//	return NO;
//}

-(NSString *)trim
{
	//这个类是NSString的一个类别，那么这个方法中的self代表NSString的当前对象
	
//	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return [self stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];

}

-(BOOL)isContainFromString:(NSString *)aString
{
	
	BOOL flag = [self rangeOfString:aString].location != NSNotFound;
	
	return flag;
}

+(NSString *)getFilepath:(NSString *)aPath
{
	return [NSHomeDirectory() stringByAppendingPathComponent:aPath];	
}

+(NSString *)getFilePathWithDirectory:(NSString *)aDirectory fileName:(NSString *)aFileName
{
	
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:aDirectory];
	NSFileManager *manager = [NSFileManager defaultManager];
	if (![manager fileExistsAtPath:path]) {
		[manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	return [path stringByAppendingPathComponent:aFileName];
	
}

@end


