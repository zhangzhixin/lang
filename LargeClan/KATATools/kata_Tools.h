//
//  kata_Tools.h
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "kata_SendMyOffer.h"

@protocol kata_ToolsDelegate <NSObject>

- (void)kata_ToolsWithSendMyOffer:(kata_SendMyOffer *)akata_SendMyOffer tag:(int)aTag ;

@end


@interface kata_Tools : NSObject <UIActionSheetDelegate>
{
    UIAlertView *_searchvcAlert;
    
    id <kata_ToolsDelegate> delegate;

}

@property (nonatomic,assign) id <kata_ToolsDelegate> delegate;



+(void)myAlertView:(NSString *)title message:(NSString *)messg  cancelButtonTitle:(NSString *)cancelTitle
 otherButtonTitles:(NSString *)otherTitle;


- (id)initWithMyTitle:(NSString *)aTitle
              oneTile:(NSString *)aOneTitle
              twoTile:(NSString *)aTwoTile
            threeTile:(NSString *)aThreeTile
            foureTile:(NSString *)aFoureTile
             fiveTile:(NSString *)aFiveTile
              sixTile:(NSString *)aSixTile
                 view:(UIView *)aView
                  tag:(int)aTag;

+ (NSString *)date:(NSDate *)aDate;

+ (NSString *)deliveryDate:(NSDate *)aDate;
@end
