//
//  kata_GlobalConst.m
//  CityStar
//
//  Created by 黎 斌 on 12-8-3.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import "kata_GlobalConst.h"

@implementation kata_GlobalConst


//#pragma mark - 电话
//NSString * const SELECTED_LOCATION_TELPHONE_KEY	=	@"SELECTED_LOCATION_TELPHONE_KEY";

#pragma makr - 模式
NSString * const MODE = @"DEBUG";//1.DEBUT 2.RELEASE
//NSString * const MODE = @"RELEASE";

//#pragma mark - 系统资源文件名
//NSString * const MAIN_NAV_BG_NAME	=	@"mainNavBg.png";		//Navigation bar background image name
//NSString * const MAIN_TAB_BAR_BG	=	@"mainTabBarBg.png";	// Tab bar background image name
//NSString * const MAIN_TAB_BAR_BG_SELECTED	=	@"mainTabBarBg_selected.png";	// Tab bar selected background image name

//#pragma mark - 默认地理位置
//NSString * const DEFAULT_LOCATION	=	@"杭州";
//NSString * const DEFAULT_LOCATION_ID=	@"572";
//
//#pragma mark - 关键KEY
//NSString * const CLLOCATION_KEY			=	@"CLLOCATION";
//NSString * const SELECTED_LOCATION_KEY	=	@"SELECTED_LOCATION_KEY";
//NSString * const LOGIN_INFO				=	@"LOGIN_INFO";

#pragma mark - 接口地址
NSString * const SERVER_URI	=	@"http://www.icszx.com/mobile/";
//NSString * const SERVER_URI = @"http://t.codeoem.com:8904/mobile/";
//NSString * const SERVER_URI = @"http://122.224.126.23:8082/mobile/";

//NSString * const SERVER_URI = @"http://www.icszx.com/mobile/";
//NSString * const SERVER_URI = @"http://192.168.1.200:8080/citystar/mobile/";


//NSString * const REGIST		=	@"mobileRegist.htm";
//NSString * const LOGIN		=	@"mobileLogin.htm";
//
//NSString * const GET_MOBILE_CITY	=	@"getMobileCity.htm";
////NSString * const GET_MOBILE_CITY	=	@"getMobileProvince";
//
//NSString * const GET_INDEX_INFO		=	@"getIndexInfo.htm";
//
//NSString * const UPLOAD_MOBILE_NEWS	=	@"uploadMobileNew.htm";
//NSString * const COMMENT_MOBILE_NEWS=	@"commentMobileNew.htm";
//NSString * const GET_MOBILE_NEWS	=	@"getMobileNew.htm";
//
//NSString * const GET_MOBILE_CLASS	=	@"getMobileClass.htm";
//NSString * const GET_MOBILE_SEARCH	=	@"getMobileSeach.htm";
//NSString * const GET_MOBILE_SKU_BY_CLASS	=	@"getMobileSkuByClass.htm";
//NSString * const GET_MOBILE_SKU_INFO		=	@"getMobileSkuInfo.htm";
//
//NSString * const GET_MOBILE_CART	=	@"getMobileCart.htm";
//NSString * const UPDATE_MOBILE_CART	=	@"updateMobileCart.htm";
//NSString * const UPDATE_MOBILE_ORDER=	@"updateMobileOrder.htm";
//
//NSString * const GET_MOBILE_ACTIVITYS		=	@"getMobileActivitys.htm";
//NSString * const GET_MOBILE_ACTIVITY_INFO	=	@"getMobileActivityInfo.htm";
//NSString * const ATTEND_MOBILE_ACTIVITY		=	@"attendMobileActivity.htm";
//NSString * const COMMENT_MOBILE_ACTIVITY	=	@"commentMobileActivity.htm";
//
//NSString * const GET_MOBILE_SERVER	=	@"getMobileServer.htm";
//NSString * const GET_MOBILE_SERVER_INFO	=	@"getMobileServerInfo.htm";
//NSString * const REQUEST_MOBILE_SERVER	=	@"requestMobileServer.htm";
//
//NSString * const UPDATE_MOBILE_FEEDBACK	=	@"updateMobileFeedBack.htm";
//NSString * const GET_MOBILE_FAVORITES	=	@"getMobileFavorites.htm";
//NSString * const UPDATE_MOBILE_FAVORITE	=	@"updateMobileFavorite.htm";
//
//NSString * const GET_TOP_CATEGORY = @"getTopCategory.htm";
//
//NSString * const GET_MOBILE_USER_INFO = @"getMobileUserInfo.htm";
//
//NSString * const GET_MOBILE_ORDER_LIST = @"getMobileOrderlist.htm";
//
//NSString * const GET_MOBILE_ORDER_INFO = @"getMobileOrderinfo.htm";
//
//NSString * const GET_MOBILE_ACTIVITY_APPLYS = @"getMobileActivityApplys.htm";
//
//NSString * const GET_MOBILE_ADDRESS = @"getMobileAddress.htm";
//
//NSString * const UPDATE_MOBILE_ADDRESS = @"updateMobileAddress.htm";
//
//NSString * const CANCEL_MOBILE_ORDER = @"calcelMobileOrder.htm";
//
//NSString * const GET_MOBILE_SUB_SERVER = @"getMobileSubServer.htm";
//
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#pragma mark -
//#pragma mark add @2013/03/09
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////NSString * const TEST_SERVER_URI     = @"http://183.129.237.218:8902/mobile/";
////NSString * const TEST_SERVER_URI     = @"http://192.168.1.107:8904/mobile/";
//NSString * const GET_NOTICE_DETAIL   = @"getServiceArticleDetail.htm";
//NSString * const GET_SERVICE_ARTICLE = @"getServiceArticle.htm";
//
//NSString * const GET_SUBPAGE_DATA    = @"getMobileSecond.htm";
//NSString * const GET_NEWS_SUB_CATEGORY_DATA = @"getMobileSecondClass.htm";
//
//NSString * const GET_NEWS_SUB_LIST_DATA = @"mobileNewsList.htm";
//
//NSString * const POST_HELP_INFO = @"uploadMobileHelp.htm";
//
//NSString * const GET_MOBILE_EXPRESS = @"getMobileExpress.htm";
//
//NSString * const DELETE_ADDRESS = @"deleteMobileAddress.htm";
//
//NSString * const GET_MOBILE_SUB_CLASS = @"getMobileSubClass.htm";
//
//#pragma mark - 接口签名
//NSString * const APP_KEY                 =   @"85850948";
//NSString * const APP_SECRET              =   @"743662a02dd68d72f92fde1bdc3ad3ff";
//
//#pragma mark - 简单编码
//NSString *const ENCODE_KEY              =   @"Kata85850948";
@end
