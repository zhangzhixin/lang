//
//  KATAUtils.h
//  CityStar
//
//  Created by 黎 斌 on 12-8-15.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#define rgba(r,g,b,a) [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:a]

#define ScreenHeight [UIScreen mainScreen].bounds.size.height

#define ScreenWidth [UIScreen mainScreen].bounds.size.width

#ifdef DEBUG
#define DebugLog(format, ...) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(format),  ##__VA_ARGS__] )
#else
#define DebugLog(format, ...)
#endif