//
//  kata_Tools.m
//  LargeClan
//
//  Created by kata on 13-11-25.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_Tools.h"

#import "kata_SendMyOffer.h"

#import "kata_testViewController.h"
@interface kata_Tools ()



@property(nonatomic,assign)int tag;

@property(nonatomic,copy)NSString *oneTitle;
@property(nonatomic,copy)NSString *twoTitle;
@property(nonatomic,copy)NSString *threeTitle;
@property(nonatomic,copy)NSString *foureTitle;
@property(nonatomic,copy)NSString *fiveTitle;
@property(nonatomic,copy)NSString *sixTitle;

@end

@implementation kata_Tools

- (void)dealloc
{
    [_oneTitle release];
    
    [_twoTitle release];
    
    [_threeTitle release];
    
    [_foureTitle release];
    
    [_fiveTitle release];
    
    [_sixTitle release];
    
    
    [super dealloc];
}

+(void)myAlertView:(NSString *)title message:(NSString *)messg
                                    cancelButtonTitle:(NSString *)cancelTitle
                                    otherButtonTitles:(NSString *)otherTitle
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:messg delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:otherTitle, nil];
    
    [alert show];
    
    [alert release];
    
    
}


- (id)initWithMyTitle:(NSString *)aTitle
                oneTile:(NSString *)aOneTitle
                twoTile:(NSString *)aTwoTile
                threeTile:(NSString *)aThreeTile
                foureTile:(NSString *)aFoureTile
                fiveTile:(NSString *)aFiveTile
                sixTile:(NSString *)aSixTile
                   view:(UIView *)aView
                  tag:(int)aTag
{
    if (self = [super init]) {
        
        self.tag = aTag;
        
        self.oneTitle = aOneTitle;
        self.twoTitle = aTwoTile;
        self.threeTitle = aThreeTile;
        self.foureTitle = aFoureTile;
        self.fiveTitle = aFiveTile;
        self.sixTitle = aSixTile;
        
        
        UIActionSheet *sheet  = [[UIActionSheet alloc]initWithTitle:aTitle delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:aOneTitle,aTwoTile,aThreeTile,aFoureTile,aFiveTile,aSixTile, nil];
        
        
        [sheet showInView:aView];
        
        
        [sheet release];

    }
    
    
    return self;
    

}

#pragma mark - actionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    kata_SendMyOffer *senderMyOffer = [[kata_SendMyOffer alloc]init];

    switch (buttonIndex) {
        case 0:
        {

            senderMyOffer.sheetSelectTitle = self.oneTitle;
            
            
            NSLog(@"  senderMyOffer.sheetOneTitle = %@",  senderMyOffer.sheetSelectTitle );
            
        }
            break;
        case 1:
        {
            
            senderMyOffer.sheetSelectTitle = self.twoTitle;
        
        }
            break;
        case 2:
        {
            senderMyOffer.sheetSelectTitle = self.threeTitle;

        }
            break;
        case 3:
        {
            senderMyOffer.sheetSelectTitle = self.foureTitle;

        }
            break;
        case 4:
        {
            senderMyOffer.sheetSelectTitle = self.fiveTitle;

        }
            break;
        case 5:
        {
            senderMyOffer.sheetSelectTitle = self.sixTitle;

        }
            break;
      

        default:
            break;
    }
    
    senderMyOffer.sheetSelectIndex = buttonIndex;

    if ([self.delegate respondsToSelector:@selector(kata_ToolsWithSendMyOffer: tag:)]) {
        
        [self.delegate kata_ToolsWithSendMyOffer:senderMyOffer tag:self.tag];

    }
    
   
    [senderMyOffer release];

    
}


+ (NSString *)date:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
    
    return auditingDate;
}

+ (NSString *)deliveryDate:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
    
    return auditingDate;
}


@end
