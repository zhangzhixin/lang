//
//  KATADebug.m
//  CityStar
//
//  Created by 黎 斌 on 12-8-20.
//  Copyright (c) 2012年 kata. All rights reserved.
//

#import "KATADebug.h"
#import "kata_GlobalConst.h"

@implementation KATADebug

+ (void)console:(NSString *)format data:(id)data
{
	
	if ([MODE isEqualToString:@"DEBUG"]) {
		NSLog(format, data);
	}
	
}

+ (void)printClassName:(UIView *)pView
{
    NSUInteger num = [[pView subviews] count];
    
    NSString * pViewClassName = NSStringFromClass([pView class]);
    
    [KATADebug console:@"PRINT::class name is # %@ #" data:pViewClassName];
    
    for (NSUInteger i = 0; i < num; i++) {
        NSString * subclassName = NSStringFromClass([[[pView subviews] objectAtIndex:i] class]);
        
        [KATADebug console:@"PRINT::subclass name is # %@ #" data:subclassName];
    }
    
}

@end
