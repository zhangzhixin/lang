//
//  kata_testViewController.m
//  LargeClan
//
//  Created by kata on 13-11-23.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_testViewController.h"

#import "KATAUITabBarController.h"


@interface kata_testViewController ()

@end

@implementation kata_testViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self.navigationItem setHidesBackButton:YES];
    
    
    

    
    KATAUITabBarController *tab = (KATAUITabBarController *)self.tabBarController;
    
    [tab hideTabBar];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title =  @"我的测试";
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    
    [backBtn setImage:[UIImage imageNamed:@"return.png"] forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    
    self.navigationItem.leftBarButtonItem  = leftItem;
    
    [leftItem release];

    
    
    UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    btn.frame = CGRectMake(0, 200, 80, 80);
    [btn addTarget:self action:@selector(test) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 320, 460-49, 49)];
//    
//    imageView.image = [UIImage imageNamed:@"mainTabBarBg_selected.png"];
//    
//    [self.view addSubview:imageView];
//    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    button.frame = CGRectMake(0, 0, 320, 32);
//    
//    [button setImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];
//    
//    [imageView addSubview:button];
//    
}
-(void)test
{
       
}
-(void)backClick
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
