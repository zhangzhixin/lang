//
//  kata_AmendMyOfferRequest.h
//  LargeClan
//
//  Created by kata on 14-1-20.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_BaseRequest.h"

@interface kata_AmendMyOfferRequest : kata_BaseRequest


- (id)initWithOfferid:(NSString *)aOfferid;
@end
