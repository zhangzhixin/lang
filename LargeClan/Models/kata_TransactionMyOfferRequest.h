//
//  kata_TransactionMyOfferRequest.h
//  LargeClan
//
//  Created by kata on 14-1-17.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_BaseRequest.h"

@interface kata_TransactionMyOfferRequest : kata_BaseRequest



- (id)intWithNameofpart:(int)aNameofpart
                 userid:(NSString *)aUserid
                Offerid:(NSString *)aOfferid
              Dealprice:(NSString *)aDealprice;

- (NSString *)method;

- (NSMutableDictionary *)params;

@end
