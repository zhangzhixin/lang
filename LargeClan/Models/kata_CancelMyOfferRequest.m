//
//  kata_CancelMyOfferRequest.m
//  LargeClan
//
//  Created by kata on 14-1-19.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_CancelMyOfferRequest.h"

//@implementation kata_CancelMyOfferRequest
//
//
//
//@end

@interface kata_CancelMyOfferRequest ()


@property(nonatomic,retain)NSMutableDictionary *params;

@end

@implementation kata_CancelMyOfferRequest



- (id)initWithNameofpart:(int)aNameOfPart
                  userid:(NSString *)aUserid
                 offerid:(NSString *)aOfferid
{
    
    if (self = [super init]) {
        
        [self.params setObject:[NSNumber numberWithInt:aNameOfPart] forKey:@"nameofpart"];
        
        [self.params setObject:aUserid forKey:@"userid"];
        
        [self.params setObject:aOfferid forKey:@"offerid"];
        
    }
    
    return self;
}


- (NSString *)method
{
    return @"index_offeruser_get";
}




@end
