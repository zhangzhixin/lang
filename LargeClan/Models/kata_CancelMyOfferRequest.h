//
//  kata_CancelMyOfferRequest.h
//  LargeClan
//
//  Created by kata on 14-1-19.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_BaseRequest.h"

@interface kata_CancelMyOfferRequest : kata_BaseRequest

- (id)initWithNameofpart:(int)aNameOfPart
                  userid:(NSString *)aUserid
                 offerid:(NSString *)aOfferid;



@end
