//
//  kata_AmendMyOfferRequest.m
//  LargeClan
//
//  Created by kata on 14-1-20.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_AmendMyOfferRequest.h"

@implementation kata_AmendMyOfferRequest


- (id)initWithOfferid:(NSString *)aOfferid
{
    
    if (self = [super init]) {

        [self.params setObject:aOfferid forKey:@"offerid"];
        
    }
    
    return self;
}


- (NSString *)method
{
    return @"offer_updateOffer_get";
    
}




@end
