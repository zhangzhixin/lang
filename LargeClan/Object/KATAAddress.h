//
//  KATAAddress.h
//  LargeClan
//
//  Created by kata on 13-12-23.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KATAAddress : NSObject



@property (nonatomic,retain) NSString * addressId;
@property (nonatomic,retain) NSString * addressName;

@end
