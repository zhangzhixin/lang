//
//  KATARegInfo.h
//  LargeClan
//
//  Created by kata on 13-12-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KATARegInfo : NSObject

@property (nonatomic,retain) NSString * handPhoneStr;
@property (nonatomic,retain) NSString * verificationStr;
@property (nonatomic,retain) NSString * enterPsswordStr;
@property (nonatomic,retain) NSString * repetitionPsswordStr;
@property (nonatomic,retain) NSString * companyTypeStr;
@property (nonatomic,retain) NSString * companyNameStr;
@property (nonatomic,retain) NSString * companyAbbreviationStr;
@property (nonatomic,retain) NSString * companyPhoneStr;
@property (nonatomic,retain) NSString * operationPeopleStr;
@property (nonatomic,retain) NSString * QQStr;


@property (nonatomic,retain) NSString * ZongStr;


@property (nonatomic,assign) int companyType;

@end
