//
//  KATAAddress.m
//  LargeClan
//
//  Created by kata on 13-12-23.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "KATAAddress.h"

@implementation KATAAddress


- (void)dealloc
{
    
    [_addressId release];
    
    [_addressName release];
    
    [super dealloc];
}
@end
