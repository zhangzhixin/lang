//
//  kata_LeftTableDelegate.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_LeftTableDelegate.h"
#import "KATAIndex.h"
#import "KATAConstants.h"
#import "KATAUtils.h"
#import "kata_LeftCell.h"
#import "KATAUITabBarController.h"
@interface kata_LeftTableDelegate ()


@property(nonatomic,retain)NSMutableArray *leftDataArray;
@end



@implementation kata_LeftTableDelegate


- (id)init
{
    
    if (self = [super init]) {
        
        
        _leftDataArray = [[NSMutableArray alloc]initWithCapacity:0];
        

    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leftDefaultIndexData:) name:DEFAULT_INDEXData object:nil];
        
        //搜索结果
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchResult:) name:SEARCH_RESULT object:nil];

        
        
        //我的报盘
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myOfferData:) name:MYOFFERDATA object:nil];

        
    }
    
    return self;
    
}
- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [_leftDataArray release];
    
    [super dealloc];
}
#pragma mark - NSNotificationCenter -- MYOFFERDATA

- (void)myOfferData:(NSNotification *)notification
{
    
    NSDictionary *dic = (NSDictionary *)[notification userInfo];
    
    DebugLog(@"dicdic == %@",dic);
    
    [self leftData:dic];
}
#pragma mark - NSNotificationCenter -- SEARCH_RESULT

- (void)searchResult:(NSNotification *)notification
{
    NSDictionary *dic = [(NSDictionary *)[notification userInfo] retain];
    
    DebugLog(@"dicdic == %@",dic);
    
    [self leftData:dic];
}

#pragma mark - NSNotificationCenter -- leftDefaultIndexData

- (void)leftDefaultIndexData:(NSNotification *)notification
{
    NSDictionary *dic = (NSDictionary *)[notification userInfo] ;

    DebugLog(@"dicdic == %@",dic);
    
    [self leftData:dic];
    
    
}


- (void)leftData:(NSDictionary *)dic
{
   
    
    [_leftDataArray removeAllObjects];
    
    [_leftTableView reloadData];

    DebugLog(@"dic ==  %@",dic);
    
    NSString *code = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"code"] intValue]];
    
    if ([dic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        if ([offer_results count] != 0) {
            
            for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                
                NSString *megCode = [dic objectForKey:@"code"];
                
                indexObj.code = megCode;
                              

                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                
                [self.leftDataArray addObject:indexObj];
                
                [indexObj release];
                
                
            }
            
            
        }
        if ([offer_results count] != 0 && [_leftDataArray count ] != 0) {
            
            [_leftTableView reloadData];
            
        }

    }
    
   
}
//#pragma mark - dateFromString
//
//
//- (NSString *)date:(NSDate *)aDate
//{
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    
//    
//    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
//    
//    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
//    
//    
//    NSString *destDateString = [dateFormatter stringFromDate:aDate];
//    
//    
//    [dateFormatter release];
//    
//    
//    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
//    
//    return auditingDate;
//}
//
//
//- (NSString *)deliveryDate:(NSDate *)aDate
//{
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    
//    
//    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
//    
//    [dateFormatter setDateFormat:@"MM/dd zzz"];
//    
//    
//    NSString *destDateString = [dateFormatter stringFromDate:aDate];
//    
//    
//    [dateFormatter release];
//    
//    
//    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
//    
//    return auditingDate;
//}
//

#pragma mark - rightTableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
    
    vi.backgroundColor = [UIColor blueColor];
    
//    UILabel *timeLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 44)];
//    
//    timeLbl.text = @"报盘时间";
//    
//
//    timeLbl.adjustsFontSizeToFitWidth = YES;
//    
//    [vi addSubview:timeLbl];
//
//    [timeLbl release];
    
    
    UILabel *numberLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
    
    numberLbl.text = @"报盘编号";
    
    numberLbl.adjustsFontSizeToFitWidth = YES;
    
    numberLbl.textAlignment = NSTextAlignmentCenter;
    
    [vi addSubview:numberLbl];
    
    [numberLbl release];

    
    return vi;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [self.leftDataArray count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"======");
    
    static  NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        
        cell = [[[kata_LeftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
    }
    
    
//    UILabel *nameLab = (UILabel *)[cell viewWithTag:111];
    
    UILabel *numberLab = (UILabel *)[cell viewWithTag:112];
    
    KATAIndex *indexObj = [_leftDataArray objectAtIndex:indexPath.row];
    
//    //时间
//    nameLab.text  = indexObj.auditingstatus;
//    
//    [nameLab setFont:[UIFont systemFontOfSize:10.0f]];
    
    //报盘编号
    
    [numberLab setFont:[UIFont  systemFontOfSize:8.0f]];
    
    numberLab.text  = indexObj.code;
    
    UIImageView *imageView1 = (UIImageView *)[cell viewWithTag:11];
    
    
    if ([indexObj.notfaze isEqualToString:@"0"]) {
        
        [imageView1 setImage:[UIImage imageNamed:@"w.png"]];
        
    }else
    {
        
        imageView1.backgroundColor = [UIColor clearColor];
        
    }
    
    //            indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
    
    
    UIImageView *imageView2 = (UIImageView *)[cell viewWithTag:12];
    
    
    [imageView2 setImage:[UIImage imageNamed:@"rz.png"]];
    


    cell.selectionStyle = UITableViewCellSelectionStyleNone;

//    cell.textLabel.text = @"left";

    return cell;



}


@end
