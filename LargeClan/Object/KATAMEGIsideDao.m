//
//  KATAMEGIsideDao.m
//  LargeClan
//
//  Created by kata on 14-1-3.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "KATAMEGIsideDao.h"

@implementation KATAMEGIsideDao





- (void)dealloc
{
    
    [_pricefrom release];
    
    [_priceto release];
    [_dirStr release];
    [_priceStr release];
    [_goodsStr release];
    [_shipStr release];
    [_arrivalStr release];
    [_stateStr release];
    [_addressStr release];
    [_dateStr1 release];
    [_dateStr2 release];
    [_dateStr3 release];
    [_dateStr4 release];
    [_delivelyTimeStr release];
   
    
    [super dealloc];
}
@end
