//
//  KATAGETArrayDao.m
//  LargeClan
//
//  Created by kata on 14-1-3.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "KATAGETArrayDao.h"

@interface KATAGETArrayDao ()


@property(nonatomic,retain)NSMutableArray *insideIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *insideIndexArrayPTA;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayPTA;



@end




@implementation KATAGETArrayDao



- (NSMutableArray *)getInsideIndexArrayMEG
{
    _insideIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];

    return _insideIndexArrayMEG;
}

-  (NSMutableArray *)getOuterIndexArrayMEG
{
    _outerIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"方向",@"美金（CFR）",@"类型",@"货源地",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];
    
    return _outerIndexArrayMEG;
}
- (NSMutableArray *)getInsideIndexArrayPTA
{
    _insideIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"人民币",@"配送",@"品牌",@"货种",@"交割时间",@"交割地",@"数量（吨）",@"报盘人",@"保证金",@"发票",@"状态",@"备注", nil];
    
    return _insideIndexArrayPTA;
}

- (NSMutableArray *)getOuterIndexArrayPTA
{
    _outerIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"方向",@"美金（CFR）",@"类型",@"货源地",@"品牌",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];
    
    return _outerIndexArrayPTA;
}


- (void)dealloc
{
    
    [_insideIndexArrayMEG release];
    
    [_outerIndexArrayMEG release];
    
    [_insideIndexArrayPTA release];
    
    [_outerIndexArrayPTA release];

    [super dealloc];
}



@end
