//
//  kata_RightTableDelegate.m
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//



#import "kata_RightTableDelegate.h"
#import "KATAConstants.h"
#import "KATAUtils.h"
#import "KATAIndex.h"
#import "KATAConstants.h"
@interface kata_RightTableDelegate ()



@property(nonatomic,retain)NSMutableArray *insideIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayMEG;
@property(nonatomic,retain)NSMutableArray *insideIndexArrayPTA;
@property(nonatomic,retain)NSMutableArray *outerIndexArrayPTA;

@property(nonatomic,retain)NSMutableArray *rightHeaderArr;


@property(nonatomic,retain)NSMutableArray *rightContentArr;



@end

@implementation kata_RightTableDelegate

- (id)init
{
    if (self = [super init]) {
        
        _rightHeaderArr  =  [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];
        
        _insideIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"人民币",@"货种",@"交割时间",@"交割地",@"保证金",@"数量（吨）",@"报盘人",@"发票",@"免仓",@"状态",@"备注", nil];
        _outerIndexArrayMEG = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"美金（CFR）",@"类型",@"货源地",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];
        
       _insideIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"人民币",@"配送",@"品牌",@"货种",@"交割时间",@"交割地",@"数量（吨）",@"报盘人",@"保证金",@"发票",@"状态",@"备注", nil];
        
        _outerIndexArrayPTA = [[NSMutableArray alloc]initWithObjects:@"报盘时间",@"方向",@"美金（CFR）",@"类型",@"货源地",@"品牌",@"预装船",@"预到港",@"转手",@"交单",@"交割地",@"数量（吨）",@"报盘人",@"清单可借",@"付款方式",@"状态",@"备注", nil];

        
        _rightContentArr = [[NSMutableArray alloc]initWithCapacity:0];
        

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(indexHeardViewRightData:) name:INDEX_HEARDVIEW object:nil];

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rightDefaultIndexData:) name:DEFAULT_INDEXData object:nil];

        
        //搜索结果
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchResult:) name:SEARCH_RESULT object:nil];
        
        //我的报盘
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myOfferRightData:) name:MYOFFERDATA object:nil];

        
        //改变表头上的数据           //改变表头上的数据

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchResultHeardView:) name:SEARCH_RESULT_HEARDView object:nil];



    }
    
    
    return self;
    
}
- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];


//    [[NSNotification description] removeObserver:self forKeyPath:@"requestData"];

    
//    [[NSNotification description] removeObserver:self forKeyPath:@"qureyResult"];

    
    
    [_rightHeaderArr release];
    
    [_insideIndexArrayPTA release];
    [_outerIndexArrayPTA release];
    [_insideIndexArrayMEG release];
    [_outerIndexArrayMEG release];

    [_rightContentArr release];
    
    
    [super dealloc];
}

#pragma - NSNotificationCenter - tableViewHeardViewData

- (void)searchResultHeardView:(NSNotification *)notification

{
    int type = [[notification object] intValue];
    
    switch (type) {
        case 0:
        {
            [self changeArray:_insideIndexArrayMEG];
            
        }
            break;
        case 1:
        {
            
            [self changeArray:_outerIndexArrayMEG];
            
        }
            break;
        case 2:
        {
            
            [self changeArray:_insideIndexArrayPTA];
            
        }
            break;
        case 3:
        {
            [self changeArray:_outerIndexArrayPTA];
            
        }
            break;
            
        default:
            break;
    }
    

    
}

#pragma mark - NSNotificationCenter -- MYOFFERDATA

- (void)myOfferRightData:(NSNotification *)notification
{
    
    NSDictionary *dic = [notification userInfo] ;
    
    DebugLog(@"dicdic == %@",dic);
    
    [self rightData:dic];
    

}

#pragma mark - NSNotificationCenter -- SEARCH_RESULT
- (void)searchResult:(NSNotification *)notification
{
    NSDictionary *dic = [(NSDictionary *)[notification userInfo] retain];
    
    DebugLog(@"dic =  %@",dic);
    
    [self rightData:dic];

    
}
#pragma mark - NSNotificationCenter -- INDEX_HEARDVIEW

- (void)indexHeardViewRightData:(NSNotification *)notification
{
    
    int type = [[notification object] intValue] - 1;
    
    switch (type) {
        case 0:
        {
            [self changeArray:_insideIndexArrayMEG];
            
        }
            break;
        case 1:
        {
            [self changeArray:_outerIndexArrayMEG];
            
            
        }
            break;
        case 2:
        {
            [self changeArray:_insideIndexArrayPTA];
            
            
        }
            break;
        case 3:
        {
            [self changeArray:_outerIndexArrayPTA];
            
        }
            break;
            
        default:
            break;
    }
    
}
- (void)changeArray:(NSMutableArray *)aArray
{
    [_rightHeaderArr removeAllObjects];
    
    [_rightHeaderArr addObjectsFromArray:aArray];
    
    [_rightTableView reloadData];
    
}

#pragma mark - NSNotificationCenter -- rightDefaultIndexData


- (void)rightDefaultIndexData:(NSNotification *)notification
{
    NSDictionary *dic = (NSDictionary *)[notification userInfo];
    
    DebugLog(@"dic =  %@",dic);
    
    [self rightData:dic];

}

- (void)changeSearchResultPageData:(NSNotification *)notification
{
    
    int tag = [[notification object] intValue];
    
    switch (tag) {
        case 0:
        {
            [self changeArray:_insideIndexArrayMEG];
            
        }
            break;
        case 1:
        {
            [self changeArray:_outerIndexArrayMEG];
            
            
        }
            break;
        case 2:
        {
            [self changeArray:_insideIndexArrayPTA];
            
            
        }
            break;
        case 3:
        {
            [self changeArray:_outerIndexArrayPTA];
            
        }
            break;
            
        default:
            break;
    }
  
    
}


- (void)rightData:(NSDictionary *)dic
{
    [_rightContentArr removeAllObjects];

    [_rightTableView reloadData];
    
    DebugLog(@"dic ==  %@  dic retainCount ===  %d",dic,[dic retainCount]);
    
//    NSString *code = [dic objectForKey:@"code"];
    
    if ([dic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [dic objectForKey:@"data"];
        
//        DebugLog(@"dic ==  %@  dic retainCount ===  %d",dic,[dic retainCount]);

        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        
        if ([offer_results count] != 0) {
            
                for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                //报盘时间
                 if ([dic objectForKey:@"auditingDate"]  != [NSNull null])
                     
                 {
                     long long time = [[[dic objectForKey:@"auditingDate"] objectForKey:@"time"] longLongValue];
                     
                     NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
                     
                     //        NSLog(@"confromTimesp ==  %@",confromTimesp);
                     
                     
                     indexObj.auditingstatus = [self date:confromTimesp];

                 }
                
                
                //交割时间
                
                if ([dic objectForKey:@"deliverytime2"]  != [NSNull null] && [dic objectForKey:@"deliverytime2"] != [NSNull null]) {
                    
                    
                    
                    long long deliveryTime1 = [[[dic objectForKey:@"deliverytime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime1 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.deliverytime1 = [self deliveryDate:confromTimesp1];
                    
                    
                    long long deliveryTime2 = [[[dic objectForKey:@"deliverytime2"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp2 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime2 / 1000)];
                    
                    NSLog(@"confromTimesp2 ==  %@",confromTimesp2);
                    
                    indexObj.deliverytime2 = [self deliveryDate:confromTimesp2];
                    
                    NSString *deliveryStr1  = [indexObj.deliverytime1 stringByReplacingOccurrencesOfString:@"GMT+" withString:@"-"];
                    
                    NSString *deliveryStr2  = [indexObj.deliverytime2 stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                    
                    indexObj.deliverytime = (NSMutableString *)[deliveryStr1 stringByAppendingString:deliveryStr2];
                    
                    DebugLog(@"indexObj.deliverytime ==  %@",indexObj.deliverytime);
                    
                    
                }
                
                
                //预计装船开始时间
                if ([dic objectForKey:@"loadingtime1"]  != [NSNull null]) {
                    
                    
                    
                    long long loadingtime1 = [[[dic objectForKey:@"loadingtime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime1 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.loadingtime1 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                }
                
                //预计装船结束时间
                
                if ([dic objectForKey:@"arrivetime1"]  != [NSNull null]) {
                    
                    
                    
                    long long loadingtime2 = [[[dic objectForKey:@"arrivetime1"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime2 / 1000)];
                    
                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);
                    
                    indexObj.loadingtime2 = [[self deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];
                    
                }
                //
                //货源地
                
                if ( [dic objectForKey:@"goodsSource"] != [NSNull null]) {
                    
                    
                    NSDictionary *goodsSource  =[dic objectForKey:@"goodsSource"];
                    
                    NSString *goodssourcename = [goodsSource objectForKey:@"goodssourcename"];
                    
                    indexObj.goodssourcename = goodssourcename;
                    
                }
                
                //
                //交割地
                
                if ( [dic objectForKey:@"region"] != [NSNull null]) {
                    
                    NSDictionary *regionDic  = [dic objectForKey:@"region"];
                    
                    NSString *deliveryname = [regionDic objectForKey:@"deliveryname"];
                    
                    indexObj.deliveryname = deliveryname;
                    
                    
                }
              
                // 转手
                if ( [dic objectForKey:@"changehands"] != [NSNull null]) {
                    
                    int  changehands =  [[dic objectForKey:@"changehands"] intValue];
                    
                    DebugLog(@"changehands = %d",changehands);
                    
                    indexObj.changehands = [NSString stringWithFormat:@"%d手",changehands];
                    
                    DebugLog(@"indexObj.changehands = %@",indexObj.changehands);
                }
                
                // 交单天数
                
                if ( [dic objectForKey:@"surrenderdocuments"] != [NSNull null]) {
                    
                    int  surrenderdocuments =  [[dic objectForKey:@"surrenderdocuments"] intValue];
                    
                    indexObj.surrenderdocuments = [NSString stringWithFormat:@"%d天",surrenderdocuments];
                    
                }
                
                // 付款方式
                
                if ( [dic objectForKey:@"paymentmethods"] != [NSNull null]) {
                    
                    int  paymentmethods =  [[dic objectForKey:@"paymentmethods"] intValue];
                    
                    
                    if (paymentmethods == 0) {
                        
                        indexObj.paymentmethods = @"L/C90 天";
                        
                    }else if (paymentmethods == 1) {
                        
                        indexObj.paymentmethods = @"L/C 即期";
                        
                    }
                    else if (paymentmethods == 2) {
                        
                        indexObj.paymentmethods = @"L/T ";
                        
                    }
                    else{
                        indexObj.paymentmethods = @"其他";
                        
                    }
                    
                    
                    
                }
                
                // 单据类型
                
                if ( [dic objectForKey:@"detailedlist"] != [NSNull null]) {
                    
                    int  detailedlist = [[dic objectForKey:@"detailedlist"] intValue];
                    
                    if (detailedlist == 0) {
                        
                        indexObj.detailedlist = @"是";
                        
                    }else
                    {
                        indexObj.detailedlist = @"否";
                        
                    }
                    
                }
                
                //品名
                
                if ( [dic objectForKey:@"nameofpart"] != [NSNull null]){
                    
                    int nameofpart = [[dic objectForKey:@"nameofpart"] intValue];
                    
                    indexObj.nameofpart = nameofpart;
                    
                    
                }
                //税 类型
                
                if ( [dic objectForKey:@"cargobonded"] != [NSNull null]){
                    
                    
                    int cargobonded = (int)[dic objectForKey:@"cargobonded"];
                    
                    
                    if (cargobonded == 0) {
                        
                        indexObj.cargobonded = @"船税";
                    }else
                    {
                        indexObj.cargobonded = @"保税";
                        
                    }
                    
                }
                
                
                //配送
                
                if ( [dic objectForKey:@"dispatching"] != [NSNull null]){
                    
                    int dispatching = [[dic objectForKey:@"dispatching"] intValue];
                    
                    if (dispatching == 0) {
                        
                        indexObj.dispatching = @"自提";
                        
                    }else{
                        
                        indexObj.dispatching = @"送到";
                    }
                }
                
                //品牌
                if ( [dic objectForKey:@"brand"] != [NSNull null]){
                    
                    indexObj.brand =  [[dic objectForKey:@"brand"] objectForKey:@"brandname"];
                    
                }
                
                
                
                //        //价格
                
                NSString *quotation  = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"quotation"] intValue]];
                
                indexObj.quotation = quotation;
                
                
                //方向
                int  direction =  [[dic objectForKey:@"direction"] intValue];
                
                DebugLog(@"directiondirection ==  %d",direction);
                
                if (direction == 0) {
                    
                    indexObj.direction = @"卖盘";
                    
                }else
                {
                    indexObj.direction = @"买盘";
                    
                }
                
                
                
                NSString *megCode = [dic objectForKey:@"code"];
                
                indexObj.code = megCode;
                
                
                //有效期
                int offerstate = [[dic objectForKey:@"offerstate"] intValue];
                
                if (offerstate == 0) {
                    
                    indexObj.offerstate = @"有效";
                    
                }else{
                    
                    indexObj.offerstate = @"过期";
                    
                }
                //是否成交
                int dealstatus = [[dic objectForKey:@"dealstatus"] intValue];
                
                if (dealstatus == 0) {
                    
                    indexObj.dealstatus = @"/成交";
                    
                }else{
                    
                    indexObj.dealstatus = @"/未成交";
                    
                }
                
                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                
                
                //
                
                //审核状态
                int auditingstatus = (int) [dic objectForKey:@"auditingstatus"];
                
                //是否删除
                BOOL isdeleted = (BOOL) [dic objectForKey:@"isdeleted"];
                
                //是否发布
                BOOL iscancel = (BOOL) [dic objectForKey:@"iscancel"];
                
                
                //期货类型
                int spotfutures = (int) [dic objectForKey:@"spotfutures"];
                
                
                if (spotfutures == 0) {
                    
                    indexObj.spotfutures = @"期货";
                    
                }else
                {
                    indexObj.spotfutures = @"现货";
                    
                }
                
                //吨数
                int number = (int) [dic objectForKey:@"number"];
                
                
                if (number == 0) {
                    
                    indexObj.number = @"500";
                    
                }else  if (number == 1)
                {
                    indexObj.spotfutures = @"1000";
                    
                }
                else  if (number == 2)
                {
                    indexObj.number = @"1500";
                    
                }
                else  if (number == 3)
                {
                    indexObj.number = @"2000";
                    
                }
                else  if (number == 4)
                {
                    indexObj.number = @"2000";
                    
                }
                else
                {
                    indexObj.number = @"500";
                    
                }
                
                
                //是否免仓
                int storagetime = (int) [dic objectForKey:@"storagetime"];
                
                if (storagetime == 0) {
                    
                    indexObj.storagetime = @"五天";
                    
                }else{
                    
                    indexObj.storagetime = @"七天";
                }
                
                
                
                //保证金
                int cautionmoney = (int) [dic objectForKey:@"cautionmoney"];
                
                if (cautionmoney == 0) {
                    
                    indexObj.cautionmoney = @"0";
                    
                }else if (spotfutures == 1)
                {
                    indexObj.cautionmoney = @"5%";
                    
                }else
                {
                    indexObj.cautionmoney = @"10%";
                    
                }
                
                //备注
                NSString *remarks = (NSString *) [dic objectForKey:@"remarks"];
                
                
                indexObj.remarks = remarks;
                
                //报盘人
                
                int offerpeople = (int)[dic objectForKey:@"offerpeople"];
                
                //发票
                int invoice = (int)[dic objectForKey:@"invoice"];
                
                if (invoice == 0) {
                    
                    indexObj.invoice = @"本月";
                    
                }else
                {
                    indexObj.invoice = @"下月";
                    
                }
                
                
                //勿扰
                
                
                if ( [dic objectForKey:@"notfaze"] != [NSNull null]) {
                    
                    int  notfaze = [[dic objectForKey:@"notfaze"] intValue];
                    
                    if (notfaze == 0) {
                        
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }else
                    {
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }
                    
                }
                    
                [_rightContentArr addObject:indexObj];
                
                [indexObj release];
                
            }
            
            
            if ([offer_results count] != 0 && [_rightContentArr count ] != 0) {
                
                [_rightTableView reloadData];
                
            }
            
        }
        
        

        
        
    }
    
}



#pragma makr -  kata_segViewDelegate




#pragma mark - rightTableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44.0f;

}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 140, 44)];
    
    vi.backgroundColor = [UIColor blueColor];
    

    for (int i = 0; i < [_rightHeaderArr count]; i++) {
        
        
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
        
        lbl.text = [_rightHeaderArr objectAtIndex:i];
        
        lbl.textAlignment = NSTextAlignmentCenter;
        
        lbl.adjustsFontSizeToFitWidth = YES;
        
        [vi addSubview:lbl];
        
        [lbl release];
        
        
    }
    
    
    
    
    return [vi autorelease];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
        
    return [_rightContentArr count];

    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    DebugLog(@" =============");
    
    static  NSString *CellIdentifier0 = @"Cell0";
    
    static  NSString *CellIdentifier1 = @"Cell1";

    static  NSString *CellIdentifier2 = @"Cell2";

    static  NSString *CellIdentifier3 = @"Cell3";

    
//    if ([_rightContentArr count] == 0) {
//        
    
        
            KATAIndex *indexObj = [_rightContentArr objectAtIndex:indexPath.row];
            
            
            int nameofpart = indexObj.nameofpart;
            
            if (nameofpart == 0) {
                
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier0];
                
                
                
                if (!cell) {
                    
                    cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier0] autorelease];
                    
                    for (int  i = 0; i < [_rightHeaderArr  count];  i ++)
                    {
                        
                        UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                        
                        [lab setFont:[UIFont systemFontOfSize:14]];
                        
                        lab.tag =  i + 10;
                        
                        lab.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lab];
                        
                        [lab release];
                        
                        
                        if (lab.tag == 18) {
                            
                            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                            
                            imageView.tag = 222;
                            
                            //                    imageView.backgroundColor = [UIColor redColor];
                            
                            [lab addSubview:imageView];
                            
                            [imageView release];
                            
                            
                        }
                        
                    }
                    
                    
                    
                }
                
                UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
                
                [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
                
                //时间
                UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
                
                timeLab.text = indexObj.auditingstatus;
                
                //方向
                UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
                
                directionLab.text = indexObj.direction;
                
                //人民币
                UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
                
                quotationLab.text  = indexObj.quotation;
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:13];
                
                spotfuturesLab.text = indexObj.spotfutures;
                
                
                //                    //交割时间
                UILabel *deliveryLab = (UILabel *)[cell viewWithTag:14];
                
                deliveryLab.text = indexObj.deliverytime;
                
                //                    //交割地
                UILabel *deliverynameLab = (UILabel *)[cell viewWithTag:15];
                
                deliverynameLab.text = indexObj.deliveryname;
                
                
                //保证金
                UILabel *cautionmoneyLab = (UILabel *)[cell viewWithTag:16];
                
                cautionmoneyLab.text = indexObj.cautionmoney;
                
                
                //数量
                UILabel *numberLab = (UILabel *)[cell viewWithTag:17];
                
                numberLab.text = indexObj.number;
                
                //报盘人
                UILabel *offerPeopleLab = (UILabel *)[cell viewWithTag:18];
                
                //发票
                UILabel *invoiceLab = (UILabel *)[cell viewWithTag:19];
                
                invoiceLab.text = indexObj.invoice;
                
                //免仓
                UILabel *storagetimeLab = (UILabel *)[cell viewWithTag:20];
                storagetimeLab.text = indexObj.storagetime;
                
                //                    //状态
                UILabel *offerstateLab = (UILabel *)[cell viewWithTag:21];
                
                offerstateLab.text = indexObj.state;
                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cell viewWithTag:22];
                
                remarksLab.text  = indexObj.remarks;
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;
                
                
            }else if(nameofpart == 1){
                
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
                
                
                
                if (!cell) {
                    
                    cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
                    
                    for (int  i = 0; i < [_rightHeaderArr  count];  i ++)
                    {
                        
                        UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                        
                        [lab setFont:[UIFont systemFontOfSize:14]];
                        
                        lab.tag =  i + 10;
                        
                        lab.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lab];
                        
                        [lab release];
                        
                        
                        if (lab.tag == 21) {
                            
                            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                            
                            imageView.tag = 222;
                            
                            //                        imageView.backgroundColor = [UIColor redColor];
                            
                            [lab addSubview:imageView];
                            
                            [imageView release];
                            
                            
                        }
                        
                    }
                    
                }
                
                UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
                
                [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
                
                //时间
                UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
                
                timeLab.text = indexObj.auditingstatus;
                
                // 方向
                UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
                
                directionLab.text = indexObj.direction;
                
                //美金
                UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
                
                quotationLab.text  = indexObj.quotation;
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:13];
                
                spotfuturesLab.text = indexObj.cargobonded;
                
                
                //                    //货源地
                UILabel *goodssourcenameLab = (UILabel *)[cell viewWithTag:14];
                
                goodssourcenameLab.text = indexObj.goodssourcename;
                
                
                //                    //预计到港时间  开始
                
                UILabel *loadingtime1Lab = (UILabel *)[cell viewWithTag:15];
                
                loadingtime1Lab.text = indexObj.loadingtime1;
                
                
                //                    //预计到港时间 结束
                
                UILabel *loadingtime2Lab = (UILabel *)[cell viewWithTag:16];
                
                loadingtime2Lab.text = indexObj.loadingtime2;
                
                
                
                //转手
                UILabel *changehandsLab = (UILabel *)[cell viewWithTag:17];
                
                changehandsLab.text = indexObj.changehands;
                
                //交单
                UILabel *surrenderdocumentsLab = (UILabel *)[cell viewWithTag:18];
                
                surrenderdocumentsLab.text = indexObj.surrenderdocuments;
                
                //交割地
                UILabel *regionLab = (UILabel *)[cell viewWithTag:19];
                
                regionLab.text = indexObj.deliveryname;
                
                //数量
                UILabel *numberLab = (UILabel *)[cell viewWithTag:20];
                
                numberLab.text = indexObj.number;
                
                //                   报盘人
                //                UILabel *offerstateLab = (UILabel *)[cell2Out viewWithTag:20];
                
                //                offerstateLab.text = indexObj.offerstate;
                
                //                    //清单
                
                UILabel *detailedlistLab = (UILabel *)[cell viewWithTag:21];
                
                detailedlistLab.text  = indexObj.detailedlist;
                
                //                    //付款方式
                UILabel *paymentmethodsLab = (UILabel *)[cell viewWithTag:22];
                
                paymentmethodsLab.text  = indexObj.paymentmethods;
                
                //                    //状态
                UILabel *offerstateLab = (UILabel *)[cell viewWithTag:23];
                
                offerstateLab.text  = indexObj.state;
                
                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cell viewWithTag:24];
                
                remarksLab.text  = indexObj.remarks;
                
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;
                
                
                
                
                
            }else if(nameofpart == 2){
                
                
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
                
                
                
                if (!cell) {
                    
                    cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                    
                    for (int  i = 0; i < [_rightHeaderArr  count];  i ++)
                    {
                        
                        UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                        
                        [lab setFont:[UIFont systemFontOfSize:14]];
                        
                        lab.tag =  i + 10;
                        
                        lab.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lab];
                        
                        [lab release];
                        
                        if (lab.tag == 19) {
                            
                            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                            
                            imageView.tag = 222;
                            
                            //                    imageView.backgroundColor = [UIColor redColor];
                            
                            [lab addSubview:imageView];
                            
                            [imageView release];
                            
                            
                        }
                        
                    }
                    
                    
                    
                }
                
                
                UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
                
                [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
                
                //时间
                UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
                
                timeLab.text = indexObj.auditingstatus;
                
                //方向
                UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
                
                directionLab.text = indexObj.direction;
                
                //人民币
                UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
                
                quotationLab.text  = indexObj.quotation;
                
                //配送
                UILabel *dispatchingLab = (UILabel *)[cell viewWithTag:13];
                
                dispatchingLab.text = indexObj.dispatching;
                
                //品牌
                UILabel *brandid = (UILabel *)[cell viewWithTag:14];
                
                brandid.text = indexObj.brand;
                
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:15];
                
                spotfuturesLab.text = indexObj.spotfutures;
                
                
                //交割时间
                UILabel *deliverytimeLab = (UILabel *)[cell viewWithTag:16];
                
                deliverytimeLab.text = indexObj.deliverytime;
                
                
                //交割地
                
                UILabel *deliverynameLab = (UILabel *)[cell viewWithTag:17];
                
                deliverynameLab.text = indexObj.deliveryname;
                
                
                
                //数量
                UILabel *numberLab = (UILabel *)[cell viewWithTag:18];
                
                numberLab.text = indexObj.number;
                
                //报盘人
                UILabel *offerpeopleLab = (UILabel *)[cell viewWithTag:19];
                
                //        offerpeopleLab.text = indexObj.offerpeople;
                
                //保证金
                UILabel *cautionmoneyLab = (UILabel *)[cell viewWithTag:20];
                
                cautionmoneyLab.text = indexObj.cautionmoney;
                
                //发票
                UILabel *invoiceLab = (UILabel *)[cell viewWithTag:21];
                
                invoiceLab.text = indexObj.invoice;
                
                //   状态
                UILabel *stateLab = (UILabel *)[cell viewWithTag:22];
                
                stateLab.text = indexObj.state;
                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cell viewWithTag:23];
                
                remarksLab.text  = indexObj.remarks;
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;
                
                
                
            }else {
                
                
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
                
                
                
                if (!cell) {
                    
                    cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
                    
                    for (int  i = 0; i < [_rightHeaderArr  count];  i ++)
                    {
                        
                        UILabel *lab  = [[UILabel alloc]initWithFrame:CGRectMake(i * LBLWIDTH, 0, LBLWIDTH, 44)];
                        
                        [lab setFont:[UIFont systemFontOfSize:14]];
                        
                        lab.tag =  i + 10;
                        
                        lab.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lab];
                        
                        [lab release];
                        
                        
                        if (lab.tag == 22) {
                            
                            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((120 - 44)/2, 11, 22, 22)];
                            
                            imageView.tag = 222;
                            
                            //                    imageView.backgroundColor = [UIColor redColor];
                            
                            [lab addSubview:imageView];
                            
                            [imageView release];
                            
                            
                        }
                        
                    }
                    
                }
                
                UIImageView *imageView  = (UIImageView *)[cell viewWithTag:222];
                
                [imageView setImage:[UIImage imageNamed:@"kefu.png"]];
                
                UILabel *timeLab = (UILabel *)[cell viewWithTag:10];
                
                timeLab.text = indexObj.auditingstatus;
                
                
                UILabel *directionLab = (UILabel *)[cell viewWithTag:11];
                
                directionLab.text = indexObj.direction;
                
                //美金
                UILabel *quotationLab = (UILabel *)[cell viewWithTag:12];
                
                quotationLab.text  = indexObj.quotation;
                
                //货种
                UILabel *spotfuturesLab = (UILabel *)[cell viewWithTag:13];
                
                spotfuturesLab.text = indexObj.cargobonded;
                
                
                //                    //货源地
                UILabel *goodssourcenameLab = (UILabel *)[cell viewWithTag:14];
                
                goodssourcenameLab.text = indexObj.goodssourcename;
                
                //品牌
                
                UILabel *brandLab = (UILabel *)[cell viewWithTag:15];
                
                brandLab.text = indexObj.brand;
                
                
                
                //                    //预计到港时间  开始
                
                UILabel *loadingtime1Lab = (UILabel *)[cell viewWithTag:16];
                
                loadingtime1Lab.text = indexObj.loadingtime1;
                
                
                //                    //预计到港时间 结束
                
                UILabel *loadingtime2Lab = (UILabel *)[cell viewWithTag:17];
                
                loadingtime2Lab.text = indexObj.loadingtime2;
                
                
                
                //转手
                UILabel *changehandsLab = (UILabel *)[cell viewWithTag:18];
                
                changehandsLab.text = indexObj.changehands;
                
                //交单
                UILabel *surrenderdocumentsLab = (UILabel *)[cell viewWithTag:19];
                
                surrenderdocumentsLab.text = indexObj.surrenderdocuments;
                
                //交割地
                UILabel *deliverynameLab = (UILabel *)[cell viewWithTag:20];
                
                deliverynameLab.text = indexObj.deliveryname;
                
                //数量
                UILabel *numberLab = (UILabel *)[cell viewWithTag:21];
                
                numberLab.text = indexObj.number;
                
                //                   报盘人
                
                UILabel *offerstateLab = (UILabel *)[cell viewWithTag:22];
                
                //                offerstateLab.text = indexObj.offerstate;
                
                //                    //清单
                
                UILabel *detailedlistLab = (UILabel *)[cell viewWithTag:23];
                
                detailedlistLab.text  = indexObj.detailedlist;
                
                //                    //付款方式
                UILabel *paymentmethodsLab = (UILabel *)[cell viewWithTag:24];
                
                paymentmethodsLab.text  = indexObj.paymentmethods;
                
                //                    //状态
                UILabel *stateLab = (UILabel *)[cell viewWithTag:25];
                
                stateLab.text  = indexObj.state;
                
                
                //                    //备注
                UILabel *remarksLab = (UILabel *)[cell viewWithTag:26];
                
                remarksLab.text  = indexObj.remarks;
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;
                
                
                }
            
            
            
//            }
//
    
}
        
      
    



#pragma mark - dateFromString


- (NSString *)date:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd HH:mm zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 9];
    
    return auditingDate;
}


- (NSString *)deliveryDate:(NSDate *)aDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    
    [dateFormatter setDateFormat:@"MM/dd zzz"];
    
    
    NSString *destDateString = [dateFormatter stringFromDate:aDate];
    
    
    [dateFormatter release];
    
    
    NSString *auditingDate = [destDateString substringToIndex:[destDateString  length] - 5];
    
    return auditingDate;
}



@end
