//
//  KATARegInfo.m
//  LargeClan
//
//  Created by kata on 13-12-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "KATARegInfo.h"

@implementation KATARegInfo



- (void)dealloc
{
    
    [_handPhoneStr release];
    [_verificationStr release];
    [_enterPsswordStr release];
    [_repetitionPsswordStr release];
    [_companyTypeStr release];
    [_companyNameStr release];
    [_companyAbbreviationStr release];
    [_companyPhoneStr release];
    [_operationPeopleStr release];
    [_QQStr release];

    [_ZongStr release];
    
    [super dealloc];
}
@end
