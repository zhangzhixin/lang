//
//  kata_LeftTableDelegate.h
//  LargeClan
//
//  Created by kata on 13-12-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface kata_LeftTableDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,retain)    UITableView *leftTableView;

@end
