//
//  KATAMEGIsideDao.h
//  LargeClan
//
//  Created by kata on 14-1-3.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KATAMEGIsideDao : NSObject

@property(nonatomic,retain)NSString *pricefrom;

@property(nonatomic,retain)NSString *priceto;

@property(nonatomic,retain)NSString *dirStr;
@property(nonatomic,retain)NSString *priceStr;
@property(nonatomic,retain)NSString *goodsStr;
@property(nonatomic,retain)NSString *shipStr;
@property(nonatomic,retain)NSString *arrivalStr;
@property(nonatomic,retain)NSString *stateStr;
@property(nonatomic,retain)NSString *addressStr;


@property(nonatomic,retain)NSString *dateStr1;
@property(nonatomic,retain)NSString *dateStr2;

@property(nonatomic,retain)NSString *dateStr3;
@property(nonatomic,retain)NSString *dateStr4;



@property(nonatomic,retain)NSString *delivelyTimeStr;


@end
