//
//  KATASearchDao.h
//  LargeClan
//
//  Created by kata on 13-12-30.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KATASearchDao : NSObject

@property (nonatomic,assign) int direction;
@property (nonatomic,assign) int dealstatus;
@property (nonatomic,assign) int cargobonded;
@property (nonatomic,assign) int spotfutures;
@property (nonatomic,assign) int brandid;
@property (nonatomic,assign) int regionid;
@property (nonatomic,assign) int dispatching;
@property (nonatomic,assign) int addressId;

@property (nonatomic,copy) NSString *temptime;
@property (nonatomic,copy) NSString *temptime1;
@property (nonatomic,copy) NSString *temptime2;
@property (nonatomic,copy) NSString * temptime3;
@property (nonatomic,copy) NSString *pricefrom;
@property (nonatomic,copy) NSString *priceto;

@end
