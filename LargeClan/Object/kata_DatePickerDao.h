//
//  kata_DatePickerDao.h
//  LargeClan
//
//  Created by kata on 14-1-10.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol kata_DatePickerDaoDelegate <NSObject>


- (void)kata_DatePickerDaoWithTime:(NSString *)aTime tag:(int)aTag;

- (void)kata_DatePickerDaoWithTimeSp:(NSString *)aTimeSp tag:(int)aTag;


@end

@interface kata_DatePickerDao : NSObject

{
    id <kata_DatePickerDaoDelegate> delegate;
}

@property(nonatomic,assign)    id <kata_DatePickerDaoDelegate> delegate;



- (id)kata_DatePickerDaoDatePicker:(int)aTag;

- (NSString *)dateStringTransformDate:(NSDate *)aDate;

@end
