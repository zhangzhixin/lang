//
//  KATAIndex.m
//  LargeClan
//
//  Created by kata on 13-12-24.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "KATAIndex.h"

@implementation KATAIndex



- (void)dealloc
{
    
    [_dealprice release];
    
    [_offerId release];
    
       [_code release];
    
    [_offertime release];
    
    [_offerstate release];
    
    [_auditingstatus release];
    
    [_isdeleted release];
    
    [_direction release];
    
    [_number release];
    
    [_storagetime release];
    
    [_cautionmoney release];
    
    [_remarks release];
    
    [_offerpeople release];
    
    [_invoice release];
    
    [_cargobonded release];
    
    [_loadingtime1 release];
    
    [_loadingtime2 release];
    
    [_arrivetime1 release];
    
    [_arrivetime2 release];
    
    [_changehands release];
    
    [_surrenderdocuments release];
    
    [_detailedlist release];
    
    [_paymentmethods release];
    
    [_dispatching release];
    
    [_brand release];
    
    [_region release];
    
    [_goodsSource release];
    
    [_user release];
    
    [_notfaze release];

    [_spotfutures release];
    
    [_deliverytime1 release];
    
    [_deliverytime2 release];
    
    [_deliverytime   release];
    
    [_deliveryname release];
    
    [_quotation release];
    
    [_goodsSource release];
    
    [_dealstatus release];
    
    [_state release];
    
    [_brandid release];
    
  
       
    [super dealloc];
}
@end
