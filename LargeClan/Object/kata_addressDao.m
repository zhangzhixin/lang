//
//  kata_addressDao.m
//  LargeClan
//
//  Created by kata on 14-1-16.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_addressDao.h"

@implementation kata_addressDao

- (void)dealloc
{
    
    
    [_cityId release];
    
    [_cityName release];
    
    [_provinceId release];
    
    [_provinceName release];
    
    [super dealloc];
    
}
@end
