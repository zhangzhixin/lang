//
//  kata_addressDao.h
//  LargeClan
//
//  Created by kata on 14-1-16.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface kata_addressDao : NSObject

@property(nonatomic,retain)NSString *provinceName;
@property(nonatomic,retain)NSString *provinceId;
@property(nonatomic,retain)NSString *cityName;
@property(nonatomic,retain)NSString *cityId;

@end
