//
//  kata_DatePickerDao.m
//  LargeClan
//
//  Created by kata on 14-1-10.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_DatePickerDao.h"

#import "KATAConstants.h"

#import "KATAUtils.h"

@interface kata_DatePickerDao ()
{
    
    BOOL isPicker;
    
    UIDatePicker  *_picker;
}
@property(nonatomic,retain)NSString *kata_DatePickerDaoTime1;
@property(nonatomic,retain)NSString *kata_DatePickerDaoTime2;
@property(nonatomic,retain)NSString *kata_DatePickerDaoTime3;
@property(nonatomic,retain)NSString *kata_DatePickerDaoTime4;


@end

@implementation kata_DatePickerDao

- (void)dealloc
{
    
    [_picker release];
    
    [super dealloc];
}

- (id)kata_DatePickerDaoDatePicker:(int)aTag
{
    
    if (isPicker == NO)
    {
        isPicker = YES;
        
      _picker  = [[UIDatePicker alloc]init];
        
        _picker.frame = CGRectMake(0,ScreenHeight, ScreenWidth, 216);
        
        _picker.datePickerMode = UIDatePickerModeDate;
        
        _picker.tag = aTag;
        
        [_picker addTarget:self action:@selector(changeTime:) forControlEvents:UIControlEventValueChanged];
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:1];
        
        //        [UIView setAnimationDelegate:self];
        //
        //        [UIView setAnimationDidStopSelector:@selector(animationDidStop:
        //                                                      finished:
        //                                                      context:)];
        
        _picker.frame = CGRectMake(0, ScreenHeight - 216 - 49, ScreenWidth, 216);
        
        
        [UIView commitAnimations];
        
        return _picker;

    }else
    {
        
        return nil;
    }
    

}


- (void)changeTime:(id)sender
{
    
    UIDatePicker *picker = (UIDatePicker *)sender;
    
    isPicker =  NO;
    
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:1];
    
    
    _picker.frame = CGRectMake(0, ScreenHeight + 216, ScreenWidth, 216);
    
    [UIView commitAnimations];
    
    
    //    UIDatePicker* control = (UIDatePicker*)sender;
    
    NSDate* _date = _picker.date;
    
    DebugLog(@"_date == %@",_date);
    
    
   


    
//    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
//    NSTimeZone *zone = [NSTimeZone systemTimeZone];
//    NSInteger interval = [zone secondsFromGMTForDate:datenow];
//    NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];
//    NSLog(@"%@", localeDate);
//    
//    NSString *timeSp = [NSString stringWithFormat:@"%lld", (long long)[localeDate timeIntervalSince1970]];
//    NSLog(@"timeSp:%@",timeSp);
//    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateStr = [formatter stringFromDate:_date];
    
    if (picker.tag == 1) {
        
      
        
//        [self dateStringTransformDate:_date];
        
        self.kata_DatePickerDaoTime1 = dateStr;
        
        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTime: tag:)]) {
            
            [self.delegate kata_DatePickerDaoWithTime:self.kata_DatePickerDaoTime1 tag:1];
            
        }
        
        //时间戳
        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTimeSp:tag:)]) {
            
            [self.delegate kata_DatePickerDaoWithTimeSp:[self dateStringTransformDate:_date] tag:1];
            
            
        }
        
    }
    
    if (picker.tag == 2) {
        
        
        
        //        [self dateStringTransformDate:_date];
        
        self.kata_DatePickerDaoTime2 = dateStr;
        
        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTime: tag:)]) {
            
            [self.delegate kata_DatePickerDaoWithTime:self.kata_DatePickerDaoTime2 tag:2];
            
        }
        
        //时间戳
        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTimeSp:tag:)]) {
            
            [self.delegate kata_DatePickerDaoWithTimeSp:[self dateStringTransformDate:_date] tag:2];
            
            
        }
        
    }

//    else  if (picker.tag == 3) {
//        
//        
//        
//        //        [self dateStringTransformDate:_date];
//        
//        self.kata_DatePickerDaoTime3 = dateStr;
//        
//        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTime: tag:)]) {
//            
//            [self.delegate kata_DatePickerDaoWithTime:self.kata_DatePickerDaoTime3 tag:3];
//            
//        }
//        
//        //时间戳
//        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTimeSp:tag:)]) {
//            
//            [self.delegate kata_DatePickerDaoWithTimeSp:[self dateStringTransformDate:_date] tag:3];
//            
//            
//        }
//        
//    }
//    
//
//    
//    else {
//        
////        [self dateStringTransformDate:_date];
//
//        
//        self.kata_DatePickerDaoTime4 = dateStr;
//        
//        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTime: tag:)]) {
//            
//            [self.delegate kata_DatePickerDaoWithTime:self.kata_DatePickerDaoTime4 tag:4];
//            
//        }
//
//        //时间戳
//        if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTimeSp:tag:)]) {
//            
//            [self.delegate kata_DatePickerDaoWithTimeSp:[self dateStringTransformDate:_date] tag:4];
//
//            
//        }
//    }
    
    [formatter release];
    
    
//    if ([self.delegate respondsToSelector:@selector(kata_DatePickerDaoWithTime1:time2:)]) {
//        
//        [self.delegate kata_DatePickerDaoWithTime1:self.kata_DatePickerDaoTime1 time2:self.kata_DatePickerDaoTime2];
//        
//    }
    
    
    
   
    
    
//    [_tableView reloadData];
}


- (NSString *)dateStringTransformDate:(NSDate *)aDate
{
//    NSTimeZone *zone = [NSTimeZone systemTimeZone];
//    NSInteger interval = [zone secondsFromGMTForDate:aDate];
//    NSDate *localeDate = [aDate  dateByAddingTimeInterval: interval];
//    
//    NSLog(@"%@", localeDate);
//    
//    NSString *timeSp = [NSString stringWithFormat:@"%lld", (long long)[localeDate timeIntervalSince1970]];
    
    
    NSString *timeSp = [NSString stringWithFormat:@"%lld", (long long)[aDate timeIntervalSince1970] * 1000];

    NSLog(@"timeSp:%@",timeSp);
    
    return timeSp;
    
}



@end
