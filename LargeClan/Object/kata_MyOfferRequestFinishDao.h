//
//  kata_MyOfferRequestFinishDao.h
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface kata_MyOfferRequestFinishDao : NSObject


- (NSMutableArray *)kata_MyOfferRequestFinishDaoWithLeftDic:(NSDictionary *)aDic;

- (NSMutableArray *)kata_MyOfferRequestFinishDaoWithRightDic:(NSDictionary *)aDic;


@end
