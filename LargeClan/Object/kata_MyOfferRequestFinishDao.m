//
//  kata_MyOfferRequestFinishDao.m
//  LargeClan
//
//  Created by kata on 14-1-15.
//  Copyright (c) 2014年 kata. All rights reserved.
//

#import "kata_MyOfferRequestFinishDao.h"
#import "KATAConstants.h"
#import "KATAIndex.h"
#import "KATAUtils.h"
#import "kata_Tools.h"

@interface kata_MyOfferRequestFinishDao ()




@property(nonatomic,retain)NSMutableArray *leftArray;
@property(nonatomic,retain)NSMutableArray *rightArray;

@property(nonatomic,retain)NSDictionary *dic;

@end


@implementation kata_MyOfferRequestFinishDao


- (void)dealloc
{
    [_leftArray release];
    
    [_rightArray release];
    
    [super dealloc];
}

- (NSMutableArray *)kata_MyOfferRequestFinishDaoWithLeftDic:(NSDictionary *)aDic

{
    
    
    
    _leftArray  = [[NSMutableArray alloc]init];
    
    
    NSString *code = [NSString stringWithFormat:@"%d",[[aDic objectForKey:@"code"] intValue]];
    
    if ([aDic objectForKey:@"data"] != [NSNull null]) {
        
        NSDictionary *data = [aDic objectForKey:@"data"];
        
        NSString *dataCode = [data objectForKey:@"code"];
        
        NSArray *offer_results = [data objectForKey:@"offer_results"];
        
        if ([offer_results count] != 0) {
            
            for (int i = 0; i < [offer_results count]; i++) {
                
                KATAIndex *indexObj = [[KATAIndex alloc]init];
                
                NSDictionary *dic = [offer_results objectAtIndex:i];
                
                
                //报盘时间
                if ([dic objectForKey:@"offertime"]  != [NSNull null])
                    
                {
                    long long time = [[[dic objectForKey:@"offertime"] objectForKey:@"time"] longLongValue];
                    
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];
                    
                    NSLog(@"confromTimesp ==  %@",confromTimesp);
                    
                    
                    indexObj.auditingstatus = [kata_Tools date:confromTimesp];
                    
                }
                
                
                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];
                
                indexObj.state = state;
                
                [_leftArray addObject:indexObj];
                
                [indexObj release];
                
            }

        }
        

    
}
    


    return _leftArray;

    

    
}

- (NSMutableArray *)kata_MyOfferRequestFinishDaoWithRightDic:(NSDictionary *)aDic

{
    
    // 右  // 右
    
    _rightArray  = [[NSMutableArray alloc]init];

    if ([aDic objectForKey:@"data"] != [NSNull null]) {

        NSDictionary *data = [aDic objectForKey:@"data"];


        NSString *dataCode = [data objectForKey:@"code"];

        NSArray *offer_results = [data objectForKey:@"offer_results"];


        if ([offer_results count] != 0) {

            for (int i = 0; i < [offer_results count]; i++) {

                KATAIndex *indexObj = [[KATAIndex alloc]init];

                NSDictionary *dic = [offer_results objectAtIndex:i];


                //报盘时间
                if ([dic objectForKey:@"auditingDate"]  != [NSNull null])

                {
                    long long time = [[[dic objectForKey:@"auditingDate"] objectForKey:@"time"] longLongValue];

                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(time / 1000)];

                    //        NSLog(@"confromTimesp ==  %@",confromTimesp);


                    indexObj.auditingstatus = [kata_Tools date:confromTimesp];

                }


                //交割时间

                if ([dic objectForKey:@"deliverytime2"]  != [NSNull null] && [dic objectForKey:@"deliverytime2"] != [NSNull null]) {



                    long long deliveryTime1 = [[[dic objectForKey:@"deliverytime1"] objectForKey:@"time"] longLongValue];

                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime1 / 1000)];

                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);

                    indexObj.deliverytime1 = [kata_Tools deliveryDate:confromTimesp1];


                    long long deliveryTime2 = [[[dic objectForKey:@"deliverytime2"] objectForKey:@"time"] longLongValue];

                    NSDate *confromTimesp2 = [NSDate dateWithTimeIntervalSince1970:(deliveryTime2 / 1000)];

                    NSLog(@"confromTimesp2 ==  %@",confromTimesp2);

                    indexObj.deliverytime2 = [kata_Tools deliveryDate:confromTimesp2];

                    NSString *deliveryStr1  = [indexObj.deliverytime1 stringByReplacingOccurrencesOfString:@"GMT+" withString:@"-"];

                    NSString *deliveryStr2  = [indexObj.deliverytime2 stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];


                    indexObj.deliverytime = (NSMutableString *)[deliveryStr1 stringByAppendingString:deliveryStr2];

                    DebugLog(@"indexObj.deliverytime ==  %@",indexObj.deliverytime);


                }


                //预计装船开始时间
                if ([dic objectForKey:@"loadingtime1"]  != [NSNull null]) {



                    long long loadingtime1 = [[[dic objectForKey:@"loadingtime1"] objectForKey:@"time"] longLongValue];

                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime1 / 1000)];


                    indexObj.loadingtime1 = [[kata_Tools deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];

                }

                //预计装船结束时间

                if ([dic objectForKey:@"arrivetime1"]  != [NSNull null]) {



                    long long loadingtime2 = [[[dic objectForKey:@"arrivetime1"] objectForKey:@"time"] longLongValue];

                    NSDate *confromTimesp1 = [NSDate dateWithTimeIntervalSince1970:(loadingtime2 / 1000)];

                    NSLog(@"confromTimesp1 ==  %@",confromTimesp1);

                    indexObj.loadingtime2 = [[kata_Tools deliveryDate:confromTimesp1] stringByReplacingOccurrencesOfString:@"GMT+" withString:@""];

                }
                //
                //货源地

                if ( [dic objectForKey:@"goodsSource"] != [NSNull null]) {


                    NSDictionary *goodsSource  =[dic objectForKey:@"goodsSource"];

                    NSString *goodssourcename = [goodsSource objectForKey:@"goodssourcename"];

                    indexObj.goodssourcename = goodssourcename;

                }

                //
                //交割地

                if ( [dic objectForKey:@"region"] != [NSNull null]) {

                    NSDictionary *regionDic  = [dic objectForKey:@"region"];

                    NSString *deliveryname = [regionDic objectForKey:@"deliveryname"];

                    indexObj.deliveryname = deliveryname;


                }

                // 转手
                if ( [dic objectForKey:@"changehands"] != [NSNull null]) {

                    int  changehands =  [[dic objectForKey:@"changehands"] intValue];

                    DebugLog(@"changehands = %d",changehands);

                    indexObj.changehands = [NSString stringWithFormat:@"%d手",changehands];

                    DebugLog(@"indexObj.changehands = %@",indexObj.changehands);
                }

                // 交单天数

                if ( [dic objectForKey:@"surrenderdocuments"] != [NSNull null]) {

                    int  surrenderdocuments =  [[dic objectForKey:@"surrenderdocuments"] intValue];

                    indexObj.surrenderdocuments = [NSString stringWithFormat:@"%d天",surrenderdocuments];

                }

                // 付款方式

                if ( [dic objectForKey:@"paymentmethods"] != [NSNull null]) {

                    int  paymentmethods =  [[dic objectForKey:@"paymentmethods"] intValue];


                    if (paymentmethods == 0) {

                        indexObj.paymentmethods = @"L/C90 天";

                    }else if (paymentmethods == 1) {

                        indexObj.paymentmethods = @"L/C 即期";

                    }
                    else if (paymentmethods == 2) {

                        indexObj.paymentmethods = @"L/T ";

                    }
                    else{
                        indexObj.paymentmethods = @"其他";

                    }



                }

                // 单据类型

                if ( [dic objectForKey:@"detailedlist"] != [NSNull null]) {

                    int  detailedlist = [[dic objectForKey:@"detailedlist"] intValue];

                    if (detailedlist == 0) {

                        indexObj.detailedlist = @"是";

                    }else
                    {
                        indexObj.detailedlist = @"否";

                    }

                }

                //品名

                if ( [dic objectForKey:@"nameofpart"] != [NSNull null]){

                    int nameofpart = [[dic objectForKey:@"nameofpart"] intValue];

                    indexObj.nameofpart = nameofpart;


                }
                //税 类型

                if ( [dic objectForKey:@"cargobonded"] != [NSNull null]){


                    int cargobonded = (int)[dic objectForKey:@"cargobonded"];


                    if (cargobonded == 0) {

                        indexObj.cargobonded = @"船税";
                    }else
                    {
                        indexObj.cargobonded = @"保税";

                    }

                }


                //配送
                if ( [dic objectForKey:@"dispatching"] != [NSNull null]){

                    int dispatching = [[dic objectForKey:@"dispatching"] intValue];

                    if (dispatching == 0) {

                        indexObj.dispatching = @"自提";

                    }else{

                        indexObj.dispatching = @"送到";
                    }
                }

                //品牌
                if ( [dic objectForKey:@"brand"] != [NSNull null]){

                    indexObj.brand =  [[dic objectForKey:@"brand"] objectForKey:@"brandname"];

                }


                //        //价格

                NSString *quotation  = [NSString stringWithFormat:@"%d",[[dic objectForKey:@"quotation"] intValue]];

                indexObj.quotation = quotation;


                //方向
                int  direction =  [[dic objectForKey:@"direction"] intValue];

                DebugLog(@"directiondirection ==  %d",direction);

                if (direction == 0) {

                    indexObj.direction = @"卖盘";

                }else
                {
                    indexObj.direction = @"买盘";

                }



                NSString *megCode = [dic objectForKey:@"code"];

                indexObj.code = megCode;


                //有效期
                int offerstate = [[dic objectForKey:@"offerstate"] intValue];

                if (offerstate == 0) {

                    indexObj.offerstate = @"有效";

                }else{

                    indexObj.offerstate = @"过期";

                }
                //是否成交
                int dealstatus = [[dic objectForKey:@"dealstatus"] intValue];

                if (dealstatus == 0) {

                    indexObj.dealstatus = @"/成交";

                }else{

                    indexObj.dealstatus = @"/未成交";

                }


                NSMutableString *state = (NSMutableString *)[indexObj.offerstate  stringByAppendingString:indexObj.dealstatus];

                indexObj.state = state;

                //

                //审核状态

                int auditingstatus = [[dic objectForKey:@"auditingstatus"] intValue];

                if (offerstate == 0) {

                    indexObj.auditingstatus = @"未审核";

                }else if(offerstate == 1)
                {
                    indexObj.auditingstatus = @"通过";

                }
                else{

                    indexObj.auditingstatus = @"失败";

                }




                //是否删除
                BOOL isdeleted = (BOOL) [dic objectForKey:@"isdeleted"];

                //是否发布
                BOOL iscancel = (BOOL) [dic objectForKey:@"iscancel"];


                //期货类型
                int spotfutures = (int) [dic objectForKey:@"spotfutures"];


                if (spotfutures == 0) {

                    indexObj.spotfutures = @"期货";

                }else
                {
                    indexObj.spotfutures = @"现货";

                }

                //吨数
                int number = (int) [dic objectForKey:@"number"];


                if (number == 0) {

                    indexObj.number = @"500";

                }else  if (number == 1)
                {
                    indexObj.spotfutures = @"1000";

                }
                else  if (number == 2)
                {
                    indexObj.number = @"1500";

                }
                else  if (number == 3)
                {
                    indexObj.number = @"2000";

                }
                else  if (number == 4)
                {
                    indexObj.number = @"2000";

                }
                else
                {
                    indexObj.number = @"500";

                }


                //是否免仓
                int storagetime = (int) [dic objectForKey:@"storagetime"];

                if (storagetime == 0) {

                    indexObj.storagetime = @"五天";

                }else{

                    indexObj.storagetime = @"七天";
                }



                //保证金
                int cautionmoney = (int) [dic objectForKey:@"cautionmoney"];

                if (cautionmoney == 0) {

                    indexObj.cautionmoney = @"0";

                }else if (spotfutures == 1)
                {
                    indexObj.cautionmoney = @"5%";

                }else
                {
                    indexObj.cautionmoney = @"10%";

                }

                //备注
                NSString *remarks = (NSString *) [dic objectForKey:@"remarks"];

                
                indexObj.remarks = remarks;
                
                //报盘人
                
                int offerpeople = (int)[dic objectForKey:@"offerpeople"];
                
                //发票
                int invoice = (int)[dic objectForKey:@"invoice"];
                
                if (invoice == 0) {
                    
                    indexObj.invoice = @"本月";
                    
                }else
                {
                    indexObj.invoice = @"下月";
                    
                }
                
                
                //勿扰
                
                
                if ( [dic objectForKey:@"notfaze"] != [NSNull null]) {
                    
                    int  notfaze = [[dic objectForKey:@"notfaze"] intValue];
                    
                    if (notfaze == 0) {
                        
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }else
                    {
                        indexObj.notfaze = [NSString stringWithFormat:@"%d",notfaze];
                        
                    }
                    
                }
                
                [_rightArray addObject:indexObj];
                
                [indexObj release];
                
            }
            
            
        }
        
    }
    return _rightArray;
}


@end