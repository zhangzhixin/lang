//
//  KATAConstants.h
//  LargeClan
//
//  Created by kata on 13-12-19.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#ifndef LargeClan_KATAConstants_h
#define LargeClan_KATAConstants_h


#define SERVER_URI @"http://www.ok4008.com/mobile/main.htm"

//#define SERVER_URI @"http://192.168.1.35:8080/dazhong/mobile/main.htm"

#define LBLWIDTH 100

// 通知
#define INDEX_HEARDVIEW @"index_heardView"

#define SEARCH_TABLEVIEWDATA @"search_tableViewData"

#define DEFAULT_INDEXData @"default_indexData"


#define SEARCH_RESULT @"search_result"

#define SEARCH_RESULT_HEARDView @"search_result_heardView"


#define MYOFFERDATA @"myOfferData"




//屏幕
#define ScreenHeight [UIScreen mainScreen].bounds.size.height

#define ScreenWidth [UIScreen mainScreen].bounds.size.width


//
#define PROVINCE_LIST @"provinces_list_get"

#define CITY_LIST @"cities_list_get"





#endif
