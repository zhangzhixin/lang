//
//  kata_BaseRequest.m
//  CityStar
//
//  Created by hark2046 on 13-8-28.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "kata_BaseRequest.h"

#import "KATAConstants.h"
#import "kata_GlobalConst.h"
#import "KATAUtils.h"
#import "NSString+MD5.h"

@implementation kata_BaseRequest

- (id)init
{
    if (self = [super init]) {
        
        _params = [[NSMutableDictionary alloc]init];
        
    }
    return self;
}

- (void)dealloc
{
    [_params release];
    
    [super dealloc];
}

#pragma mark - parameter

- (NSString *)sign
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSMutableString *sign = [NSMutableString stringWithString:@"10010 +"];
    
    [sign appendFormat:@"%@ + %ld",[self method],timestamp];
    
    [sign MD5];
    
    return [sign description] ;
}


- (NSString *)appkey
{
    return @"10010";
    
}
- (NSString *)method
{
    return nil;
}

- (NSMutableDictionary *)params
{
    return _params;
    
}

- (long int )time
{
    long int timestamp = [[NSDate date] timeIntervalSince1970];
    
    return timestamp;
}

@end
